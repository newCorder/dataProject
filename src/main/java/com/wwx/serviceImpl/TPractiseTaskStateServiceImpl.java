package com.wwx.serviceImpl;

import com.wwx.entity.TPractiseTaskState;
import com.wwx.mapper.TPractiseTaskStateMapper;
import com.wwx.service.ITPractiseTaskStateService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
@Service
public class TPractiseTaskStateServiceImpl extends ServiceImpl<TPractiseTaskStateMapper, TPractiseTaskState> implements ITPractiseTaskStateService {

}
