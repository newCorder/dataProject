package com.wwx.serviceImpl;

import com.wwx.entity.TPractiseQuestionScore;
import com.wwx.mapper.TPractiseQuestionScoreMapper;
import com.wwx.service.ITPractiseQuestionScoreService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-21
 */
@Service
public class TPractiseQuestionScoreServiceImpl extends ServiceImpl<TPractiseQuestionScoreMapper, TPractiseQuestionScore> implements ITPractiseQuestionScoreService {

}
