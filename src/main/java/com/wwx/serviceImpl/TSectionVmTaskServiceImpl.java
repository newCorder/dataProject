package com.wwx.serviceImpl;

import com.wwx.entity.TSectionVmTask;
import com.wwx.mapper.TSectionVmTaskMapper;
import com.wwx.service.ITSectionVmTaskService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
@Service
public class TSectionVmTaskServiceImpl extends ServiceImpl<TSectionVmTaskMapper, TSectionVmTask> implements ITSectionVmTaskService {

}
