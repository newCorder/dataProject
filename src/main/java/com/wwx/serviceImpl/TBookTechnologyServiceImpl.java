package com.wwx.serviceImpl;

import com.wwx.entity.TBookTechnology;
import com.wwx.mapper.TBookTechnologyMapper;
import com.wwx.service.ITBookTechnologyService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
@Service
public class TBookTechnologyServiceImpl extends ServiceImpl<TBookTechnologyMapper, TBookTechnology> implements ITBookTechnologyService {

}
