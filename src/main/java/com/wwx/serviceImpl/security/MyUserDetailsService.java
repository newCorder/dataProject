package com.wwx.serviceImpl.security;

import com.wwx.config.MyUserDetails;
import com.wwx.entity.Student;
import com.wwx.entity.TStudent;
import com.wwx.entity.pojo.Role;
import com.wwx.entity.pojo.UserInfo;
import com.wwx.entity.pojo.UserRole;
import com.wwx.service.IStudentService;
import com.wwx.service.ITRoleService;
import com.wwx.service.ITStudentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author wuweixu
 * @create 2019/8/19
 */
//@Component
public class MyUserDetailsService  implements UserDetailsService {
    private static final Logger LOG = LoggerFactory.getLogger(MyUserDetailsService.class);

    @Autowired
    private ITStudentService studentService;
    @Autowired
    private ITRoleService roleService;
    @Autowired
    private IStudentService iStudentService;
    @Override
    public UserDetails loadUserByUsername(String userNo) throws UsernameNotFoundException {
        UserDetails userDetails = null;
        try{
            Student result = iStudentService.getById("1");
            List<Student> aResult = iStudentService.list();
            TStudent student = studentService.getById(userNo);
            if( student != null ){
//                List<UserRole> urs = userRoleMapper.findByUserId(user.getId());
//                roleService.
                Collection<GrantedAuthority> authorities = new ArrayList<>();
//                for(UserRole ur : urs) {
//                    String roleName = ur.getRole().getName();
                String roleName = "学生";
                SimpleGrantedAuthority grant = new SimpleGrantedAuthority(roleName);
                authorities.add(grant);
//                }
                //封装自定义UserDetails类
                userDetails = new MyUserDetails(student, authorities);
            }
        }catch (Exception e){
            LOG.error("loadUserByUsername is error.",e);
        }
        return userDetails;
    }
}
