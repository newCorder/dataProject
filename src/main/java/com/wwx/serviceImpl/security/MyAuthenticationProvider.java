package com.wwx.serviceImpl.security;

import com.wwx.entity.TStudent;
import com.wwx.entity.pojo.UserInfo;
import com.wwx.service.ITAdminService;
import com.wwx.service.ITStudentService;
import com.wwx.service.ITTeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.*;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collection;

/**
 * @author wuweixu
 * @create 2019/8/19
 */
//@Component
public class MyAuthenticationProvider implements AuthenticationProvider {
    /**
     * 注入我们自己定义的用户信息获取对象
     */
//    @Autowired
//    private UserDetailsService userDetailService;
    @Autowired
    private ITStudentService studentService;
    @Autowired
    private ITAdminService tAdminService;
    @Autowired
    private ITTeacherService teacherService;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        return null;
//        UsernamePasswordAuthenticationToken token = (UsernamePasswordAuthenticationToken) authentication;
//        // 这个获取表单输入中返回的用户名
//        String userNo = token.getName();
//        // 这个是表单中输入的密码
//        // String password = (String) token.getPrincipal();
//
//        UserDetails userDetails = null;
//        if (userNo != null) {
//            userDetails = userDetailService.loadUserByUsername(userNo);
//        }
//        System.out.println("$$" + userDetails);
//
//        if (userDetails == null) {
//            throw new UsernameNotFoundException("用户名/密码无效");
//        }
//
//        String password = userDetails.getPassword();
//
//        //判断密码是否正确
//        //与authentication里面的credentials相比较
//        if (!password.equals(token.getCredentials())) {
//            throw new BadCredentialsException("Invalid username/password");
//        }
//        //授权
//        return new UsernamePasswordAuthenticationToken(userDetails, password, userDetails.getAuthorities());
    }

//    @Override
//    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
//        // TODO Auto-generated method stub
//        String userName = authentication.getName();// 这个获取表单输入中返回的用户名;
//        String password = (String) authentication.getCredentials();// 这个是表单中输入的密码；
//        // 这里构建来判断用户是否存在和密码是否正确
//        UserInfo userInfo = (UserInfo) userDetailService.loadUserByUsername(userName); // 这里调用我们的自己写的获取用户的方法；
//
//        if (userInfo == null) {
//            throw new BadCredentialsException("用户名不存在");
//        }
//        // //这里我们还要判断密码是否正确，实际应用中，我们的密码一般都会加密，以Md5加密为例
//        // Md5PasswordEncoder md5PasswordEncoder=new Md5PasswordEncoder();
//        // //这里第个参数，是salt
//        // 就是加点盐的意思，这样的好处就是用户的密码如果都是123456，由于盐的不同，密码也是不一样的，就不用怕相同密码泄漏之后，不会批量被破解。
//        // String encodePwd=md5PasswordEncoder.encodePassword(password, userName);
//        // //这里判断密码正确与否
//        // if(!userInfo.getPassword().equals(encodePwd))
//        // {
//        // throw new BadCredentialsException("密码不正确");
//        // }
//        // //这里还可以加一些其他信息的判断，比如用户账号已停用等判断，这里为了方便我接下去的判断，我就不用加密了。
//        //
//        //
//        if (!userInfo.getPassword().equals(password )) {
//            throw new BadCredentialsException("密码不正确");
//        }
//        Collection<? extends GrantedAuthority> authorities = userInfo.getAuthorities();
//        // 构建返回的用户登录成功的token
//        return new UsernamePasswordAuthenticationToken(userInfo, password, authorities);
//    }
    @Override
    public boolean supports(Class<?> authentication) {
        // TODO Auto-generated method stub
        // 这里直接改成retrun true;表示是支持这个执行
        return true;
    }
}
