package com.wwx.serviceImpl;

import com.wwx.entity.TGrade;
import com.wwx.mapper.TGradeMapper;
import com.wwx.service.ITGradeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
@Service
public class TGradeServiceImpl extends ServiceImpl<TGradeMapper, TGrade> implements ITGradeService {

}
