package com.wwx.serviceImpl;

import com.wwx.entity.TExamInstancePractiseEssayTestpoint;
import com.wwx.mapper.TExamInstancePractiseEssayTestpointMapper;
import com.wwx.service.ITExamInstancePractiseEssayTestpointService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
@Service
public class TExamInstancePractiseEssayTestpointServiceImpl extends ServiceImpl<TExamInstancePractiseEssayTestpointMapper, TExamInstancePractiseEssayTestpoint> implements ITExamInstancePractiseEssayTestpointService {

}
