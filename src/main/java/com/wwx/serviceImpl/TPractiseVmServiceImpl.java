package com.wwx.serviceImpl;

import com.wwx.entity.TPractiseVm;
import com.wwx.mapper.TPractiseVmMapper;
import com.wwx.service.ITPractiseVmService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
@Service
public class TPractiseVmServiceImpl extends ServiceImpl<TPractiseVmMapper, TPractiseVm> implements ITPractiseVmService {

}
