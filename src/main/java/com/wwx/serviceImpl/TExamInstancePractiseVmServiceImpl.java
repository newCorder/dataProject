package com.wwx.serviceImpl;

import com.wwx.entity.TExamInstancePractiseVm;
import com.wwx.mapper.TExamInstancePractiseVmMapper;
import com.wwx.service.ITExamInstancePractiseVmService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
@Service
public class TExamInstancePractiseVmServiceImpl extends ServiceImpl<TExamInstancePractiseVmMapper, TExamInstancePractiseVm> implements ITExamInstancePractiseVmService {

}
