package com.wwx.serviceImpl;

import com.wwx.entity.TConfig;
import com.wwx.mapper.TConfigMapper;
import com.wwx.service.ITConfigService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * system config 服务实现类
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
@Service
public class TConfigServiceImpl extends ServiceImpl<TConfigMapper, TConfig> implements ITConfigService {

}
