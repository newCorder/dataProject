package com.wwx.serviceImpl;

import com.wwx.entity.TReportScreenshot;
import com.wwx.mapper.TReportScreenshotMapper;
import com.wwx.service.ITReportScreenshotService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
@Service
public class TReportScreenshotServiceImpl extends ServiceImpl<TReportScreenshotMapper, TReportScreenshot> implements ITReportScreenshotService {

}
