package com.wwx.serviceImpl;

import com.wwx.entity.TRole;
import com.wwx.mapper.TRoleMapper;
import com.wwx.service.ITRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色表 服务实现类
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-19
 */
@Service
public class TRoleServiceImpl extends ServiceImpl<TRoleMapper, TRole> implements ITRoleService {

}
