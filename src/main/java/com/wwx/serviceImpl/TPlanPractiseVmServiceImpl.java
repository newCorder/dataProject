package com.wwx.serviceImpl;

import com.wwx.entity.TPlanPractiseVm;
import com.wwx.mapper.TPlanPractiseVmMapper;
import com.wwx.service.ITPlanPractiseVmService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
@Service
public class TPlanPractiseVmServiceImpl extends ServiceImpl<TPlanPractiseVmMapper, TPlanPractiseVm> implements ITPlanPractiseVmService {

}
