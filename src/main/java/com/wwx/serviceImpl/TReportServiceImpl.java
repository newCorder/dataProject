package com.wwx.serviceImpl;

import com.wwx.entity.TReport;
import com.wwx.mapper.TReportMapper;
import com.wwx.service.ITReportService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
@Service
public class TReportServiceImpl extends ServiceImpl<TReportMapper, TReport> implements ITReportService {

}
