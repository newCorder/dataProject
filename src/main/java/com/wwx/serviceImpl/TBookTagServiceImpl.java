package com.wwx.serviceImpl;

import com.wwx.entity.TBookTag;
import com.wwx.mapper.TBookTagMapper;
import com.wwx.service.ITBookTagService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
@Service
public class TBookTagServiceImpl extends ServiceImpl<TBookTagMapper, TBookTag> implements ITBookTagService {

}
