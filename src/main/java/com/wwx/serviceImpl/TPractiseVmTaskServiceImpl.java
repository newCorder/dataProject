package com.wwx.serviceImpl;

import com.wwx.entity.TPractiseVmTask;
import com.wwx.mapper.TPractiseVmTaskMapper;
import com.wwx.service.ITPractiseVmTaskService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
@Service
public class TPractiseVmTaskServiceImpl extends ServiceImpl<TPractiseVmTaskMapper, TPractiseVmTask> implements ITPractiseVmTaskService {

}
