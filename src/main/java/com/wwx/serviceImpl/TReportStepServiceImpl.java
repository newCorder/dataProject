package com.wwx.serviceImpl;

import com.wwx.entity.TReportStep;
import com.wwx.mapper.TReportStepMapper;
import com.wwx.service.ITReportStepService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
@Service
public class TReportStepServiceImpl extends ServiceImpl<TReportStepMapper, TReportStep> implements ITReportStepService {

}
