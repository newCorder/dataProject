package com.wwx.serviceImpl;

import com.wwx.entity.TCoursetag;
import com.wwx.mapper.TCoursetagMapper;
import com.wwx.service.ITCoursetagService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-21
 */
@Service
public class TCoursetagServiceImpl extends ServiceImpl<TCoursetagMapper, TCoursetag> implements ITCoursetagService {

}
