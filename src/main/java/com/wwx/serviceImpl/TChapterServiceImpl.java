package com.wwx.serviceImpl;

import com.wwx.entity.TChapter;
import com.wwx.mapper.TChapterMapper;
import com.wwx.service.ITChapterService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-21
 */
@Service
public class TChapterServiceImpl extends ServiceImpl<TChapterMapper, TChapter> implements ITChapterService {

}
