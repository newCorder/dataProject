package com.wwx.serviceImpl;

import com.wwx.entity.TPlanPractiseVmTask;
import com.wwx.mapper.TPlanPractiseVmTaskMapper;
import com.wwx.service.ITPlanPractiseVmTaskService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
@Service
public class TPlanPractiseVmTaskServiceImpl extends ServiceImpl<TPlanPractiseVmTaskMapper, TPlanPractiseVmTask> implements ITPlanPractiseVmTaskService {

}
