package com.wwx.serviceImpl;

import com.wwx.entity.TPractiseItemStu;
import com.wwx.mapper.TPractiseItemStuMapper;
import com.wwx.service.ITPractiseItemStuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
@Service
public class TPractiseItemStuServiceImpl extends ServiceImpl<TPractiseItemStuMapper, TPractiseItemStu> implements ITPractiseItemStuService {

}
