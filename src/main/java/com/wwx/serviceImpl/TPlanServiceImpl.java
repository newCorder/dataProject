package com.wwx.serviceImpl;

import com.wwx.entity.TPlan;
import com.wwx.mapper.TPlanMapper;
import com.wwx.service.ITPlanService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
@Service
public class TPlanServiceImpl extends ServiceImpl<TPlanMapper, TPlan> implements ITPlanService {

}
