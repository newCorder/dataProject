package com.wwx.serviceImpl;

import com.wwx.entity.TPractise;
import com.wwx.mapper.TPractiseMapper;
import com.wwx.service.ITPractiseService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 测评信息表 服务实现类
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
@Service
public class TPractiseServiceImpl extends ServiceImpl<TPractiseMapper, TPractise> implements ITPractiseService {

}
