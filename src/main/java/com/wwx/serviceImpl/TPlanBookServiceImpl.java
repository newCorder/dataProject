package com.wwx.serviceImpl;

import com.wwx.entity.TPlanBook;
import com.wwx.mapper.TPlanBookMapper;
import com.wwx.service.ITPlanBookService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
@Service
public class TPlanBookServiceImpl extends ServiceImpl<TPlanBookMapper, TPlanBook> implements ITPlanBookService {

}
