package com.wwx.serviceImpl;

import com.wwx.entity.TJob;
import com.wwx.mapper.TJobMapper;
import com.wwx.service.ITJobService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
@Service
public class TJobServiceImpl extends ServiceImpl<TJobMapper, TJob> implements ITJobService {

}
