package com.wwx.serviceImpl;

import com.wwx.entity.TTechnology;
import com.wwx.mapper.TTechnologyMapper;
import com.wwx.service.ITTechnologyService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
@Service
public class TTechnologyServiceImpl extends ServiceImpl<TTechnologyMapper, TTechnology> implements ITTechnologyService {

}
