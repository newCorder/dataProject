package com.wwx.serviceImpl;

import com.wwx.entity.TExamPractiseItemStu;
import com.wwx.mapper.TExamPractiseItemStuMapper;
import com.wwx.service.ITExamPractiseItemStuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
@Service
public class TExamPractiseItemStuServiceImpl extends ServiceImpl<TExamPractiseItemStuMapper, TExamPractiseItemStu> implements ITExamPractiseItemStuService {

}
