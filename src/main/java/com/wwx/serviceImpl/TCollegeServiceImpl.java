package com.wwx.serviceImpl;

import com.wwx.entity.TCollege;
import com.wwx.mapper.TCollegeMapper;
import com.wwx.service.ITCollegeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
@Service
public class TCollegeServiceImpl extends ServiceImpl<TCollegeMapper, TCollege> implements ITCollegeService {

}
