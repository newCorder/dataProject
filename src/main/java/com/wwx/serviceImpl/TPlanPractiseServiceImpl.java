package com.wwx.serviceImpl;

import com.wwx.entity.TPlanPractise;
import com.wwx.mapper.TPlanPractiseMapper;
import com.wwx.service.ITPlanPractiseService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
@Service
public class TPlanPractiseServiceImpl extends ServiceImpl<TPlanPractiseMapper, TPlanPractise> implements ITPlanPractiseService {

}
