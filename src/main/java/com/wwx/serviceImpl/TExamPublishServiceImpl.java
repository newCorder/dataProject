package com.wwx.serviceImpl;

import com.wwx.entity.TExamPublish;
import com.wwx.mapper.TExamPublishMapper;
import com.wwx.service.ITExamPublishService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
@Service
public class TExamPublishServiceImpl extends ServiceImpl<TExamPublishMapper, TExamPublish> implements ITExamPublishService {

}
