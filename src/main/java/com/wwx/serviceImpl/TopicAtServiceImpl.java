package com.wwx.serviceImpl;

import com.wwx.entity.TopicAt;
import com.wwx.mapper.TopicAtMapper;
import com.wwx.service.ITopicAtService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 评论@信息表 服务实现类
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-05
 */
@Service
public class TopicAtServiceImpl extends ServiceImpl<TopicAtMapper, TopicAt> implements ITopicAtService {

}
