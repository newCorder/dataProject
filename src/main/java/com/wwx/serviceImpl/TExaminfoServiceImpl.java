package com.wwx.serviceImpl;

import com.wwx.entity.TExaminfo;
import com.wwx.mapper.TExaminfoMapper;
import com.wwx.service.ITExaminfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-21
 */
@Service
public class TExaminfoServiceImpl extends ServiceImpl<TExaminfoMapper, TExaminfo> implements ITExaminfoService {

}
