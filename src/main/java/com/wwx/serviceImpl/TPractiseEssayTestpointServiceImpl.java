package com.wwx.serviceImpl;

import com.wwx.entity.TPractiseEssayTestpoint;
import com.wwx.mapper.TPractiseEssayTestpointMapper;
import com.wwx.service.ITPractiseEssayTestpointService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
@Service
public class TPractiseEssayTestpointServiceImpl extends ServiceImpl<TPractiseEssayTestpointMapper, TPractiseEssayTestpoint> implements ITPractiseEssayTestpointService {

}
