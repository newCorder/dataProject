package com.wwx.serviceImpl;

import com.wwx.entity.TBookCategory;
import com.wwx.mapper.TBookCategoryMapper;
import com.wwx.service.ITBookCategoryService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
@Service
public class TBookCategoryServiceImpl extends ServiceImpl<TBookCategoryMapper, TBookCategory> implements ITBookCategoryService {

}
