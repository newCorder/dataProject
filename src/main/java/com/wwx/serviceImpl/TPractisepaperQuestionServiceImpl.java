package com.wwx.serviceImpl;

import com.wwx.entity.TPractisepaperQuestion;
import com.wwx.mapper.TPractisepaperQuestionMapper;
import com.wwx.service.ITPractisepaperQuestionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-21
 */
@Service
public class TPractisepaperQuestionServiceImpl extends ServiceImpl<TPractisepaperQuestionMapper, TPractisepaperQuestion> implements ITPractisepaperQuestionService {

}
