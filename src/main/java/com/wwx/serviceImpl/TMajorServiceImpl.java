package com.wwx.serviceImpl;

import com.wwx.entity.TMajor;
import com.wwx.mapper.TMajorMapper;
import com.wwx.service.ITMajorService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
@Service
public class TMajorServiceImpl extends ServiceImpl<TMajorMapper, TMajor> implements ITMajorService {

}
