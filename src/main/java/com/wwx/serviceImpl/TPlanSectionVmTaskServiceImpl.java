package com.wwx.serviceImpl;

import com.wwx.entity.TPlanSectionVmTask;
import com.wwx.mapper.TPlanSectionVmTaskMapper;
import com.wwx.service.ITPlanSectionVmTaskService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
@Service
public class TPlanSectionVmTaskServiceImpl extends ServiceImpl<TPlanSectionVmTaskMapper, TPlanSectionVmTask> implements ITPlanSectionVmTaskService {

}
