package com.wwx.serviceImpl;

import com.wwx.entity.TLatestSectionStu;
import com.wwx.mapper.TLatestSectionStuMapper;
import com.wwx.service.ITLatestSectionStuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
@Service
public class TLatestSectionStuServiceImpl extends ServiceImpl<TLatestSectionStuMapper, TLatestSectionStu> implements ITLatestSectionStuService {

}
