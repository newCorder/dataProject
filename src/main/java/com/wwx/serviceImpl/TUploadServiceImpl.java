package com.wwx.serviceImpl;

import com.wwx.entity.TUpload;
import com.wwx.mapper.TUploadMapper;
import com.wwx.service.ITUploadService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 上传的文件 服务实现类
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
@Service
public class TUploadServiceImpl extends ServiceImpl<TUploadMapper, TUpload> implements ITUploadService {

}
