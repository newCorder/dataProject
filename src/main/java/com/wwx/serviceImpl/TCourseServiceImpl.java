package com.wwx.serviceImpl;

import com.wwx.entity.TCourse;
import com.wwx.mapper.TCourseMapper;
import com.wwx.service.ITCourseService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-21
 */
@Service
public class TCourseServiceImpl extends ServiceImpl<TCourseMapper, TCourse> implements ITCourseService {

}
