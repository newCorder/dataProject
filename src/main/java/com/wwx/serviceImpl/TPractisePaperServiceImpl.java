package com.wwx.serviceImpl;

import com.wwx.entity.TPractisePaper;
import com.wwx.mapper.TPractisePaperMapper;
import com.wwx.service.ITPractisePaperService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-21
 */
@Service
public class TPractisePaperServiceImpl extends ServiceImpl<TPractisePaperMapper, TPractisePaper> implements ITPractisePaperService {

}
