package com.wwx.serviceImpl;

import com.wwx.entity.TExamPaper;
import com.wwx.mapper.TExamPaperMapper;
import com.wwx.service.ITExamPaperService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
@Service
public class TExamPaperServiceImpl extends ServiceImpl<TExamPaperMapper, TExamPaper> implements ITExamPaperService {

}
