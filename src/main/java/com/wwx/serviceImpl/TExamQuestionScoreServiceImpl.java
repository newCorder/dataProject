package com.wwx.serviceImpl;

import com.wwx.entity.TExamQuestionScore;
import com.wwx.mapper.TExamQuestionScoreMapper;
import com.wwx.service.ITExamQuestionScoreService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-21
 */
@Service
public class TExamQuestionScoreServiceImpl extends ServiceImpl<TExamQuestionScoreMapper, TExamQuestionScore> implements ITExamQuestionScoreService {

}
