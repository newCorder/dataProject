package com.wwx.serviceImpl;

import com.wwx.entity.TAuthRecord;
import com.wwx.mapper.TAuthRecordMapper;
import com.wwx.service.ITAuthRecordService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
@Service
public class TAuthRecordServiceImpl extends ServiceImpl<TAuthRecordMapper, TAuthRecord> implements ITAuthRecordService {

}
