package com.wwx.serviceImpl;

import com.wwx.entity.TLatestSectionTeacher;
import com.wwx.mapper.TLatestSectionTeacherMapper;
import com.wwx.service.ITLatestSectionTeacherService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
@Service
public class TLatestSectionTeacherServiceImpl extends ServiceImpl<TLatestSectionTeacherMapper, TLatestSectionTeacher> implements ITLatestSectionTeacherService {

}
