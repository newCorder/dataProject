package com.wwx.serviceImpl;

import com.wwx.entity.TExamInstancePractiseTaskState;
import com.wwx.mapper.TExamInstancePractiseTaskStateMapper;
import com.wwx.service.ITExamInstancePractiseTaskStateService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
@Service
public class TExamInstancePractiseTaskStateServiceImpl extends ServiceImpl<TExamInstancePractiseTaskStateMapper, TExamInstancePractiseTaskState> implements ITExamInstancePractiseTaskStateService {

}
