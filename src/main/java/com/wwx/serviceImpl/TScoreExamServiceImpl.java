package com.wwx.serviceImpl;

import com.wwx.entity.TScoreExam;
import com.wwx.mapper.TScoreExamMapper;
import com.wwx.service.ITScoreExamService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
@Service
public class TScoreExamServiceImpl extends ServiceImpl<TScoreExamMapper, TScoreExam> implements ITScoreExamService {

}
