package com.wwx.serviceImpl;

import com.wwx.entity.TPractiseBank;
import com.wwx.mapper.TPractiseBankMapper;
import com.wwx.service.ITPractiseBankService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
@Service
public class TPractiseBankServiceImpl extends ServiceImpl<TPractiseBankMapper, TPractiseBank> implements ITPractiseBankService {

}
