package com.wwx.serviceImpl;

import com.wwx.entity.TPlanPractiseEssayTestpoint;
import com.wwx.mapper.TPlanPractiseEssayTestpointMapper;
import com.wwx.service.ITPlanPractiseEssayTestpointService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
@Service
public class TPlanPractiseEssayTestpointServiceImpl extends ServiceImpl<TPlanPractiseEssayTestpointMapper, TPlanPractiseEssayTestpoint> implements ITPlanPractiseEssayTestpointService {

}
