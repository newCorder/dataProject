package com.wwx.serviceImpl;

import com.wwx.entity.TTaskState;
import com.wwx.mapper.TTaskStateMapper;
import com.wwx.service.ITTaskStateService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
@Service
public class TTaskStateServiceImpl extends ServiceImpl<TTaskStateMapper, TTaskState> implements ITTaskStateService {

}
