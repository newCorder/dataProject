package com.wwx.serviceImpl;

import com.wwx.entity.TPlanSection;
import com.wwx.mapper.TPlanSectionMapper;
import com.wwx.service.ITPlanSectionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
@Service
public class TPlanSectionServiceImpl extends ServiceImpl<TPlanSectionMapper, TPlanSection> implements ITPlanSectionService {

}
