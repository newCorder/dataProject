package com.wwx.serviceImpl;

import com.wwx.entity.TVm;
import com.wwx.mapper.TVmMapper;
import com.wwx.service.ITVmService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
@Service
public class TVmServiceImpl extends ServiceImpl<TVmMapper, TVm> implements ITVmService {

}
