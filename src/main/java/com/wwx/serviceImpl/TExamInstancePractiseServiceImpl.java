package com.wwx.serviceImpl;

import com.wwx.entity.TExamInstancePractise;
import com.wwx.mapper.TExamInstancePractiseMapper;
import com.wwx.service.ITExamInstancePractiseService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
@Service
public class TExamInstancePractiseServiceImpl extends ServiceImpl<TExamInstancePractiseMapper, TExamInstancePractise> implements ITExamInstancePractiseService {

}
