package com.wwx.serviceImpl;

import com.wwx.entity.TChapterRecord;
import com.wwx.mapper.TChapterRecordMapper;
import com.wwx.service.ITChapterRecordService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-21
 */
@Service
public class TChapterRecordServiceImpl extends ServiceImpl<TChapterRecordMapper, TChapterRecord> implements ITChapterRecordService {

}
