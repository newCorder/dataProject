package com.wwx.serviceImpl;

import com.wwx.entity.TExampaperQuestion;
import com.wwx.mapper.TExampaperQuestionMapper;
import com.wwx.service.ITExampaperQuestionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-21
 */
@Service
public class TExampaperQuestionServiceImpl extends ServiceImpl<TExampaperQuestionMapper, TExampaperQuestion> implements ITExampaperQuestionService {

}
