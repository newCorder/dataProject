package com.wwx.serviceImpl;

import com.wwx.entity.TScore;
import com.wwx.mapper.TScoreMapper;
import com.wwx.service.ITScoreService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-21
 */
@Service
public class TScoreServiceImpl extends ServiceImpl<TScoreMapper, TScore> implements ITScoreService {

}
