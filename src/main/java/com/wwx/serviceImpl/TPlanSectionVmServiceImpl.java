package com.wwx.serviceImpl;

import com.wwx.entity.TPlanSectionVm;
import com.wwx.mapper.TPlanSectionVmMapper;
import com.wwx.service.ITPlanSectionVmService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
@Service
public class TPlanSectionVmServiceImpl extends ServiceImpl<TPlanSectionVmMapper, TPlanSectionVm> implements ITPlanSectionVmService {

}
