package com.wwx.controller;

import io.swagger.annotations.Api;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author wuweixu
 * @create 2019/8/19
 */
@Api(description = "默认页")
@Controller
public class IndexViewController {
    /**
     * 登录
     * @return
     */
//    @GetMapping("/")
//    public String index() {
//        return "login";
//    }

    /**
     * 欢迎页
     * @return
     */
    @GetMapping("/welcome")
    public String welcome() {
        return "welcome";
    }
}
