package com.wwx.controller;

import com.github.dockerjava.api.model.LogConfig;
import com.wwx.common.RoleEnum;
import com.wwx.entity.TAdmin;
import com.wwx.entity.TStudent;
import com.wwx.entity.TTeacher;
import com.wwx.service.ITAdminService;
import com.wwx.service.ITStudentService;
import com.wwx.service.ITTeacherService;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * @author wuweixu
 * @create 2019/8/19
 */
@Api(description = "登录页")
@Controller
public class LoginViewController {
    private static final Logger LOG = LoggerFactory.getLogger(LoginViewController.class);

    // 预先设置好的正确的用户名和密码，用于登录验证
    private String rightUserName = "admin";
    private String rightPassword = "admin";

    @Autowired
    private ITStudentService studentService;
    @Autowired
    private ITAdminService tAdminService;
    @Autowired
    private ITTeacherService teacherService;

    /**
     * 登录校验
     *
     * @param request
     * @return
     */
    @RequestMapping("/login")
    public String login(HttpServletRequest request) {
        LOG.info("======进行登录验证======");
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String type = request.getParameter("type");
        if (null == username || null == password || null == type) {
            return "redirect:/";
        }

        if( RoleEnum.STUDENT.getType().equals(type) ){
            TStudent studentResult = studentService.getById(username);
            if( null != studentResult ){
                // 前端传回的密码实际为用户输入的：用户名（小写）+ 密码（小写）组合的字符串生成的md5值
                // 此处先通过后台保存的正确的用户名和密码计算出正确的md5值，然后和前端传回来的作比较
                String md5info = studentResult.getName().toLowerCase() + studentResult.getPwd().toLowerCase();
                String realPassword = DigestUtils.md5DigestAsHex(md5info.getBytes());
                if (!password.equals(realPassword)  ) {
                    return "redirect:/";
                }
            }
            // 校验通过时，在session里放入一个标识
            // 后续通过session里是否存在该标识来判断用户是否登录
            request.getSession().setAttribute("loginName", "admin");
            return "redirect:/welcome";
        }else if( RoleEnum.TEACHER.getType().equals(type) ){
            TTeacher teacherResult = teacherService.getById(username);
            if( null != teacherResult ){
                String md5info = teacherResult.getName().toLowerCase() + teacherResult.getPwd().toLowerCase();
                String realPassword = DigestUtils.md5DigestAsHex(md5info.getBytes());
                if (!password.equals(realPassword)) {
                    return "redirect:/";
                }
            }
            request.getSession().setAttribute("loginName", "admin");
            return "redirect:/welcome";
        }else if( RoleEnum.ADMIN.getType().equals(type) ){
            TAdmin adminResult = tAdminService.getById(username);
            if( null != adminResult ){
                String md5info = adminResult.getName().toLowerCase() + adminResult.getPwd().toLowerCase();
                String realPassword = DigestUtils.md5DigestAsHex(md5info.getBytes());
                if (!password.equals(realPassword)) {
                    return "redirect:/";
                }
            }
            request.getSession().setAttribute("loginName", "admin");
            return "redirect:/welcome";
        }
        return "redirect:/";
    }

//    @RequestMapping("/whoim")
//    public Object whoIm()
//    {
//        return SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//    }

    /**
     * 注销登录
     *
     * @param request
     * @return
     */
    @RequestMapping("/loginout")
    public String loginOut(HttpServletRequest request) {
        request.getSession().invalidate();
        return "redirect:/";
    }

}
