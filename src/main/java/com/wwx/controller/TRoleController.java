package com.wwx.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

/**
 * <p>
 * 角色表 前端控制器
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-19
 */
@Controller
@RequestMapping("/wwx/tRole")
public class TRoleController {

}

