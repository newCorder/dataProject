package com.wwx.controller;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wwx.common.ApiResult;
import com.wwx.common.ApiResultMsg;
import com.wwx.common.YnEnum;
import com.wwx.entity.TCollege;
import com.wwx.service.ITCollegeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  学院表
 * </p>
 * @author wuweixu
 * @since 2019-08-12
 */
@Api(description = "院系管理")
@RestController
@RequestMapping("/priAdmin/school")
public class TCollegeController {
    private final Logger LOG = LoggerFactory.getLogger(getClass());

    @Autowired
    private ITCollegeService itCollegeService;

    /**
     * 院系管理查询列表（分页）
     * @return
     */
    @ApiOperation(value="院系管理查询列表（分页）", notes="院系管理查询列表（分页）")
    @RequestMapping(value = "/list",method = RequestMethod.GET)
    public ApiResult<IPage<TCollege>> list(TCollege tCollege, int pageNo , int pageSize){
        try{
            QueryWrapper<TCollege> queryWrapper = new QueryWrapper<TCollege>();
            LambdaQueryWrapper<TCollege> q = queryWrapper.lambda();
            if( StringUtils.isNoneBlank(tCollege.getName()) ){
                q.eq(TCollege::getName, tCollege.getName());
            }
            IPage<TCollege> page = new Page<>(pageNo, pageSize);
            IPage<TCollege> result = itCollegeService.page(page, q);
            return ApiResult.getSuccessResult(result);
        }catch (Exception e){
            LOG.error("院系管理查询列表（分页） 异常",e);
            return ApiResult.getFailResult(ApiResultMsg.UNKNOWN_ERROR.getCode(), ApiResultMsg.UNKNOWN_ERROR.getMsg());
        }
    }

    /**
     * 新增院系信息
     * @return
     */
    @ApiOperation(value="新增院系信息", notes="新增院系信息")
    @RequestMapping(value = "/add",method = RequestMethod.POST)
    public boolean addTeacher(TCollege tCollege){
        boolean result = itCollegeService.save(tCollege);
        return result;
    }

    /**
     * 查询单个院系信息
     * @return
     */
    @ApiOperation(value="查询单个院系信息", notes="uid字段必填")
    @RequestMapping(value = "/query", method = RequestMethod.GET)
    public ApiResult<TCollege> queryTeacher(TCollege tCollege){
        TCollege result = itCollegeService.getById(tCollege.getCollegeId());
        return ApiResult.getSuccessResult(result);
    }

    /**
     * 修改院系信息
     * @param tCollege 入参：对象中需要有主键id
     * @return
     */
    @ApiOperation(value="修改院系信息", notes="uid字段必填")
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public boolean ediTCollege(TCollege tCollege){
        UpdateWrapper<TCollege> updateWrapper = new UpdateWrapper<TCollege>();
        LambdaUpdateWrapper<TCollege> wrapper = updateWrapper.lambda();
        wrapper.eq(TCollege::getCollegeId, tCollege.getCollegeId());
        boolean result = itCollegeService.update(tCollege, wrapper);
        return result;
    }

    /**
     * 删除院系数据（软删除）
     * @return
     */
    @ApiOperation(value="删除院系数据（软删除）", notes="tCollegeId字段必填")
    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    public boolean deleteTeacher(int tCollegeId){
        UpdateWrapper<TCollege> updateWrapper = new UpdateWrapper<TCollege>();
        updateWrapper.lambda().eq(TCollege::getCollegeId, tCollegeId);
        TCollege tCollege = new TCollege();
        tCollege.setState(YnEnum.ABANDON.getCode());
        tCollege.setCollegeId(tCollegeId);
        boolean result = itCollegeService.update(tCollege, updateWrapper);
        return result;
    }

}

