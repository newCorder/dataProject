package com.wwx.controller;


import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.command.CreateContainerResponse;
import com.wwx.common.DockerClientService;
import com.wwx.entity.Student;
import com.wwx.service.IStudentService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-06
 */
@Api(description = "学生管理-测试页")
@Controller
@RequestMapping("/wwx/student")
public class StudentController {

    @Autowired
    private IStudentService studentService;

    /**
     * 测试mybatis-puls写法
     * @param pin
     * @return
     */
    @RequestMapping("/hello2")
    @ResponseBody
    public String selectList(String pin) {
        QueryWrapper<Student> queryWrapper = new QueryWrapper<Student>();
        queryWrapper.lambda().eq(Student::getName, "赵一");
        List<Student> result = studentService.list(queryWrapper);
        for (Student s:result) {
            System.out.println(JSON.toJSONString(s));
        }
        String str = JSON.toJSONString(result);
        return str;
    }

    /**
     * 测试页面
     * @param model
     * @return
     */
    @RequestMapping("/hello")
    public String hello(Model model) {
        String vncUrl = "";
        model.addAttribute("userName","小明");
        model.addAttribute("phone","123456");
        model.addAttribute("vncUrl",vncUrl);
        HashMap map = new HashMap();
        return "user";
    }

    @RequestMapping("/test")
    public String test(Model model) {
        String vncUrl = "";
        model.addAttribute("userName","小明");
        model.addAttribute("phone","123456");
        model.addAttribute("vncUrl",vncUrl);
        HashMap<Integer ,Integer> map = new HashMap<Integer ,Integer>();
        HashSet set = new HashSet();
        Iterator it = set.iterator();
        Object en = it.next();
        return "index";
    }

    /**
     * 测试docker连接
     * @return
     */
    @RequestMapping("/connectDocker")
    public String connectDocker(){
        try {
            DockerClientService dockerClientService =new DockerClientService();
//            String url="tcp://192.168.64.1:2375";
            String url="tcp://10.13.154.186:2375";
            //连接docker服务器
            DockerClient client = dockerClientService.connectDocker(url);
            //创建容器
            CreateContainerResponse container = dockerClientService.createContainers(client,"nginx1","nginx:latest");
            //启动容器
            dockerClientService.startContainer(client,container.getId());
            return "ok";
        }catch (Exception e){
            return "error:"+e;
        }
    }
}

