package com.wwx.controller;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wwx.common.ApiResult;
import com.wwx.common.ApiResultMsg;
import com.wwx.common.YnEnum;
import com.wwx.entity.TStudent;
import com.wwx.entity.TStudent;
import com.wwx.service.ITStudentService;
import com.wwx.service.ITStudentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 学生管理
 * @author wuweixu
 * @since 2019-08-12
 */
@Api(description = "学生管理")
@RestController
@RequestMapping("/priAdmin/tStudent")
public class TStudentController {
    private static final Logger LOG = LoggerFactory.getLogger(TStudentController.class);

    @Autowired
    private ITStudentService studentService;

    /**
     * 学生管理查询列表（分页）
     * 根据院系，性别，姓名查询
     * @return
     */
    @ApiOperation(value="学生管理查询列表（分页）", notes="学生管理查询列表（分页）")
    @RequestMapping(value = "/list",method = RequestMethod.GET)
    public ApiResult<IPage<TStudent>> list(TStudent student, int pageNo , int pageSize){
        try{
            QueryWrapper<TStudent> queryWrapper = new QueryWrapper<TStudent>();
            LambdaQueryWrapper<TStudent> q = queryWrapper.lambda();
            if( null != student.getCollegeId() ) {
                q.eq(TStudent::getCollegeId, student.getCollegeId());
            }
            if( null != student.getMajorId() ){
                q.eq(TStudent::getMajorId, student.getMajorId());
            }
            if( null != student.getGradeId() ){
                q.eq(TStudent::getGradeId, student.getGradeId());
            }
            if( null != student.getClassId()){
                q.eq(TStudent::getClassId, student.getClassId());
            }
            if( null != student.getSex() ){
                q.eq(TStudent::getSex, student.getSex());
            }
            if( StringUtils.isNoneBlank(student.getName()) ){
                q.eq(TStudent::getName, student.getName());
            }
            IPage<TStudent> page = new Page<>(pageNo, pageSize);
            IPage<TStudent> result = studentService.page(page, q);
            return ApiResult.getSuccessResult(result);
        }catch (Exception e){
            LOG.error("学生管理查询列表（分页） 异常",e);
            return ApiResult.getFailResult(ApiResultMsg.UNKNOWN_ERROR.getCode(), ApiResultMsg.UNKNOWN_ERROR.getMsg());
        }
    }

    /**
     * 新增学生信息
     * @return
     */
    @ApiOperation(value="新增学生信息", notes="新增学生信息")
    @RequestMapping(value = "/add",method = RequestMethod.POST)
    public boolean addTeacher(TStudent student){
        boolean result = studentService.save(student);
        return result;
    }

    /**
     * 查询单个学生信息
     * @return
     */
    @ApiOperation(value="查询单个学生信息", notes="uid字段必填")
    @RequestMapping(value = "/query", method = RequestMethod.GET)
    public ApiResult<TStudent> queryTeacher(TStudent student){
        TStudent result = studentService.getById(student.getStudentId());
        return ApiResult.getSuccessResult(result);
    }

    /**
     * 修改学生信息
     * @param student 入参：对象中需要有主键id
     * @return
     */
    @ApiOperation(value="修改学生信息", notes="uid字段必填")
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public boolean ediTStudent(TStudent student){
        UpdateWrapper<TStudent> updateWrapper = new UpdateWrapper<TStudent>();
        LambdaUpdateWrapper<TStudent> wrapper = updateWrapper.lambda();
        wrapper.eq(TStudent::getStudentId, student.getStudentId());
        boolean result = studentService.update(student, wrapper);
        return result;
    }

    /**
     * 重置学生密码 默认密码123456
     * @param student 入参：对象中需要有主键id
     * @return
     */
    @ApiOperation(value="重置学生密码", notes="uid字段必填")
    @RequestMapping(value = "/resettingPwd", method = RequestMethod.POST)
    public boolean resettingPwd(TStudent student){
        UpdateWrapper<TStudent> updateWrapper = new UpdateWrapper<TStudent>();
        LambdaUpdateWrapper<TStudent> wrapper = updateWrapper.lambda();
        wrapper.eq(TStudent::getStudentId, student.getStudentId()).set(TStudent::getPwd, "123456");
        boolean result = studentService.update(student, wrapper);
        return result;
    }

    /**
     * 禁用学生数据
     * @return
     */
    @ApiOperation(value="禁用学生数据", notes="uid字段必填")
    @RequestMapping(value = "/forbidden", method = RequestMethod.POST)
    public boolean forbiddenTeacher(TStudent student){
        UpdateWrapper<TStudent> updateWrapper = new UpdateWrapper<TStudent>();
        LambdaUpdateWrapper<TStudent> wrapper = updateWrapper.lambda();
        wrapper.eq(TStudent::getStudentId, student.getStudentId());
        boolean result = studentService.update(student, wrapper);
        return result;
    }

    /**
     * 删除学生数据（软删除）
     * @return
     */
    @ApiOperation(value="删除学生数据（软删除）", notes="uid字段必填")
    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    public boolean deleteTeacher(String uid){
        UpdateWrapper<TStudent> updateWrapper = new UpdateWrapper<TStudent>();
        updateWrapper.lambda().eq(TStudent::getStudentId, uid);
        TStudent student = new TStudent();
        student.setState(YnEnum.ABANDON.getCode());
        student.setStudentId(uid);
        boolean result = studentService.update(student, updateWrapper);
        return result;
    }

    /**
     * 上传学生数据
     * @return
     */
    @ApiOperation(value="上传学生数据（未开发）", notes="")
    @RequestMapping(value = "/uploadTeacher", method = RequestMethod.POST)
    public String uploadTeacher(){
        return null;
    }

    /**
     * 下载学生数据
     * @return
     */
    @ApiOperation(value="下载学生数据（未开发）", notes="")
    @RequestMapping(value = "/downloadTeacher", method = RequestMethod.POST)
    public String downloadTeacher(){
        return null;
    }
}

