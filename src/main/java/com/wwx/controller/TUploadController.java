package com.wwx.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

/**
 * <p>
 * 上传的文件 前端控制器
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
@Controller
@RequestMapping("/wwx/tUpload")
public class TUploadController {

}

