package com.wwx.controller;

import com.alibaba.druid.support.json.JSONUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.wwx.entity.TopicAt;
import com.wwx.mapper.TopicAtMapper;
import com.wwx.service.ITopicAtService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * <p>
 * 评论@信息表 前端控制器
 * </p>
 *
 * @author wuweixu
 * @since 2019-07-29
 */
@Api(description = "测试页")
@Controller
@RequestMapping("/wwx/topicAt")
public class TopicAtController {
    @Autowired
    ITopicAtService iTopicAtService;

//    @RequestMapping("/insert")
//    @ResponseBody
//    public String insert() {
//        //insert
//        TopicAt topicAt = new TopicAt();
//        topicAt.setAtFromPin("juejin1");
//        topicAt.setType(1);
//        topicAt.setAtToPin("juejin2");
//        topicAt.setAtFromId(1L);
//        topicAt.setStatus(1);
//        topicAt.setCreateTime(new Date());
//        boolean res = iTopicAtService.insert(topicAt);
//
//        return res ? "success" : "fail";
//    }

    @RequestMapping("/hello2")
    @ResponseBody
    public String selectList(String pin) {
        QueryWrapper<TopicAt> queryWrapper = new QueryWrapper<TopicAt>();
        queryWrapper.lambda().eq(TopicAt::getAtFromPin, "juejin");
        List<TopicAt> result = iTopicAtService.list(queryWrapper);

        String str = JSONUtils.toJSONString(result);

//        EntityWrapper<TopicAt> ew = new EntityWrapper<TopicAt>();
//        ew.setEntity(new TopicAt());
//        ew.setParamAlias("at_from_pin as atFromPin");
//        ew.setSqlSelect("id,at_from_pin,type,at_to_pin,at_from_id,status,create_time")
//                .where("at_from_pin={0}",pin)
//                .and("type=1")
//                .orderBy("id desc")
//        ;
//        System.out.println(ew.getSqlSegment());
//        List<TopicAt> result = iTopicAtService.selectList(ew);
//        Gson gson = new Gson();
//        String str = gson.toJson(result);

//        String str = JSONUtils.toJSONString(result);
        return str;
    }

    TopicAtMapper topicAtMapper;

//    @RequestMapping("/update")
//    @ResponseBody
//    public String update(String pin) {
//        List<TopicAt> list = topicAtMapper.selectList(new EntityWrapper<TopicAt>().and("type=1"));
//        System.out.println(list);
//        Gson gson = new Gson();
//        String str = gson.toJson(list);
//        return str;
////        iTopicAtService.update();
//    }
}
