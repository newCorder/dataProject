package com.wwx.controller;


import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wwx.common.ApiResult;
import com.wwx.common.ApiResultMsg;
import com.wwx.common.YnEnum;
import com.wwx.entity.TCollege;
import com.wwx.entity.TTeacher;
import com.wwx.service.ITCollegeService;
import com.wwx.service.ITTeacherService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
@Api(description = "管理员权限下的 教师管理功能")
@RestController
@RequestMapping("/priAdmin/teacher")
public class TTeacherController {
    private static final Logger LOG = LoggerFactory.getLogger(TTeacherController.class);

    @Autowired
    private ITTeacherService itTeacherService;

    /**
     * 教师管理查询列表（分页）
     * 根据院系，性别，姓名查询
     * @return
     */
    @ApiOperation(value="教师管理查询列表（分页）", notes="教师管理查询列表（分页）")//swagger
    @RequestMapping(value = "/list",method = RequestMethod.GET)
    public ApiResult<IPage<TTeacher>> teacherList(TTeacher teacher, int pageNo , int pageSize){
        try {
            if ( null == Integer.valueOf(pageNo)  || null == Integer.valueOf(pageSize) ){
                return ApiResult.getFailResult(ApiResultMsg.INVALID_PARAMS.getCode(), ApiResultMsg.INVALID_PARAMS.getMsg());
            }
            QueryWrapper<TTeacher> queryWrapper = new QueryWrapper<TTeacher>();
            LambdaQueryWrapper<TTeacher> q = queryWrapper.lambda();
            if( null != teacher.getCollegeId() ) {
                q.eq(TTeacher::getCollegeId, teacher.getCollegeId());
            }
            if( null != teacher.getSex() ){
                q.eq(TTeacher::getSex, teacher.getSex());
            }
            if( StringUtils.isNoneBlank(teacher.getName()) ){
                q.eq(TTeacher::getName, teacher.getName());
            }
            IPage<TTeacher> page = new Page<>(pageNo, pageSize);
            IPage<TTeacher> tTeacherIPage = itTeacherService.page(page, q);
            return ApiResult.getSuccessResult(tTeacherIPage);
        }catch (Exception e){
            LOG.error("教师管理查询列表（分页） 异常",e);
            return ApiResult.getFailResult(ApiResultMsg.UNKNOWN_ERROR.getCode(), ApiResultMsg.UNKNOWN_ERROR.getMsg());
        }
    }

    /**
     * 新增教师信息
     * @return
     */
    @ApiOperation(value="新增教师信息", notes="新增教师信息")
    @RequestMapping(value = "/add",method = RequestMethod.POST)
    public boolean addTeacher(TTeacher teacher){
        boolean result = itTeacherService.save(teacher);
        return result;
    }

    /**
     * 查询单个教师信息
     * @return
     */
    @ApiOperation(value="查询单个教师信息", notes="uid字段必填")
    @RequestMapping(value = "/query", method = RequestMethod.GET)
    public ApiResult<TTeacher> queryTeacher(TTeacher teacher){
        TTeacher result = itTeacherService.getById(teacher.getTeacherId());
        return ApiResult.getSuccessResult(result);
    }

    /**
     * 修改教师信息
     * @param teacher 入参：对象中需要有主键id
     * @return
     */
    @ApiOperation(value="修改教师信息", notes="uid字段必填")
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public boolean editTeacher(TTeacher teacher){
        UpdateWrapper<TTeacher> updateWrapper = new UpdateWrapper<TTeacher>();
        LambdaUpdateWrapper<TTeacher> wrapper = updateWrapper.lambda();
        wrapper.eq(TTeacher::getTeacherId, teacher.getTeacherId());
        boolean result = itTeacherService.update(teacher, wrapper);
        return result;
    }

    /**
     * 重置教师密码 默认密码123456
     * @param teacher 入参：对象中需要有主键id
     * @return
     */
    @ApiOperation(value="重置教师密码", notes="uid字段必填")
    @RequestMapping(value = "/resettingPwd", method = RequestMethod.POST)
    public boolean resettingPwd(TTeacher teacher){
        UpdateWrapper<TTeacher> updateWrapper = new UpdateWrapper<TTeacher>();
        LambdaUpdateWrapper<TTeacher> wrapper = updateWrapper.lambda();
        wrapper.eq(TTeacher::getTeacherId, teacher.getTeacherId()).set(TTeacher::getPwd, "123456");
        boolean result = itTeacherService.update(teacher, wrapper);
        return result;
    }

    /**
     * 禁用教师数据
     * @return
     */
    @ApiOperation(value="禁用教师数据", notes="uid字段必填")
    @RequestMapping(value = "/forbidden", method = RequestMethod.POST)
    public boolean forbiddenTeacher(TTeacher teacher){
        UpdateWrapper<TTeacher> updateWrapper = new UpdateWrapper<TTeacher>();
        LambdaUpdateWrapper<TTeacher> wrapper = updateWrapper.lambda();
        wrapper.eq(TTeacher::getTeacherId, teacher.getTeacherId());
        boolean result = itTeacherService.update(teacher, wrapper);
        return result;
    }

    /**
     * 删除教师数据（软删除）
     * @return
     */
    @ApiOperation(value="删除教师数据（软删除）", notes="uid字段必填")
    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    public boolean deleteTeacher(String uid){
        UpdateWrapper<TTeacher> updateWrapper = new UpdateWrapper<TTeacher>();
        updateWrapper.lambda().eq(TTeacher::getTeacherId, uid);
        TTeacher teacher = new TTeacher();
        teacher.setState(YnEnum.ABANDON.getCode());
        teacher.setYn(YnEnum.ABANDON.getCode());
        teacher.setTeacherId(uid);
        boolean result = itTeacherService.update(teacher, updateWrapper);
        return result;
    }

    /**
     * 上传教师数据
     * @return
     */
    @ApiOperation(value="上传教师数据（未开发）", notes="")
    @RequestMapping(value = "/uploadTeacher", method = RequestMethod.POST)
    public String uploadTeacher(){
        return null;
    }

    /**
     * 下载教师数据
     * @return
     */
    @ApiOperation(value="下载教师数据（未开发）", notes="")
    @RequestMapping(value = "/downloadTeacher", method = RequestMethod.POST)
    public String downloadTeacher(){
        return null;
    }
}

