package com.wwx.controller;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wwx.common.ApiResult;
import com.wwx.common.ApiResultMsg;
import com.wwx.common.YnEnum;
import com.wwx.entity.TTerm;
import com.wwx.entity.TTerm;
import com.wwx.service.ITTermService;
import com.wwx.service.ITTermService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 学期管理
 * @author wuweixu
 * @since 2019-08-12
 */
@Api(description = "学期管理")
@RestController
@RequestMapping("/priAdmin/tTerm")
public class TTermController {
    private final Logger LOG = LoggerFactory.getLogger(getClass());

    @Autowired
    private ITTermService termService;

    /**
     * 学期管理查询列表（分页）
     * @return
     */
    @ApiOperation(value="学期管理查询列表（分页）", notes="学期管理查询列表（分页）")
    @RequestMapping(value = "/list",method = RequestMethod.GET)
    public ApiResult<IPage<TTerm>> list(TTerm term, int pageNo , int pageSize){
        try{
            QueryWrapper<TTerm> queryWrapper = new QueryWrapper<TTerm>();
            LambdaQueryWrapper<TTerm> q = queryWrapper.lambda();
            if( StringUtils.isNoneBlank(term.getName()) ){
                q.eq(TTerm::getName, term.getName());
            }
            IPage<TTerm> page = new Page<>(pageNo, pageSize);
            IPage<TTerm> result = termService.page(page, q);
            return ApiResult.getSuccessResult(result);
        }catch (Exception e){
            LOG.error("学期管理查询列表（分页） 异常",e);
            return ApiResult.getFailResult(ApiResultMsg.UNKNOWN_ERROR.getCode(), ApiResultMsg.UNKNOWN_ERROR.getMsg());
        }
    }

    /**
     * 新增学期信息
     * @return
     */
    @ApiOperation(value="新增学期信息", notes="新增学期信息")
    @RequestMapping(value = "/add",method = RequestMethod.POST)
    public boolean addTeacher(TTerm term){
        boolean result = termService.save(term);
        return result;
    }

    /**
     * 查询单个学期信息
     * @return
     */
    @ApiOperation(value="查询单个学期信息", notes="uid字段必填")
    @RequestMapping(value = "/query", method = RequestMethod.GET)
    public ApiResult<TTerm> queryTeacher(TTerm term){
        TTerm result = termService.getById(term.getTermId());
        return ApiResult.getSuccessResult(result);
    }

    /**
     * 修改学期信息
     * @param term 入参：对象中需要有主键id
     * @return
     */
    @ApiOperation(value="修改学期信息", notes="uid字段必填")
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public boolean ediTTerm(TTerm term){
        UpdateWrapper<TTerm> updateWrapper = new UpdateWrapper<TTerm>();
        LambdaUpdateWrapper<TTerm> wrapper = updateWrapper.lambda();
        wrapper.eq(TTerm::getTermId, term.getTermId());
        boolean result = termService.update(term, wrapper);
        return result;
    }

    /**
     * 删除学期数据（软删除）
     * @return
     */
    @ApiOperation(value="删除学期数据（软删除）", notes="termId字段必填")
    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    public boolean deleteTeacher(int termId){
        UpdateWrapper<TTerm> updateWrapper = new UpdateWrapper<TTerm>();
        updateWrapper.lambda().eq(TTerm::getTermId, termId);
        TTerm term = new TTerm();
        term.setState(YnEnum.ABANDON.getCode());
        term.setTermId(termId);
        boolean result = termService.update(term, updateWrapper);
        return result;
    }
}

