package com.wwx.controller;

import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author wuweixu
 * @create 2019/8/12
 */
@Api(description = "restful测试页")
@RestController
@RequestMapping("/rest/")
public class RestfulController {

    @RequestMapping(value="/user",method = RequestMethod.GET)
    public String query(@RequestParam(name = "username",required = false,defaultValue = "defaultName") String username){
        System.out.println(username);
        return username;
    }

}
