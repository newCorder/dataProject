package com.wwx.common;

/**
 * 数据可用字段枚举
 * @author wuweixu
 * @create 2019/8/19
 */
public enum YnEnum {

    ABANDON(0,"禁用"),
    NORMAL(1,"启用"),
    ;

    private Integer code;
    private String desc;

    YnEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
