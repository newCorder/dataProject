package com.wwx.common;

import lombok.Data;

/**
 * API 返回结果
 * @param <T>
 */
@Data
public class ApiResult<T> {

    private String code;
    private String msg;
    private T data;

    public static ApiResult getFailResult(String code, String msg) {
        ApiResult result = new ApiResult();
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    public static ApiResult getFailResult(ApiResultMsg codeMsg) {
        ApiResult result = new ApiResult();
        result.setCode(codeMsg.getCode());
        result.setMsg(codeMsg.getMsg());
        return result;
    }

    public static <T> ApiResult<T> getSuccessResult(T data) {
        ApiResult<T> result = new ApiResult<>();
        result.setCode(ApiResultMsg.SUCCESS.getCode());
        result.setMsg(ApiResultMsg.SUCCESS.getMsg());
        result.setData(data);
        return result;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
