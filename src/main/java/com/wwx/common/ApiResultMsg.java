package com.wwx.common;

/**
 * API 枚举
 */
public enum ApiResultMsg {

    UNKNOWN_ERROR("9999", "未知异常"),
    SUCCESS("0000", "成功"),
    FAILED("0001", "失败"),
    INVALID_PARAMS("0002", "参数异常"),


    ;



    ApiResultMsg(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    private String code;
    private String msg;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

}
