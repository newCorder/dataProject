package com.wwx.common;

/**
 * 角色枚举
 * @author wuweixu
 * @create 2019/8/19
 */
public enum RoleEnum {

    STUDENT("0","学生"),
    TEACHER("1","教师"),
    ADMIN("2","管理员"),
    ;

    private String type;
    private String desc;

    RoleEnum(String type, String desc) {
        this.type = type;
        this.desc = desc;
    }

    public static boolean isInclude(String typeStr){
        try {
            for ( RoleEnum roleEnum: RoleEnum.values() ) {
                if ( roleEnum.getType().toString().equals(typeStr) ){
                    return true;
                }
            }
            return false;
        }catch (Exception e){
            return false;
        }
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
