package com.wwx.mapper;

import com.wwx.entity.TConfig;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * system config Mapper 接口
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
public interface TConfigMapper extends BaseMapper<TConfig> {

}
