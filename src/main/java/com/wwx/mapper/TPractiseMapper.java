package com.wwx.mapper;

import com.wwx.entity.TPractise;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 测评信息表 Mapper 接口
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
public interface TPractiseMapper extends BaseMapper<TPractise> {

}
