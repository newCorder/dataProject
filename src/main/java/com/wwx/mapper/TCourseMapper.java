package com.wwx.mapper;

import com.wwx.entity.TCourse;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-21
 */
public interface TCourseMapper extends BaseMapper<TCourse> {

}
