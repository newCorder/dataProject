package com.wwx.mapper;

import com.wwx.entity.TRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 角色表 Mapper 接口
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-19
 */
public interface TRoleMapper extends BaseMapper<TRole> {

}
