package com.wwx.mapper;

import com.wwx.entity.TStudent;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
public interface TStudentMapper extends BaseMapper<TStudent> {

}
