package com.wwx.mapper;

import com.wwx.entity.TSection;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
public interface TSectionMapper extends BaseMapper<TSection> {

}
