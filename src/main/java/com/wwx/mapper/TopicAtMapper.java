package com.wwx.mapper;

import com.wwx.entity.TopicAt;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 评论@信息表 Mapper 接口
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-05
 */
public interface TopicAtMapper extends BaseMapper<TopicAt> {

}
