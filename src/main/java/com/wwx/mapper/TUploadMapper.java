package com.wwx.mapper;

import com.wwx.entity.TUpload;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 上传的文件 Mapper 接口
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
public interface TUploadMapper extends BaseMapper<TUpload> {

}
