package com.wwx.mapper;

import com.wwx.entity.Student;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-06
 */
public interface StudentMapper extends BaseMapper<Student> {

}
