package com.wwx.mapper;

import com.wwx.entity.TTaskState;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
public interface TTaskStateMapper extends BaseMapper<TTaskState> {

}
