package com.wwx.config;

import com.wwx.entity.TStudent;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

/**
 * 自定义用户身份信息
 * <p>
 * Created by
 */
public class MyUserDetails implements UserDetails {
    
    private static final long serialVersionUID = 1L;

    public final static String STATE_ACCOUNTEXPIRED = "1";
    public static final String STATE_LOCK = "1";
    public static final String STATE_TOKENEXPIRED = "1";
    public static final String STATE_NORMAL = "1";

    // 用户信息
    private TStudent user;

    // 用户角色
    private Collection<? extends GrantedAuthority> authorities;

    public MyUserDetails(TStudent user, Collection<? extends GrantedAuthority> authorities) {
        this.user = user;
        this.authorities = authorities;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return this.user.getPwd();
    }

    @Override
    public String getUsername() {
        return this.user.getName();
    }

    @Override
    public boolean isAccountNonExpired() {
        return this.user.getState().equals(STATE_ACCOUNTEXPIRED);
    }

    @Override
    public boolean isAccountNonLocked() {
        return this.user.getState().equals(STATE_LOCK);
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return this.user.getState().equals(STATE_TOKENEXPIRED);
    }

    @Override
    public boolean isEnabled() {
        return this.user.getState().equals(STATE_NORMAL);
    }
}
