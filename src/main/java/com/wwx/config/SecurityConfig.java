package com.wwx.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/**
 * @author wuweixu
 * @create 2019/8/19
 */
//@Configuration
//@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter{

//    @Autowired
//    private AuthenticationProvider provider;  //注入我们自己的AuthenticationProvider

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        // TODO Auto-generated method stub
//        auth.authenticationProvider(provider);

//        http
//            .authorizeRequests()
//                .antMatchers("/css/**", "/index").permitAll()
//                //与/ user / **匹配的请求要求用户进行身份验证，并且必须与USER角色相关联
//                .antMatchers("/wwx/**").hasRole("USER")
//                .and()
//            .formLogin()
//                .and()
//                .csrf().disable() //关闭CSRF
//                .formLogin().loginPage("/login")
//                .loginProcessingUrl("/form")
//                .defaultSuccessUrl("/index") //成功登陆后跳转页面
//                .failureUrl("/loginError").permitAll();
    }
}
