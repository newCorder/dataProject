package com.wwx.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-21
 */
public class TExaminfo implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 学期ID
     */
    @TableId("examId")
    private Integer examId;

    /**
     * 学期名字
     */
    @TableField("examName")
    private String examName;

    /**
     * 班级ID
     */
    @TableField("classId")
    private Integer classId;

    /**
     * 专业ID
     */
    @TableField("majorId")
    private Integer majorId;

    /**
     * 学院ID
     */
    @TableField("collegeId")
    private Integer collegeId;

    /**
     * 发布考试信息的教师ID
     */
    @TableField("teacherId")
    private String teacherId;

    /**
     * 考试封面存储路径

     */
    @TableField("imageUrl")
    private String imageUrl;

    /**
     * 考试描述（简介）
     */
    @TableField("examDsc")
    private String examDsc;

    /**
     * 试卷属性：1表示独占，2表示共享（默认值）
     */
    @TableField("examState")
    private Integer examState;

    /**
     * 考试难度
     */
    @TableField("examLevel")
    private Integer examLevel;

    /**
     * 课程名字
     */
    @TableField("courseName")
    private String courseName;

    /**
     * 课程ID
     */
    @TableField("courseId")
    private Integer courseId;

    /**
     * 试卷ID
     */
    @TableField("paperId")
    private Integer paperId;

    /**
     * 结束时间
     */
    @TableField("endTime")
    private LocalDateTime endTime;

    /**
     * 开始时间
     */
    @TableField("startTime")
    private LocalDateTime startTime;

    /**
     * 年级ID
     */
    @TableField("gradeId")
    private Integer gradeId;


    public Integer getExamId() {
        return examId;
    }

    public TExaminfo setExamId(Integer examId) {
        this.examId = examId;
        return this;
    }

    public String getExamName() {
        return examName;
    }

    public TExaminfo setExamName(String examName) {
        this.examName = examName;
        return this;
    }

    public Integer getClassId() {
        return classId;
    }

    public TExaminfo setClassId(Integer classId) {
        this.classId = classId;
        return this;
    }

    public Integer getMajorId() {
        return majorId;
    }

    public TExaminfo setMajorId(Integer majorId) {
        this.majorId = majorId;
        return this;
    }

    public Integer getCollegeId() {
        return collegeId;
    }

    public TExaminfo setCollegeId(Integer collegeId) {
        this.collegeId = collegeId;
        return this;
    }

    public String getTeacherId() {
        return teacherId;
    }

    public TExaminfo setTeacherId(String teacherId) {
        this.teacherId = teacherId;
        return this;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public TExaminfo setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
        return this;
    }

    public String getExamDsc() {
        return examDsc;
    }

    public TExaminfo setExamDsc(String examDsc) {
        this.examDsc = examDsc;
        return this;
    }

    public Integer getExamState() {
        return examState;
    }

    public TExaminfo setExamState(Integer examState) {
        this.examState = examState;
        return this;
    }

    public Integer getExamLevel() {
        return examLevel;
    }

    public TExaminfo setExamLevel(Integer examLevel) {
        this.examLevel = examLevel;
        return this;
    }

    public String getCourseName() {
        return courseName;
    }

    public TExaminfo setCourseName(String courseName) {
        this.courseName = courseName;
        return this;
    }

    public Integer getCourseId() {
        return courseId;
    }

    public TExaminfo setCourseId(Integer courseId) {
        this.courseId = courseId;
        return this;
    }

    public Integer getPaperId() {
        return paperId;
    }

    public TExaminfo setPaperId(Integer paperId) {
        this.paperId = paperId;
        return this;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public TExaminfo setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
        return this;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public TExaminfo setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
        return this;
    }

    public Integer getGradeId() {
        return gradeId;
    }

    public TExaminfo setGradeId(Integer gradeId) {
        this.gradeId = gradeId;
        return this;
    }

    @Override
    public String toString() {
        return "TExaminfo{" +
        "examId=" + examId +
        ", examName=" + examName +
        ", classId=" + classId +
        ", majorId=" + majorId +
        ", collegeId=" + collegeId +
        ", teacherId=" + teacherId +
        ", imageUrl=" + imageUrl +
        ", examDsc=" + examDsc +
        ", examState=" + examState +
        ", examLevel=" + examLevel +
        ", courseName=" + courseName +
        ", courseId=" + courseId +
        ", paperId=" + paperId +
        ", endTime=" + endTime +
        ", startTime=" + startTime +
        ", gradeId=" + gradeId +
        "}";
    }
}
