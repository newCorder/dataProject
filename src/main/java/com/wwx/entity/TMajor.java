package com.wwx.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
public class TMajor implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "majorId", type = IdType.AUTO)
    private Integer majorId;

    @TableField("collegeId")
    private Integer collegeId;

    private String name;


    public Integer getMajorId() {
        return majorId;
    }

    public TMajor setMajorId(Integer majorId) {
        this.majorId = majorId;
        return this;
    }

    public Integer getCollegeId() {
        return collegeId;
    }

    public TMajor setCollegeId(Integer collegeId) {
        this.collegeId = collegeId;
        return this;
    }

    public String getName() {
        return name;
    }

    public TMajor setName(String name) {
        this.name = name;
        return this;
    }

    @Override
    public String toString() {
        return "TMajor{" +
        "majorId=" + majorId +
        ", collegeId=" + collegeId +
        ", name=" + name +
        "}";
    }
}
