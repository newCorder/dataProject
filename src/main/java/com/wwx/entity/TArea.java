package com.wwx.entity;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
public class TArea implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;

    private String name;

    private Integer parent;


    public Integer getId() {
        return id;
    }

    public TArea setId(Integer id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public TArea setName(String name) {
        this.name = name;
        return this;
    }

    public Integer getParent() {
        return parent;
    }

    public TArea setParent(Integer parent) {
        this.parent = parent;
        return this;
    }

    @Override
    public String toString() {
        return "TArea{" +
        "id=" + id +
        ", name=" + name +
        ", parent=" + parent +
        "}";
    }
}
