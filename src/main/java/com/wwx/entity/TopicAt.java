package com.wwx.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;

/**
 * <p>
 * 评论@信息表
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-05
 */
public class TopicAt implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键iD
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * @发起人
     */
    private String atFromPin;

    /**
     * @类型 0 评论 1回复
     */
    private Integer type;

    /**
     * @谁
     */
    private String atToPin;

    /**
     * @主题id(type为0是评论id,type为1是回复id) 
     */
    private Long atFromId;

    /**
     * 数据状态(0 可用 1已删除)
     */
    private Integer status;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;


    public Long getId() {
        return id;
    }

    public TopicAt setId(Long id) {
        this.id = id;
        return this;
    }

    public String getAtFromPin() {
        return atFromPin;
    }

    public TopicAt setAtFromPin(String atFromPin) {
        this.atFromPin = atFromPin;
        return this;
    }

    public Integer getType() {
        return type;
    }

    public TopicAt setType(Integer type) {
        this.type = type;
        return this;
    }

    public String getAtToPin() {
        return atToPin;
    }

    public TopicAt setAtToPin(String atToPin) {
        this.atToPin = atToPin;
        return this;
    }

    public Long getAtFromId() {
        return atFromId;
    }

    public TopicAt setAtFromId(Long atFromId) {
        this.atFromId = atFromId;
        return this;
    }

    public Integer getStatus() {
        return status;
    }

    public TopicAt setStatus(Integer status) {
        this.status = status;
        return this;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public TopicAt setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
        return this;
    }

    @Override
    public String toString() {
        return "TopicAt{" +
        "id=" + id +
        ", atFromPin=" + atFromPin +
        ", type=" + type +
        ", atToPin=" + atToPin +
        ", atFromId=" + atFromId +
        ", status=" + status +
        ", createTime=" + createTime +
        "}";
    }
}
