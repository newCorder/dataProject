package com.wwx.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
public class TBookCategory implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId("bookId")
    private Integer bookId;

    @TableField("categoryId")
    private Integer categoryId;


    public Integer getBookId() {
        return bookId;
    }

    public TBookCategory setBookId(Integer bookId) {
        this.bookId = bookId;
        return this;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public TBookCategory setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
        return this;
    }

    @Override
    public String toString() {
        return "TBookCategory{" +
        "bookId=" + bookId +
        ", categoryId=" + categoryId +
        "}";
    }
}
