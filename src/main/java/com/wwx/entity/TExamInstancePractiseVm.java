package com.wwx.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
public class TExamInstancePractiseVm implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId("examId")
    private Integer examId;

    @TableField("questionTypeId")
    private Integer questionTypeId;

    @TableField("questionId")
    private Integer questionId;

    @TableField("vmType")
    private Integer vmType;

    @TableField("vmUUID")
    private String vmUUID;

    @TableField("vmName")
    private String vmName;

    @TableField("tagName")
    private String tagName;

    @TableField("vmBtnName")
    private String vmBtnName;

    @TableField("vmSnapshotName")
    private String vmSnapshotName;

    @TableField("restoreSnapshotName")
    private String restoreSnapshotName;


    public Integer getExamId() {
        return examId;
    }

    public TExamInstancePractiseVm setExamId(Integer examId) {
        this.examId = examId;
        return this;
    }

    public Integer getQuestionTypeId() {
        return questionTypeId;
    }

    public TExamInstancePractiseVm setQuestionTypeId(Integer questionTypeId) {
        this.questionTypeId = questionTypeId;
        return this;
    }

    public Integer getQuestionId() {
        return questionId;
    }

    public TExamInstancePractiseVm setQuestionId(Integer questionId) {
        this.questionId = questionId;
        return this;
    }

    public Integer getVmType() {
        return vmType;
    }

    public TExamInstancePractiseVm setVmType(Integer vmType) {
        this.vmType = vmType;
        return this;
    }

    public String getVmUUID() {
        return vmUUID;
    }

    public TExamInstancePractiseVm setVmUUID(String vmUUID) {
        this.vmUUID = vmUUID;
        return this;
    }

    public String getVmName() {
        return vmName;
    }

    public TExamInstancePractiseVm setVmName(String vmName) {
        this.vmName = vmName;
        return this;
    }

    public String getTagName() {
        return tagName;
    }

    public TExamInstancePractiseVm setTagName(String tagName) {
        this.tagName = tagName;
        return this;
    }

    public String getVmBtnName() {
        return vmBtnName;
    }

    public TExamInstancePractiseVm setVmBtnName(String vmBtnName) {
        this.vmBtnName = vmBtnName;
        return this;
    }

    public String getVmSnapshotName() {
        return vmSnapshotName;
    }

    public TExamInstancePractiseVm setVmSnapshotName(String vmSnapshotName) {
        this.vmSnapshotName = vmSnapshotName;
        return this;
    }

    public String getRestoreSnapshotName() {
        return restoreSnapshotName;
    }

    public TExamInstancePractiseVm setRestoreSnapshotName(String restoreSnapshotName) {
        this.restoreSnapshotName = restoreSnapshotName;
        return this;
    }

    @Override
    public String toString() {
        return "TExamInstancePractiseVm{" +
        "examId=" + examId +
        ", questionTypeId=" + questionTypeId +
        ", questionId=" + questionId +
        ", vmType=" + vmType +
        ", vmUUID=" + vmUUID +
        ", vmName=" + vmName +
        ", tagName=" + tagName +
        ", vmBtnName=" + vmBtnName +
        ", vmSnapshotName=" + vmSnapshotName +
        ", restoreSnapshotName=" + restoreSnapshotName +
        "}";
    }
}
