package com.wwx.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
public class TBookJob implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId("jobId")
    private Integer jobId;

    @TableField("bookId")
    private Integer bookId;

    /**
     * 岗位课程类别： 1基础课，2专业课
            另外：书籍课程类型bookType： 1核心课 2 实训案例
     */
    @TableField("bookCategory")
    private Integer bookCategory;


    public Integer getJobId() {
        return jobId;
    }

    public TBookJob setJobId(Integer jobId) {
        this.jobId = jobId;
        return this;
    }

    public Integer getBookId() {
        return bookId;
    }

    public TBookJob setBookId(Integer bookId) {
        this.bookId = bookId;
        return this;
    }

    public Integer getBookCategory() {
        return bookCategory;
    }

    public TBookJob setBookCategory(Integer bookCategory) {
        this.bookCategory = bookCategory;
        return this;
    }

    @Override
    public String toString() {
        return "TBookJob{" +
        "jobId=" + jobId +
        ", bookId=" + bookId +
        ", bookCategory=" + bookCategory +
        "}";
    }
}
