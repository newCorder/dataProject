package com.wwx.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
public class TReportScreenshot implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "sid", type = IdType.AUTO)
    private Integer sid;

    @TableField("stepId")
    private Integer stepId;

    private String name;

    private String screenshoturl;


    public Integer getSid() {
        return sid;
    }

    public TReportScreenshot setSid(Integer sid) {
        this.sid = sid;
        return this;
    }

    public Integer getStepId() {
        return stepId;
    }

    public TReportScreenshot setStepId(Integer stepId) {
        this.stepId = stepId;
        return this;
    }

    public String getName() {
        return name;
    }

    public TReportScreenshot setName(String name) {
        this.name = name;
        return this;
    }

    public String getScreenshoturl() {
        return screenshoturl;
    }

    public TReportScreenshot setScreenshoturl(String screenshoturl) {
        this.screenshoturl = screenshoturl;
        return this;
    }

    @Override
    public String toString() {
        return "TReportScreenshot{" +
        "sid=" + sid +
        ", stepId=" + stepId +
        ", name=" + name +
        ", screenshoturl=" + screenshoturl +
        "}";
    }
}
