package com.wwx.entity;

import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
public class TAdmin implements Serializable {

    private static final long serialVersionUID = 1L;

    private String uid;

    private String name;

    private String pwd;

    /**
     * 每个用户的密码盐不一样。
     */
    private String salt;

    @TableField("updateTime")
    private LocalDateTime updateTime;

    private LocalDateTime logintime;

    /**
     * 权限相关
     */
    private Integer roleid;

    /**
     * 0 禁用 1 启用
     */
    private Integer state;


    public String getUid() {
        return uid;
    }

    public TAdmin setUid(String uid) {
        this.uid = uid;
        return this;
    }

    public String getName() {
        return name;
    }

    public TAdmin setName(String name) {
        this.name = name;
        return this;
    }

    public String getPwd() {
        return pwd;
    }

    public TAdmin setPwd(String pwd) {
        this.pwd = pwd;
        return this;
    }

    public String getSalt() {
        return salt;
    }

    public TAdmin setSalt(String salt) {
        this.salt = salt;
        return this;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public TAdmin setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
        return this;
    }

    public LocalDateTime getLogintime() {
        return logintime;
    }

    public TAdmin setLogintime(LocalDateTime logintime) {
        this.logintime = logintime;
        return this;
    }

    public Integer getRoleid() {
        return roleid;
    }

    public TAdmin setRoleid(Integer roleid) {
        this.roleid = roleid;
        return this;
    }

    public Integer getState() {
        return state;
    }

    public TAdmin setState(Integer state) {
        this.state = state;
        return this;
    }

    @Override
    public String toString() {
        return "TAdmin{" +
        "uid=" + uid +
        ", name=" + name +
        ", pwd=" + pwd +
        ", salt=" + salt +
        ", updateTime=" + updateTime +
        ", logintime=" + logintime +
        ", roleid=" + roleid +
        ", state=" + state +
        "}";
    }
}
