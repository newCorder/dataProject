package com.wwx.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
public class TPlanSectionVmTask implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId("planId")
    private Integer planId;

    @TableField("sectionId")
    private Integer sectionId;

    @TableField("vmType")
    private Integer vmType;

    @TableField("vmUUID")
    private String vmUUID;

    @TableField("vmName")
    private String vmName;

    @TableField("tagName")
    private String tagName;

    @TableField("taskId")
    private Integer taskId;

    private String name;

    private String dsc;

    private String tip;

    private String rule;


    public Integer getPlanId() {
        return planId;
    }

    public TPlanSectionVmTask setPlanId(Integer planId) {
        this.planId = planId;
        return this;
    }

    public Integer getSectionId() {
        return sectionId;
    }

    public TPlanSectionVmTask setSectionId(Integer sectionId) {
        this.sectionId = sectionId;
        return this;
    }

    public Integer getVmType() {
        return vmType;
    }

    public TPlanSectionVmTask setVmType(Integer vmType) {
        this.vmType = vmType;
        return this;
    }

    public String getVmUUID() {
        return vmUUID;
    }

    public TPlanSectionVmTask setVmUUID(String vmUUID) {
        this.vmUUID = vmUUID;
        return this;
    }

    public String getVmName() {
        return vmName;
    }

    public TPlanSectionVmTask setVmName(String vmName) {
        this.vmName = vmName;
        return this;
    }

    public String getTagName() {
        return tagName;
    }

    public TPlanSectionVmTask setTagName(String tagName) {
        this.tagName = tagName;
        return this;
    }

    public Integer getTaskId() {
        return taskId;
    }

    public TPlanSectionVmTask setTaskId(Integer taskId) {
        this.taskId = taskId;
        return this;
    }

    public String getName() {
        return name;
    }

    public TPlanSectionVmTask setName(String name) {
        this.name = name;
        return this;
    }

    public String getDsc() {
        return dsc;
    }

    public TPlanSectionVmTask setDsc(String dsc) {
        this.dsc = dsc;
        return this;
    }

    public String getTip() {
        return tip;
    }

    public TPlanSectionVmTask setTip(String tip) {
        this.tip = tip;
        return this;
    }

    public String getRule() {
        return rule;
    }

    public TPlanSectionVmTask setRule(String rule) {
        this.rule = rule;
        return this;
    }

    @Override
    public String toString() {
        return "TPlanSectionVmTask{" +
        "planId=" + planId +
        ", sectionId=" + sectionId +
        ", vmType=" + vmType +
        ", vmUUID=" + vmUUID +
        ", vmName=" + vmName +
        ", tagName=" + tagName +
        ", taskId=" + taskId +
        ", name=" + name +
        ", dsc=" + dsc +
        ", tip=" + tip +
        ", rule=" + rule +
        "}";
    }
}
