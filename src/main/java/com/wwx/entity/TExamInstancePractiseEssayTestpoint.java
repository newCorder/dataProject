package com.wwx.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
public class TExamInstancePractiseEssayTestpoint implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "testpointId", type = IdType.AUTO)
    private Integer testpointId;

    @TableField("examId")
    private Integer examId;

    @TableField("questionTypeId")
    private Integer questionTypeId;

    @TableField("questionId")
    private Integer questionId;

    @TableField("testpointName")
    private String testpointName;

    private String answer;

    private String analyse;


    public Integer getTestpointId() {
        return testpointId;
    }

    public TExamInstancePractiseEssayTestpoint setTestpointId(Integer testpointId) {
        this.testpointId = testpointId;
        return this;
    }

    public Integer getExamId() {
        return examId;
    }

    public TExamInstancePractiseEssayTestpoint setExamId(Integer examId) {
        this.examId = examId;
        return this;
    }

    public Integer getQuestionTypeId() {
        return questionTypeId;
    }

    public TExamInstancePractiseEssayTestpoint setQuestionTypeId(Integer questionTypeId) {
        this.questionTypeId = questionTypeId;
        return this;
    }

    public Integer getQuestionId() {
        return questionId;
    }

    public TExamInstancePractiseEssayTestpoint setQuestionId(Integer questionId) {
        this.questionId = questionId;
        return this;
    }

    public String getTestpointName() {
        return testpointName;
    }

    public TExamInstancePractiseEssayTestpoint setTestpointName(String testpointName) {
        this.testpointName = testpointName;
        return this;
    }

    public String getAnswer() {
        return answer;
    }

    public TExamInstancePractiseEssayTestpoint setAnswer(String answer) {
        this.answer = answer;
        return this;
    }

    public String getAnalyse() {
        return analyse;
    }

    public TExamInstancePractiseEssayTestpoint setAnalyse(String analyse) {
        this.analyse = analyse;
        return this;
    }

    @Override
    public String toString() {
        return "TExamInstancePractiseEssayTestpoint{" +
        "testpointId=" + testpointId +
        ", examId=" + examId +
        ", questionTypeId=" + questionTypeId +
        ", questionId=" + questionId +
        ", testpointName=" + testpointName +
        ", answer=" + answer +
        ", analyse=" + analyse +
        "}";
    }
}
