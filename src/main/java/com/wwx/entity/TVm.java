package com.wwx.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
public class TVm implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 章节使用的虚拟机类型： 0（无虚拟机）， 1（VB）， 2（docker）； 默认1
     */
    @TableId("vmType")
    private Integer vmType;

    @TableField("vmUUID")
    private String vmUUID;

    @TableField("vmName")
    private String vmName;

    @TableField("tagName")
    private String tagName;

    @TableField("requireVMUUID")
    private String requireVMUUID;


    public Integer getVmType() {
        return vmType;
    }

    public TVm setVmType(Integer vmType) {
        this.vmType = vmType;
        return this;
    }

    public String getVmUUID() {
        return vmUUID;
    }

    public TVm setVmUUID(String vmUUID) {
        this.vmUUID = vmUUID;
        return this;
    }

    public String getVmName() {
        return vmName;
    }

    public TVm setVmName(String vmName) {
        this.vmName = vmName;
        return this;
    }

    public String getTagName() {
        return tagName;
    }

    public TVm setTagName(String tagName) {
        this.tagName = tagName;
        return this;
    }

    public String getRequireVMUUID() {
        return requireVMUUID;
    }

    public TVm setRequireVMUUID(String requireVMUUID) {
        this.requireVMUUID = requireVMUUID;
        return this;
    }

    @Override
    public String toString() {
        return "TVm{" +
        "vmType=" + vmType +
        ", vmUUID=" + vmUUID +
        ", vmName=" + vmName +
        ", tagName=" + tagName +
        ", requireVMUUID=" + requireVMUUID +
        "}";
    }
}
