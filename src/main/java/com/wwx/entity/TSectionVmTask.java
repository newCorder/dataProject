package com.wwx.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
public class TSectionVmTask implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId("vmUUID")
    private String vmUUID;

    @TableField("vmType")
    private Integer vmType;

    /**
     * docker 模式使用
     */
    @TableField("tagName")
    private String tagName;

    @TableField("sectionId")
    private Integer sectionId;

    @TableField("taskId")
    private Integer taskId;

    @TableField("belongTo")
    private String belongTo;


    public String getVmUUID() {
        return vmUUID;
    }

    public TSectionVmTask setVmUUID(String vmUUID) {
        this.vmUUID = vmUUID;
        return this;
    }

    public Integer getVmType() {
        return vmType;
    }

    public TSectionVmTask setVmType(Integer vmType) {
        this.vmType = vmType;
        return this;
    }

    public String getTagName() {
        return tagName;
    }

    public TSectionVmTask setTagName(String tagName) {
        this.tagName = tagName;
        return this;
    }

    public Integer getSectionId() {
        return sectionId;
    }

    public TSectionVmTask setSectionId(Integer sectionId) {
        this.sectionId = sectionId;
        return this;
    }

    public Integer getTaskId() {
        return taskId;
    }

    public TSectionVmTask setTaskId(Integer taskId) {
        this.taskId = taskId;
        return this;
    }

    public String getBelongTo() {
        return belongTo;
    }

    public TSectionVmTask setBelongTo(String belongTo) {
        this.belongTo = belongTo;
        return this;
    }

    @Override
    public String toString() {
        return "TSectionVmTask{" +
        "vmUUID=" + vmUUID +
        ", vmType=" + vmType +
        ", tagName=" + tagName +
        ", sectionId=" + sectionId +
        ", taskId=" + taskId +
        ", belongTo=" + belongTo +
        "}";
    }
}
