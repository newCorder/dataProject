package com.wwx.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
public class TExamInstancePractiseVmTask implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId("examId")
    private Integer examId;

    @TableField("questionTypeId")
    private Integer questionTypeId;

    @TableField("questionId")
    private Integer questionId;

    @TableField("vmType")
    private Integer vmType;

    @TableField("vmUUID")
    private String vmUUID;

    @TableField("vmName")
    private String vmName;

    @TableField("tagName")
    private String tagName;

    @TableField("taskId")
    private Integer taskId;

    private String name;

    private String dsc;

    private String tip;

    private String rule;


    public Integer getExamId() {
        return examId;
    }

    public TExamInstancePractiseVmTask setExamId(Integer examId) {
        this.examId = examId;
        return this;
    }

    public Integer getQuestionTypeId() {
        return questionTypeId;
    }

    public TExamInstancePractiseVmTask setQuestionTypeId(Integer questionTypeId) {
        this.questionTypeId = questionTypeId;
        return this;
    }

    public Integer getQuestionId() {
        return questionId;
    }

    public TExamInstancePractiseVmTask setQuestionId(Integer questionId) {
        this.questionId = questionId;
        return this;
    }

    public Integer getVmType() {
        return vmType;
    }

    public TExamInstancePractiseVmTask setVmType(Integer vmType) {
        this.vmType = vmType;
        return this;
    }

    public String getVmUUID() {
        return vmUUID;
    }

    public TExamInstancePractiseVmTask setVmUUID(String vmUUID) {
        this.vmUUID = vmUUID;
        return this;
    }

    public String getVmName() {
        return vmName;
    }

    public TExamInstancePractiseVmTask setVmName(String vmName) {
        this.vmName = vmName;
        return this;
    }

    public String getTagName() {
        return tagName;
    }

    public TExamInstancePractiseVmTask setTagName(String tagName) {
        this.tagName = tagName;
        return this;
    }

    public Integer getTaskId() {
        return taskId;
    }

    public TExamInstancePractiseVmTask setTaskId(Integer taskId) {
        this.taskId = taskId;
        return this;
    }

    public String getName() {
        return name;
    }

    public TExamInstancePractiseVmTask setName(String name) {
        this.name = name;
        return this;
    }

    public String getDsc() {
        return dsc;
    }

    public TExamInstancePractiseVmTask setDsc(String dsc) {
        this.dsc = dsc;
        return this;
    }

    public String getTip() {
        return tip;
    }

    public TExamInstancePractiseVmTask setTip(String tip) {
        this.tip = tip;
        return this;
    }

    public String getRule() {
        return rule;
    }

    public TExamInstancePractiseVmTask setRule(String rule) {
        this.rule = rule;
        return this;
    }

    @Override
    public String toString() {
        return "TExamInstancePractiseVmTask{" +
        "examId=" + examId +
        ", questionTypeId=" + questionTypeId +
        ", questionId=" + questionId +
        ", vmType=" + vmType +
        ", vmUUID=" + vmUUID +
        ", vmName=" + vmName +
        ", tagName=" + tagName +
        ", taskId=" + taskId +
        ", name=" + name +
        ", dsc=" + dsc +
        ", tip=" + tip +
        ", rule=" + rule +
        "}";
    }
}
