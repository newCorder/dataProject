package com.wwx.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-21
 */
public class TPractisePaper implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 测评卷ID
     */
    @TableId("practisePaperId")
    private Integer practisePaperId;

    /**
     * 课程ID
     */
    @TableField("courseId")
    private Integer courseId;

    /**
     * 测评卷名称
     */
    private String name;

    /**
     * 作者
     */
    private String author;

    /**
     * 创建的老师ID
     */
    @TableField("teacherId")
    private String teacherId;

    /**
     * 测评创建时间
     */
    @TableField("createTime")
    private LocalDateTime createTime;

    /**
     * 测评简介
     */
    private String dsc;

    /**
     * 测评难度
     */
    private Integer level;

    /**
     * 测评封面路径
     */
    @TableField("imageUrl")
    private String imageUrl;

    /**
     * 测评更新时间
     */
    @TableField("updateTime")
    private LocalDateTime updateTime;


    public Integer getPractisePaperId() {
        return practisePaperId;
    }

    public TPractisePaper setPractisePaperId(Integer practisePaperId) {
        this.practisePaperId = practisePaperId;
        return this;
    }

    public Integer getCourseId() {
        return courseId;
    }

    public TPractisePaper setCourseId(Integer courseId) {
        this.courseId = courseId;
        return this;
    }

    public String getName() {
        return name;
    }

    public TPractisePaper setName(String name) {
        this.name = name;
        return this;
    }

    public String getAuthor() {
        return author;
    }

    public TPractisePaper setAuthor(String author) {
        this.author = author;
        return this;
    }

    public String getTeacherId() {
        return teacherId;
    }

    public TPractisePaper setTeacherId(String teacherId) {
        this.teacherId = teacherId;
        return this;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public TPractisePaper setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
        return this;
    }

    public String getDsc() {
        return dsc;
    }

    public TPractisePaper setDsc(String dsc) {
        this.dsc = dsc;
        return this;
    }

    public Integer getLevel() {
        return level;
    }

    public TPractisePaper setLevel(Integer level) {
        this.level = level;
        return this;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public TPractisePaper setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
        return this;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public TPractisePaper setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
        return this;
    }

    @Override
    public String toString() {
        return "TPractisePaper{" +
        "practisePaperId=" + practisePaperId +
        ", courseId=" + courseId +
        ", name=" + name +
        ", author=" + author +
        ", teacherId=" + teacherId +
        ", createTime=" + createTime +
        ", dsc=" + dsc +
        ", level=" + level +
        ", imageUrl=" + imageUrl +
        ", updateTime=" + updateTime +
        "}";
    }
}
