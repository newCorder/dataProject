package com.wwx.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 测评信息表
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
public class TPractise implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 1单选，2多选， 3填空， 4问答， 5程序， 6实操
     */
    @TableId("questionTypeId")
    private Integer questionTypeId;

    @TableField("questionId")
    private Integer questionId;

    /**
     * 富文本题目
     */
    @TableField("questionContent")
    private String questionContent;

    /**
     * 答案选项json字符串：‘[{"name":"A","content":"edits"},{"name":"B","content":"NameNode"},{"name":"C","content":"DateNode"},{"name":"D","content":"fsimage"}]’
     */
    @TableField("answerOptions")
    private String answerOptions;

    /**
     * 表示填空个数
     */
    @TableField("questionNumber")
    private Integer questionNumber;

    /**
     * 正确答案： A（单选）；A,B,C（多选）；：['填空1‘, '填空2‘]（填空）
     */
    private String answer;

    /**
     * 答案解析
     */
    private String analyse;


    public Integer getQuestionTypeId() {
        return questionTypeId;
    }

    public TPractise setQuestionTypeId(Integer questionTypeId) {
        this.questionTypeId = questionTypeId;
        return this;
    }

    public Integer getQuestionId() {
        return questionId;
    }

    public TPractise setQuestionId(Integer questionId) {
        this.questionId = questionId;
        return this;
    }

    public String getQuestionContent() {
        return questionContent;
    }

    public TPractise setQuestionContent(String questionContent) {
        this.questionContent = questionContent;
        return this;
    }

    public String getAnswerOptions() {
        return answerOptions;
    }

    public TPractise setAnswerOptions(String answerOptions) {
        this.answerOptions = answerOptions;
        return this;
    }

    public Integer getQuestionNumber() {
        return questionNumber;
    }

    public TPractise setQuestionNumber(Integer questionNumber) {
        this.questionNumber = questionNumber;
        return this;
    }

    public String getAnswer() {
        return answer;
    }

    public TPractise setAnswer(String answer) {
        this.answer = answer;
        return this;
    }

    public String getAnalyse() {
        return analyse;
    }

    public TPractise setAnalyse(String analyse) {
        this.analyse = analyse;
        return this;
    }

    @Override
    public String toString() {
        return "TPractise{" +
        "questionTypeId=" + questionTypeId +
        ", questionId=" + questionId +
        ", questionContent=" + questionContent +
        ", answerOptions=" + answerOptions +
        ", questionNumber=" + questionNumber +
        ", answer=" + answer +
        ", analyse=" + analyse +
        "}";
    }
}
