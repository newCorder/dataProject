package com.wwx.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
public class TScoreExam implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @TableField("examId")
    private Integer examId;

    private String uid;

    @TableField("classId")
    private Integer classId;

    /**
     *  0作答中， 1 已提交 ，2 已评分
     */
    @TableField("practiseState")
    private Integer practiseState;

    @TableField("practiseAutoScore")
    private Float practiseAutoScore;

    @TableField("practiseManualScore")
    private Float practiseManualScore;

    @TableField("practiseSubmitTime")
    private LocalDateTime practiseSubmitTime;

    @TableField("practiseScoreTime")
    private LocalDateTime practiseScoreTime;


    public Integer getId() {
        return id;
    }

    public TScoreExam setId(Integer id) {
        this.id = id;
        return this;
    }

    public Integer getExamId() {
        return examId;
    }

    public TScoreExam setExamId(Integer examId) {
        this.examId = examId;
        return this;
    }

    public String getUid() {
        return uid;
    }

    public TScoreExam setUid(String uid) {
        this.uid = uid;
        return this;
    }

    public Integer getClassId() {
        return classId;
    }

    public TScoreExam setClassId(Integer classId) {
        this.classId = classId;
        return this;
    }

    public Integer getPractiseState() {
        return practiseState;
    }

    public TScoreExam setPractiseState(Integer practiseState) {
        this.practiseState = practiseState;
        return this;
    }

    public Float getPractiseAutoScore() {
        return practiseAutoScore;
    }

    public TScoreExam setPractiseAutoScore(Float practiseAutoScore) {
        this.practiseAutoScore = practiseAutoScore;
        return this;
    }

    public Float getPractiseManualScore() {
        return practiseManualScore;
    }

    public TScoreExam setPractiseManualScore(Float practiseManualScore) {
        this.practiseManualScore = practiseManualScore;
        return this;
    }

    public LocalDateTime getPractiseSubmitTime() {
        return practiseSubmitTime;
    }

    public TScoreExam setPractiseSubmitTime(LocalDateTime practiseSubmitTime) {
        this.practiseSubmitTime = practiseSubmitTime;
        return this;
    }

    public LocalDateTime getPractiseScoreTime() {
        return practiseScoreTime;
    }

    public TScoreExam setPractiseScoreTime(LocalDateTime practiseScoreTime) {
        this.practiseScoreTime = practiseScoreTime;
        return this;
    }

    @Override
    public String toString() {
        return "TScoreExam{" +
        "id=" + id +
        ", examId=" + examId +
        ", uid=" + uid +
        ", classId=" + classId +
        ", practiseState=" + practiseState +
        ", practiseAutoScore=" + practiseAutoScore +
        ", practiseManualScore=" + practiseManualScore +
        ", practiseSubmitTime=" + practiseSubmitTime +
        ", practiseScoreTime=" + practiseScoreTime +
        "}";
    }
}
