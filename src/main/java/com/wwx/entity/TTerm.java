package com.wwx.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

/**
 * 学期表
 * @author wuweixu
 * @since 2019-08-12
 */
public class TTerm implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 学期ID
     */
    @TableId(value = "termId", type = IdType.AUTO)
    private Integer termId;
    /**
     * 学期名字
     */
    private String name;
    /**
     * 0 禁用 1 启用
     */
    private Integer state;

    public Integer getTermId() {
        return termId;
    }

    public TTerm setTermId(Integer termId) {
        this.termId = termId;
        return this;
    }

    public String getName() {
        return name;
    }

    public TTerm setName(String name) {
        this.name = name;
        return this;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }
}
