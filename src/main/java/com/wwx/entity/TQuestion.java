package com.wwx.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-21
 */
public class TQuestion implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 正确答案（A（单选）；A,B,C（多选）；[‘填空1’,‘填空2’]（填空））
     */
    private String answer;

    /**
     * 表示填空个数
     */
    @TableField("questionNumber")
    private Integer questionNumber;

    /**
     * 题目类型（1单选，2多选， 3填空， 4问答）
     */
    @TableField("questionTypeId")
    private Integer questionTypeId;

    /**
     * 题目ID
     */
    @TableId("questionId")
    private Integer questionId;

    /**
     * 答案选项json字符串：‘[{"name":"A","content":"edits"},{"name":"B","content":"NameNode"},{"name":"C","content":"DateNode"},{"name":"D","content":"fsimage"}]’
     */
    @TableField("answerOptions")
    private String answerOptions;

    /**
     * 题目文本
     */
    @TableField("questionContent")
    private String questionContent;

    /**
     * 答案解析
     */
    private String analyse;


    public String getAnswer() {
        return answer;
    }

    public TQuestion setAnswer(String answer) {
        this.answer = answer;
        return this;
    }

    public Integer getQuestionNumber() {
        return questionNumber;
    }

    public TQuestion setQuestionNumber(Integer questionNumber) {
        this.questionNumber = questionNumber;
        return this;
    }

    public Integer getQuestionTypeId() {
        return questionTypeId;
    }

    public TQuestion setQuestionTypeId(Integer questionTypeId) {
        this.questionTypeId = questionTypeId;
        return this;
    }

    public Integer getQuestionId() {
        return questionId;
    }

    public TQuestion setQuestionId(Integer questionId) {
        this.questionId = questionId;
        return this;
    }

    public String getAnswerOptions() {
        return answerOptions;
    }

    public TQuestion setAnswerOptions(String answerOptions) {
        this.answerOptions = answerOptions;
        return this;
    }

    public String getQuestionContent() {
        return questionContent;
    }

    public TQuestion setQuestionContent(String questionContent) {
        this.questionContent = questionContent;
        return this;
    }

    public String getAnalyse() {
        return analyse;
    }

    public TQuestion setAnalyse(String analyse) {
        this.analyse = analyse;
        return this;
    }

    @Override
    public String toString() {
        return "TQuestion{" +
        "answer=" + answer +
        ", questionNumber=" + questionNumber +
        ", questionTypeId=" + questionTypeId +
        ", questionId=" + questionId +
        ", answerOptions=" + answerOptions +
        ", questionContent=" + questionContent +
        ", analyse=" + analyse +
        "}";
    }
}
