package com.wwx.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
public class TAuthRecord implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 32位16进制授权码
     */
    @TableId("authCode")
    private String authCode;

    /**
     * 确保授权信息完整性32位16进制
     */
    private String sign;

    /**
     * 32位16进制机器特征码
     */
    @TableField("featureCode")
    private String featureCode;

    /**
     * 允许同时在线的数量
     */
    @TableField("onlineCount")
    private Integer onlineCount;

    /**
     * 使用时长： -1无限制，其他分钟数
     */
    @TableField("timeLimit")
    private Integer timeLimit;

    /**
     * 授权类型： 1在线授权， 2离线授权
     */
    @TableField("authType")
    private Integer authType;

    /**
     * 授权算法版本
     */
    private String version;

    private LocalDateTime time;


    public String getAuthCode() {
        return authCode;
    }

    public TAuthRecord setAuthCode(String authCode) {
        this.authCode = authCode;
        return this;
    }

    public String getSign() {
        return sign;
    }

    public TAuthRecord setSign(String sign) {
        this.sign = sign;
        return this;
    }

    public String getFeatureCode() {
        return featureCode;
    }

    public TAuthRecord setFeatureCode(String featureCode) {
        this.featureCode = featureCode;
        return this;
    }

    public Integer getOnlineCount() {
        return onlineCount;
    }

    public TAuthRecord setOnlineCount(Integer onlineCount) {
        this.onlineCount = onlineCount;
        return this;
    }

    public Integer getTimeLimit() {
        return timeLimit;
    }

    public TAuthRecord setTimeLimit(Integer timeLimit) {
        this.timeLimit = timeLimit;
        return this;
    }

    public Integer getAuthType() {
        return authType;
    }

    public TAuthRecord setAuthType(Integer authType) {
        this.authType = authType;
        return this;
    }

    public String getVersion() {
        return version;
    }

    public TAuthRecord setVersion(String version) {
        this.version = version;
        return this;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public TAuthRecord setTime(LocalDateTime time) {
        this.time = time;
        return this;
    }

    @Override
    public String toString() {
        return "TAuthRecord{" +
        "authCode=" + authCode +
        ", sign=" + sign +
        ", featureCode=" + featureCode +
        ", onlineCount=" + onlineCount +
        ", timeLimit=" + timeLimit +
        ", authType=" + authType +
        ", version=" + version +
        ", time=" + time +
        "}";
    }
}
