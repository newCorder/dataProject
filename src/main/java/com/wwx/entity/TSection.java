package com.wwx.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
public class TSection implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "sectionId", type = IdType.AUTO)
    private Integer sectionId;

    @TableField("bookId")
    private Integer bookId;

    @TableField("sectionName")
    private String sectionName;

    private String pdf;

    private String html;

    @TableField("resName")
    private String resName;

    /**
     * 上传资源的原始路径，zip，ppt，doc等（转换成html，pdf）
     */
    @TableField("orgResPath")
    private String orgResPath;

    private String video;

    @TableField("videoName")
    private String videoName;

    /**
     * 建议课时
     */
    @TableField("suggestTime")
    private Integer suggestTime;

    /**
     * 实验目的
     */
    private String purpose;

    /**
     * 实验内容
     */
    private String content;


    public Integer getSectionId() {
        return sectionId;
    }

    public TSection setSectionId(Integer sectionId) {
        this.sectionId = sectionId;
        return this;
    }

    public Integer getBookId() {
        return bookId;
    }

    public TSection setBookId(Integer bookId) {
        this.bookId = bookId;
        return this;
    }

    public String getSectionName() {
        return sectionName;
    }

    public TSection setSectionName(String sectionName) {
        this.sectionName = sectionName;
        return this;
    }

    public String getPdf() {
        return pdf;
    }

    public TSection setPdf(String pdf) {
        this.pdf = pdf;
        return this;
    }

    public String getHtml() {
        return html;
    }

    public TSection setHtml(String html) {
        this.html = html;
        return this;
    }

    public String getResName() {
        return resName;
    }

    public TSection setResName(String resName) {
        this.resName = resName;
        return this;
    }

    public String getOrgResPath() {
        return orgResPath;
    }

    public TSection setOrgResPath(String orgResPath) {
        this.orgResPath = orgResPath;
        return this;
    }

    public String getVideo() {
        return video;
    }

    public TSection setVideo(String video) {
        this.video = video;
        return this;
    }

    public String getVideoName() {
        return videoName;
    }

    public TSection setVideoName(String videoName) {
        this.videoName = videoName;
        return this;
    }

    public Integer getSuggestTime() {
        return suggestTime;
    }

    public TSection setSuggestTime(Integer suggestTime) {
        this.suggestTime = suggestTime;
        return this;
    }

    public String getPurpose() {
        return purpose;
    }

    public TSection setPurpose(String purpose) {
        this.purpose = purpose;
        return this;
    }

    public String getContent() {
        return content;
    }

    public TSection setContent(String content) {
        this.content = content;
        return this;
    }

    @Override
    public String toString() {
        return "TSection{" +
        "sectionId=" + sectionId +
        ", bookId=" + bookId +
        ", sectionName=" + sectionName +
        ", pdf=" + pdf +
        ", html=" + html +
        ", resName=" + resName +
        ", orgResPath=" + orgResPath +
        ", video=" + video +
        ", videoName=" + videoName +
        ", suggestTime=" + suggestTime +
        ", purpose=" + purpose +
        ", content=" + content +
        "}";
    }
}
