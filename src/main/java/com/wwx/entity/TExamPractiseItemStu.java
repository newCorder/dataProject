package com.wwx.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
public class TExamPractiseItemStu implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId("examId")
    private Integer examId;

    private String uid;

    @TableField("configId")
    private Integer configId;

    @TableField("autoScore")
    private Float autoScore;

    @TableField("manualScore")
    private Float manualScore;

    @TableField("scoreForEssay")
    private String scoreForEssay;

    @TableField("hasScore")
    private Boolean hasScore;

    @TableField("isRight")
    private Boolean isRight;

    /**
     * 我的答案json串-需要根据题目类型解析不同的答案
     */
    @TableField("myAnswer")
    private String myAnswer;


    public Integer getExamId() {
        return examId;
    }

    public TExamPractiseItemStu setExamId(Integer examId) {
        this.examId = examId;
        return this;
    }

    public String getUid() {
        return uid;
    }

    public TExamPractiseItemStu setUid(String uid) {
        this.uid = uid;
        return this;
    }

    public Integer getConfigId() {
        return configId;
    }

    public TExamPractiseItemStu setConfigId(Integer configId) {
        this.configId = configId;
        return this;
    }

    public Float getAutoScore() {
        return autoScore;
    }

    public TExamPractiseItemStu setAutoScore(Float autoScore) {
        this.autoScore = autoScore;
        return this;
    }

    public Float getManualScore() {
        return manualScore;
    }

    public TExamPractiseItemStu setManualScore(Float manualScore) {
        this.manualScore = manualScore;
        return this;
    }

    public String getScoreForEssay() {
        return scoreForEssay;
    }

    public TExamPractiseItemStu setScoreForEssay(String scoreForEssay) {
        this.scoreForEssay = scoreForEssay;
        return this;
    }

    public Boolean getHasScore() {
        return hasScore;
    }

    public TExamPractiseItemStu setHasScore(Boolean hasScore) {
        this.hasScore = hasScore;
        return this;
    }

    public Boolean getRight() {
        return isRight;
    }

    public TExamPractiseItemStu setRight(Boolean isRight) {
        this.isRight = isRight;
        return this;
    }

    public String getMyAnswer() {
        return myAnswer;
    }

    public TExamPractiseItemStu setMyAnswer(String myAnswer) {
        this.myAnswer = myAnswer;
        return this;
    }

    @Override
    public String toString() {
        return "TExamPractiseItemStu{" +
        "examId=" + examId +
        ", uid=" + uid +
        ", configId=" + configId +
        ", autoScore=" + autoScore +
        ", manualScore=" + manualScore +
        ", scoreForEssay=" + scoreForEssay +
        ", hasScore=" + hasScore +
        ", isRight=" + isRight +
        ", myAnswer=" + myAnswer +
        "}";
    }
}
