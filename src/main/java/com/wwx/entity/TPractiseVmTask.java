package com.wwx.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
public class TPractiseVmTask implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId("questionTypeId")
    private Integer questionTypeId;

    @TableField("questionId")
    private Integer questionId;

    @TableField("taskId")
    private Integer taskId;

    @TableField("vmType")
    private Integer vmType;

    @TableField("vmUUID")
    private String vmUUID;

    @TableField("tagName")
    private String tagName;


    public Integer getQuestionTypeId() {
        return questionTypeId;
    }

    public TPractiseVmTask setQuestionTypeId(Integer questionTypeId) {
        this.questionTypeId = questionTypeId;
        return this;
    }

    public Integer getQuestionId() {
        return questionId;
    }

    public TPractiseVmTask setQuestionId(Integer questionId) {
        this.questionId = questionId;
        return this;
    }

    public Integer getTaskId() {
        return taskId;
    }

    public TPractiseVmTask setTaskId(Integer taskId) {
        this.taskId = taskId;
        return this;
    }

    public Integer getVmType() {
        return vmType;
    }

    public TPractiseVmTask setVmType(Integer vmType) {
        this.vmType = vmType;
        return this;
    }

    public String getVmUUID() {
        return vmUUID;
    }

    public TPractiseVmTask setVmUUID(String vmUUID) {
        this.vmUUID = vmUUID;
        return this;
    }

    public String getTagName() {
        return tagName;
    }

    public TPractiseVmTask setTagName(String tagName) {
        this.tagName = tagName;
        return this;
    }

    @Override
    public String toString() {
        return "TPractiseVmTask{" +
        "questionTypeId=" + questionTypeId +
        ", questionId=" + questionId +
        ", taskId=" + taskId +
        ", vmType=" + vmType +
        ", vmUUID=" + vmUUID +
        ", tagName=" + tagName +
        "}";
    }
}
