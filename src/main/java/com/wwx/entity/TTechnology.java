package com.wwx.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
public class TTechnology implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "technologyId", type = IdType.AUTO)
    private Integer technologyId;

    private String name;

    /**
     * 1 不可删除， 
     */
    private Integer state;


    public Integer getTechnologyId() {
        return technologyId;
    }

    public TTechnology setTechnologyId(Integer technologyId) {
        this.technologyId = technologyId;
        return this;
    }

    public String getName() {
        return name;
    }

    public TTechnology setName(String name) {
        this.name = name;
        return this;
    }

    public Integer getState() {
        return state;
    }

    public TTechnology setState(Integer state) {
        this.state = state;
        return this;
    }

    @Override
    public String toString() {
        return "TTechnology{" +
        "technologyId=" + technologyId +
        ", name=" + name +
        ", state=" + state +
        "}";
    }
}
