package com.wwx.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 角色表
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-19
 */
public class TRole implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @TableField("userId")
    private String userId;

    @TableField("roleId")
    private Integer roleId;

    private String name;

    private String value;

    private String available;


    public Integer getId() {
        return id;
    }

    public TRole setId(Integer id) {
        this.id = id;
        return this;
    }

    public String getUserId() {
        return userId;
    }

    public TRole setUserId(String userId) {
        this.userId = userId;
        return this;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public TRole setRoleId(Integer roleId) {
        this.roleId = roleId;
        return this;
    }

    public String getName() {
        return name;
    }

    public TRole setName(String name) {
        this.name = name;
        return this;
    }

    public String getValue() {
        return value;
    }

    public TRole setValue(String value) {
        this.value = value;
        return this;
    }

    public String getAvailable() {
        return available;
    }

    public TRole setAvailable(String available) {
        this.available = available;
        return this;
    }

    @Override
    public String toString() {
        return "TRole{" +
        "id=" + id +
        ", userId=" + userId +
        ", roleId=" + roleId +
        ", name=" + name +
        ", value=" + value +
        ", available=" + available +
        "}";
    }
}
