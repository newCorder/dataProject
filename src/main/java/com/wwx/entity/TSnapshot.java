package com.wwx.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
public class TSnapshot implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 快照名字：t_t01_1_vmName(.vbox, .vdi)
     */
    @TableId("idKey")
    private String idKey;

    private String name;

    private String uid;

    private String role;

    @TableField("bookId")
    private Integer bookId;

    @TableField("sectionId")
    private Integer sectionId;

    /**
     * 快照上传时间
     */
    @TableField("saveTime")
    private LocalDateTime saveTime;

    /**
     * 快照文件最后一次修改时间
     */
    @TableField("fileTime")
    private LocalDateTime fileTime;

    /**
     * 文件存储路径（ftp集群）
     */
    @TableField("fileServerUrl")
    private String fileServerUrl;

    /**
     * 标识上传文件的机子(可能用于断点续传)
     */
    @TableField("srcId")
    private String srcId;

    private Long size;

    @TableField("sataUuid")
    private String sataUuid;


    public String getIdKey() {
        return idKey;
    }

    public TSnapshot setIdKey(String idKey) {
        this.idKey = idKey;
        return this;
    }

    public String getName() {
        return name;
    }

    public TSnapshot setName(String name) {
        this.name = name;
        return this;
    }

    public String getUid() {
        return uid;
    }

    public TSnapshot setUid(String uid) {
        this.uid = uid;
        return this;
    }

    public String getRole() {
        return role;
    }

    public TSnapshot setRole(String role) {
        this.role = role;
        return this;
    }

    public Integer getBookId() {
        return bookId;
    }

    public TSnapshot setBookId(Integer bookId) {
        this.bookId = bookId;
        return this;
    }

    public Integer getSectionId() {
        return sectionId;
    }

    public TSnapshot setSectionId(Integer sectionId) {
        this.sectionId = sectionId;
        return this;
    }

    public LocalDateTime getSaveTime() {
        return saveTime;
    }

    public TSnapshot setSaveTime(LocalDateTime saveTime) {
        this.saveTime = saveTime;
        return this;
    }

    public LocalDateTime getFileTime() {
        return fileTime;
    }

    public TSnapshot setFileTime(LocalDateTime fileTime) {
        this.fileTime = fileTime;
        return this;
    }

    public String getFileServerUrl() {
        return fileServerUrl;
    }

    public TSnapshot setFileServerUrl(String fileServerUrl) {
        this.fileServerUrl = fileServerUrl;
        return this;
    }

    public String getSrcId() {
        return srcId;
    }

    public TSnapshot setSrcId(String srcId) {
        this.srcId = srcId;
        return this;
    }

    public Long getSize() {
        return size;
    }

    public TSnapshot setSize(Long size) {
        this.size = size;
        return this;
    }

    public String getSataUuid() {
        return sataUuid;
    }

    public TSnapshot setSataUuid(String sataUuid) {
        this.sataUuid = sataUuid;
        return this;
    }

    @Override
    public String toString() {
        return "TSnapshot{" +
        "idKey=" + idKey +
        ", name=" + name +
        ", uid=" + uid +
        ", role=" + role +
        ", bookId=" + bookId +
        ", sectionId=" + sectionId +
        ", saveTime=" + saveTime +
        ", fileTime=" + fileTime +
        ", fileServerUrl=" + fileServerUrl +
        ", srcId=" + srcId +
        ", size=" + size +
        ", sataUuid=" + sataUuid +
        "}";
    }
}
