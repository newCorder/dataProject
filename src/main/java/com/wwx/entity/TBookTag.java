package com.wwx.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
public class TBookTag implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "tagId", type = IdType.AUTO)
    private Integer tagId;

    @TableField("bookId")
    private Integer bookId;

    @TableField("tagName")
    private String tagName;


    public Integer getTagId() {
        return tagId;
    }

    public TBookTag setTagId(Integer tagId) {
        this.tagId = tagId;
        return this;
    }

    public Integer getBookId() {
        return bookId;
    }

    public TBookTag setBookId(Integer bookId) {
        this.bookId = bookId;
        return this;
    }

    public String getTagName() {
        return tagName;
    }

    public TBookTag setTagName(String tagName) {
        this.tagName = tagName;
        return this;
    }

    @Override
    public String toString() {
        return "TBookTag{" +
        "tagId=" + tagId +
        ", bookId=" + bookId +
        ", tagName=" + tagName +
        "}";
    }
}
