package com.wwx.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
public class TTaskState implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId("planId")
    private Integer planId;

    @TableField("sectionId")
    private Integer sectionId;

    private String uid;

    @TableField("taskId")
    private Integer taskId;

    @TableField("vmType")
    private Integer vmType;

    @TableField("vmUUID")
    private String vmUUID;

    @TableField("tagName")
    private String tagName;

    private Integer state;

    /**
     * 任务开始时间
     */
    @TableField("startTime")
    private LocalDateTime startTime;

    @TableField("endTime")
    private LocalDateTime endTime;

    private String ext;


    public Integer getPlanId() {
        return planId;
    }

    public TTaskState setPlanId(Integer planId) {
        this.planId = planId;
        return this;
    }

    public Integer getSectionId() {
        return sectionId;
    }

    public TTaskState setSectionId(Integer sectionId) {
        this.sectionId = sectionId;
        return this;
    }

    public String getUid() {
        return uid;
    }

    public TTaskState setUid(String uid) {
        this.uid = uid;
        return this;
    }

    public Integer getTaskId() {
        return taskId;
    }

    public TTaskState setTaskId(Integer taskId) {
        this.taskId = taskId;
        return this;
    }

    public Integer getVmType() {
        return vmType;
    }

    public TTaskState setVmType(Integer vmType) {
        this.vmType = vmType;
        return this;
    }

    public String getVmUUID() {
        return vmUUID;
    }

    public TTaskState setVmUUID(String vmUUID) {
        this.vmUUID = vmUUID;
        return this;
    }

    public String getTagName() {
        return tagName;
    }

    public TTaskState setTagName(String tagName) {
        this.tagName = tagName;
        return this;
    }

    public Integer getState() {
        return state;
    }

    public TTaskState setState(Integer state) {
        this.state = state;
        return this;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public TTaskState setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
        return this;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public TTaskState setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
        return this;
    }

    public String getExt() {
        return ext;
    }

    public TTaskState setExt(String ext) {
        this.ext = ext;
        return this;
    }

    @Override
    public String toString() {
        return "TTaskState{" +
        "planId=" + planId +
        ", sectionId=" + sectionId +
        ", uid=" + uid +
        ", taskId=" + taskId +
        ", vmType=" + vmType +
        ", vmUUID=" + vmUUID +
        ", tagName=" + tagName +
        ", state=" + state +
        ", startTime=" + startTime +
        ", endTime=" + endTime +
        ", ext=" + ext +
        "}";
    }
}
