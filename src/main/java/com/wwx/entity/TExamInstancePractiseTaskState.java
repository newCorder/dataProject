package com.wwx.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
public class TExamInstancePractiseTaskState implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId("configId")
    private Integer configId;

    @TableField("taskId")
    private Integer taskId;

    @TableField("examId")
    private Integer examId;

    private String uid;

    @TableField("vmType")
    private Integer vmType;

    @TableField("vmUUID")
    private String vmUUID;

    @TableField("tagName")
    private String tagName;

    /**
     * 0 未开展 1 已完成
     */
    private Integer state;

    private LocalDateTime time;


    public Integer getConfigId() {
        return configId;
    }

    public TExamInstancePractiseTaskState setConfigId(Integer configId) {
        this.configId = configId;
        return this;
    }

    public Integer getTaskId() {
        return taskId;
    }

    public TExamInstancePractiseTaskState setTaskId(Integer taskId) {
        this.taskId = taskId;
        return this;
    }

    public Integer getExamId() {
        return examId;
    }

    public TExamInstancePractiseTaskState setExamId(Integer examId) {
        this.examId = examId;
        return this;
    }

    public String getUid() {
        return uid;
    }

    public TExamInstancePractiseTaskState setUid(String uid) {
        this.uid = uid;
        return this;
    }

    public Integer getVmType() {
        return vmType;
    }

    public TExamInstancePractiseTaskState setVmType(Integer vmType) {
        this.vmType = vmType;
        return this;
    }

    public String getVmUUID() {
        return vmUUID;
    }

    public TExamInstancePractiseTaskState setVmUUID(String vmUUID) {
        this.vmUUID = vmUUID;
        return this;
    }

    public String getTagName() {
        return tagName;
    }

    public TExamInstancePractiseTaskState setTagName(String tagName) {
        this.tagName = tagName;
        return this;
    }

    public Integer getState() {
        return state;
    }

    public TExamInstancePractiseTaskState setState(Integer state) {
        this.state = state;
        return this;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public TExamInstancePractiseTaskState setTime(LocalDateTime time) {
        this.time = time;
        return this;
    }

    @Override
    public String toString() {
        return "TExamInstancePractiseTaskState{" +
        "configId=" + configId +
        ", taskId=" + taskId +
        ", examId=" + examId +
        ", uid=" + uid +
        ", vmType=" + vmType +
        ", vmUUID=" + vmUUID +
        ", tagName=" + tagName +
        ", state=" + state +
        ", time=" + time +
        "}";
    }
}
