package com.wwx.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-21
 */
public class TExamQuestionScore implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 题目ID
     */
    @TableId("questionId")
    private Integer questionId;

    /**
     * 考试ID
     */
    @TableField("examId")
    private Integer examId;

    /**
     * 学生ID
     */
    @TableField("studentId")
    private String studentId;

    /**
     * 考试的开始时间
     */
    @TableField("startTime")
    private LocalDateTime startTime;

    /**
     * 该题目的满分
     */
    private Float score;

    /**
     * 学生答案
     */
    @TableField("studentAnswer")
    private String studentAnswer;

    /**
     * 学生该题的得分
     */
    @TableField("studentScore")
    private Float studentScore;


    public Integer getQuestionId() {
        return questionId;
    }

    public TExamQuestionScore setQuestionId(Integer questionId) {
        this.questionId = questionId;
        return this;
    }

    public Integer getExamId() {
        return examId;
    }

    public TExamQuestionScore setExamId(Integer examId) {
        this.examId = examId;
        return this;
    }

    public String getStudentId() {
        return studentId;
    }

    public TExamQuestionScore setStudentId(String studentId) {
        this.studentId = studentId;
        return this;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public TExamQuestionScore setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
        return this;
    }

    public Float getScore() {
        return score;
    }

    public TExamQuestionScore setScore(Float score) {
        this.score = score;
        return this;
    }

    public String getStudentAnswer() {
        return studentAnswer;
    }

    public TExamQuestionScore setStudentAnswer(String studentAnswer) {
        this.studentAnswer = studentAnswer;
        return this;
    }

    public Float getStudentScore() {
        return studentScore;
    }

    public TExamQuestionScore setStudentScore(Float studentScore) {
        this.studentScore = studentScore;
        return this;
    }

    @Override
    public String toString() {
        return "TExamQuestionScore{" +
        "questionId=" + questionId +
        ", examId=" + examId +
        ", studentId=" + studentId +
        ", startTime=" + startTime +
        ", score=" + score +
        ", studentAnswer=" + studentAnswer +
        ", studentScore=" + studentScore +
        "}";
    }
}
