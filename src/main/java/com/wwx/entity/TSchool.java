package com.wwx.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
public class TSchool implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "schoolId", type = IdType.AUTO)
    private Integer schoolId;

    @TableField("schoolName")
    private String schoolName;

    @TableField("provinceId")
    private Integer provinceId;

    @TableField("cityId")
    private Integer cityId;

    @TableField("areaId")
    private Integer areaId;

    private String address;

    private String logo;

    private String contact;

    private String phone;

    @TableField("createTime")
    private LocalDateTime createTime;

    @TableField("updateTime")
    private LocalDateTime updateTime;


    public Integer getSchoolId() {
        return schoolId;
    }

    public TSchool setSchoolId(Integer schoolId) {
        this.schoolId = schoolId;
        return this;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public TSchool setSchoolName(String schoolName) {
        this.schoolName = schoolName;
        return this;
    }

    public Integer getProvinceId() {
        return provinceId;
    }

    public TSchool setProvinceId(Integer provinceId) {
        this.provinceId = provinceId;
        return this;
    }

    public Integer getCityId() {
        return cityId;
    }

    public TSchool setCityId(Integer cityId) {
        this.cityId = cityId;
        return this;
    }

    public Integer getAreaId() {
        return areaId;
    }

    public TSchool setAreaId(Integer areaId) {
        this.areaId = areaId;
        return this;
    }

    public String getAddress() {
        return address;
    }

    public TSchool setAddress(String address) {
        this.address = address;
        return this;
    }

    public String getLogo() {
        return logo;
    }

    public TSchool setLogo(String logo) {
        this.logo = logo;
        return this;
    }

    public String getContact() {
        return contact;
    }

    public TSchool setContact(String contact) {
        this.contact = contact;
        return this;
    }

    public String getPhone() {
        return phone;
    }

    public TSchool setPhone(String phone) {
        this.phone = phone;
        return this;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public TSchool setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
        return this;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public TSchool setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
        return this;
    }

    @Override
    public String toString() {
        return "TSchool{" +
        "schoolId=" + schoolId +
        ", schoolName=" + schoolName +
        ", provinceId=" + provinceId +
        ", cityId=" + cityId +
        ", areaId=" + areaId +
        ", address=" + address +
        ", logo=" + logo +
        ", contact=" + contact +
        ", phone=" + phone +
        ", createTime=" + createTime +
        ", updateTime=" + updateTime +
        "}";
    }
}
