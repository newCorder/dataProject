package com.wwx.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
public class TBook implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "bookId", type = IdType.AUTO)
    private Integer bookId;

    @TableField("bookName")
    private String bookName;

    @TableField("bookImagePath")
    private String bookImagePath;

    /**
     * 1表示核心课程，2标识实训案例
     */
    @TableField("bookType")
    private Integer bookType;


    public Integer getBookId() {
        return bookId;
    }

    public TBook setBookId(Integer bookId) {
        this.bookId = bookId;
        return this;
    }

    public String getBookName() {
        return bookName;
    }

    public TBook setBookName(String bookName) {
        this.bookName = bookName;
        return this;
    }

    public String getBookImagePath() {
        return bookImagePath;
    }

    public TBook setBookImagePath(String bookImagePath) {
        this.bookImagePath = bookImagePath;
        return this;
    }

    public Integer getBookType() {
        return bookType;
    }

    public TBook setBookType(Integer bookType) {
        this.bookType = bookType;
        return this;
    }

    @Override
    public String toString() {
        return "TBook{" +
        "bookId=" + bookId +
        ", bookName=" + bookName +
        ", bookImagePath=" + bookImagePath +
        ", bookType=" + bookType +
        "}";
    }
}
