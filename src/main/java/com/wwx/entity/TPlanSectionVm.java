package com.wwx.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
public class TPlanSectionVm implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId("planId")
    private Integer planId;

    @TableField("sectionId")
    private Integer sectionId;

    @TableField("vmType")
    private Integer vmType;

    @TableField("vmUUID")
    private String vmUUID;

    @TableField("vmName")
    private String vmName;

    @TableField("tagName")
    private String tagName;

    @TableField("vmBtnName")
    private String vmBtnName;

    @TableField("vmSnapshotName")
    private String vmSnapshotName;

    @TableField("restoreSnapshotName")
    private String restoreSnapshotName;


    public Integer getPlanId() {
        return planId;
    }

    public TPlanSectionVm setPlanId(Integer planId) {
        this.planId = planId;
        return this;
    }

    public Integer getSectionId() {
        return sectionId;
    }

    public TPlanSectionVm setSectionId(Integer sectionId) {
        this.sectionId = sectionId;
        return this;
    }

    public Integer getVmType() {
        return vmType;
    }

    public TPlanSectionVm setVmType(Integer vmType) {
        this.vmType = vmType;
        return this;
    }

    public String getVmUUID() {
        return vmUUID;
    }

    public TPlanSectionVm setVmUUID(String vmUUID) {
        this.vmUUID = vmUUID;
        return this;
    }

    public String getVmName() {
        return vmName;
    }

    public TPlanSectionVm setVmName(String vmName) {
        this.vmName = vmName;
        return this;
    }

    public String getTagName() {
        return tagName;
    }

    public TPlanSectionVm setTagName(String tagName) {
        this.tagName = tagName;
        return this;
    }

    public String getVmBtnName() {
        return vmBtnName;
    }

    public TPlanSectionVm setVmBtnName(String vmBtnName) {
        this.vmBtnName = vmBtnName;
        return this;
    }

    public String getVmSnapshotName() {
        return vmSnapshotName;
    }

    public TPlanSectionVm setVmSnapshotName(String vmSnapshotName) {
        this.vmSnapshotName = vmSnapshotName;
        return this;
    }

    public String getRestoreSnapshotName() {
        return restoreSnapshotName;
    }

    public TPlanSectionVm setRestoreSnapshotName(String restoreSnapshotName) {
        this.restoreSnapshotName = restoreSnapshotName;
        return this;
    }

    @Override
    public String toString() {
        return "TPlanSectionVm{" +
        "planId=" + planId +
        ", sectionId=" + sectionId +
        ", vmType=" + vmType +
        ", vmUUID=" + vmUUID +
        ", vmName=" + vmName +
        ", tagName=" + tagName +
        ", vmBtnName=" + vmBtnName +
        ", vmSnapshotName=" + vmSnapshotName +
        ", restoreSnapshotName=" + restoreSnapshotName +
        "}";
    }
}
