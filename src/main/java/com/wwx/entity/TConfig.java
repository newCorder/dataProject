package com.wwx.entity;

import java.io.Serializable;

/**
 * <p>
 * system config
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
public class TConfig implements Serializable {

    private static final long serialVersionUID = 1L;

    private String key;

    private String value;


    public String getKey() {
        return key;
    }

    public TConfig setKey(String key) {
        this.key = key;
        return this;
    }

    public String getValue() {
        return value;
    }

    public TConfig setValue(String value) {
        this.value = value;
        return this;
    }

    @Override
    public String toString() {
        return "TConfig{" +
        "key=" + key +
        ", value=" + value +
        "}";
    }
}
