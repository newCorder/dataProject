package com.wwx.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
public class TPlanPractise implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "configId", type = IdType.AUTO)
    private Integer configId;

    @TableField("planId")
    private Integer planId;

    /**
     * 对应的章节
     */
    @TableField("sectionId")
    private Integer sectionId;

    @TableField("questionTypeId")
    private Integer questionTypeId;

    @TableField("questionId")
    private Integer questionId;

    @TableField("questionContent")
    private String questionContent;

    @TableField("answerOptions")
    private String answerOptions;

    @TableField("questionNumber")
    private Integer questionNumber;

    private String answer;

    private String analyse;

    private Float score;


    public Integer getConfigId() {
        return configId;
    }

    public TPlanPractise setConfigId(Integer configId) {
        this.configId = configId;
        return this;
    }

    public Integer getPlanId() {
        return planId;
    }

    public TPlanPractise setPlanId(Integer planId) {
        this.planId = planId;
        return this;
    }

    public Integer getSectionId() {
        return sectionId;
    }

    public TPlanPractise setSectionId(Integer sectionId) {
        this.sectionId = sectionId;
        return this;
    }

    public Integer getQuestionTypeId() {
        return questionTypeId;
    }

    public TPlanPractise setQuestionTypeId(Integer questionTypeId) {
        this.questionTypeId = questionTypeId;
        return this;
    }

    public Integer getQuestionId() {
        return questionId;
    }

    public TPlanPractise setQuestionId(Integer questionId) {
        this.questionId = questionId;
        return this;
    }

    public String getQuestionContent() {
        return questionContent;
    }

    public TPlanPractise setQuestionContent(String questionContent) {
        this.questionContent = questionContent;
        return this;
    }

    public String getAnswerOptions() {
        return answerOptions;
    }

    public TPlanPractise setAnswerOptions(String answerOptions) {
        this.answerOptions = answerOptions;
        return this;
    }

    public Integer getQuestionNumber() {
        return questionNumber;
    }

    public TPlanPractise setQuestionNumber(Integer questionNumber) {
        this.questionNumber = questionNumber;
        return this;
    }

    public String getAnswer() {
        return answer;
    }

    public TPlanPractise setAnswer(String answer) {
        this.answer = answer;
        return this;
    }

    public String getAnalyse() {
        return analyse;
    }

    public TPlanPractise setAnalyse(String analyse) {
        this.analyse = analyse;
        return this;
    }

    public Float getScore() {
        return score;
    }

    public TPlanPractise setScore(Float score) {
        this.score = score;
        return this;
    }

    @Override
    public String toString() {
        return "TPlanPractise{" +
        "configId=" + configId +
        ", planId=" + planId +
        ", sectionId=" + sectionId +
        ", questionTypeId=" + questionTypeId +
        ", questionId=" + questionId +
        ", questionContent=" + questionContent +
        ", answerOptions=" + answerOptions +
        ", questionNumber=" + questionNumber +
        ", answer=" + answer +
        ", analyse=" + analyse +
        ", score=" + score +
        "}";
    }
}
