package com.wwx.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
public class TPlan implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "planId", type = IdType.AUTO)
    private Integer planId;

    private String uid;

    @TableField("collegeId")
    private Integer collegeId;

    @TableField("majorId")
    private Integer majorId;

    @TableField("classId")
    private Integer classId;

    @TableField("gradeId")
    private Integer gradeId;

    @TableField("termId")
    private Integer termId;

    @TableField("bookId")
    private Integer bookId;

    @TableField("bookName")
    private String bookName;

    @TableField("bookImagePath")
    private String bookImagePath;

    @TableField("bookType")
    private Integer bookType;

    @TableField("planName")
    private String planName;

    private String remark;

    @TableField("updateTime")
    private LocalDateTime updateTime;

    /**
     * 扩展为状态： 0 不可用， 1已发布， 2已删除
     */
    private Integer state;


    public Integer getPlanId() {
        return planId;
    }

    public TPlan setPlanId(Integer planId) {
        this.planId = planId;
        return this;
    }

    public String getUid() {
        return uid;
    }

    public TPlan setUid(String uid) {
        this.uid = uid;
        return this;
    }

    public Integer getCollegeId() {
        return collegeId;
    }

    public TPlan setCollegeId(Integer collegeId) {
        this.collegeId = collegeId;
        return this;
    }

    public Integer getMajorId() {
        return majorId;
    }

    public TPlan setMajorId(Integer majorId) {
        this.majorId = majorId;
        return this;
    }

    public Integer getClassId() {
        return classId;
    }

    public TPlan setClassId(Integer classId) {
        this.classId = classId;
        return this;
    }

    public Integer getGradeId() {
        return gradeId;
    }

    public TPlan setGradeId(Integer gradeId) {
        this.gradeId = gradeId;
        return this;
    }

    public Integer getTermId() {
        return termId;
    }

    public TPlan setTermId(Integer termId) {
        this.termId = termId;
        return this;
    }

    public Integer getBookId() {
        return bookId;
    }

    public TPlan setBookId(Integer bookId) {
        this.bookId = bookId;
        return this;
    }

    public String getBookName() {
        return bookName;
    }

    public TPlan setBookName(String bookName) {
        this.bookName = bookName;
        return this;
    }

    public String getBookImagePath() {
        return bookImagePath;
    }

    public TPlan setBookImagePath(String bookImagePath) {
        this.bookImagePath = bookImagePath;
        return this;
    }

    public Integer getBookType() {
        return bookType;
    }

    public TPlan setBookType(Integer bookType) {
        this.bookType = bookType;
        return this;
    }

    public String getPlanName() {
        return planName;
    }

    public TPlan setPlanName(String planName) {
        this.planName = planName;
        return this;
    }

    public String getRemark() {
        return remark;
    }

    public TPlan setRemark(String remark) {
        this.remark = remark;
        return this;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public TPlan setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
        return this;
    }

    public Integer getState() {
        return state;
    }

    public TPlan setState(Integer state) {
        this.state = state;
        return this;
    }

    @Override
    public String toString() {
        return "TPlan{" +
        "planId=" + planId +
        ", uid=" + uid +
        ", collegeId=" + collegeId +
        ", majorId=" + majorId +
        ", classId=" + classId +
        ", gradeId=" + gradeId +
        ", termId=" + termId +
        ", bookId=" + bookId +
        ", bookName=" + bookName +
        ", bookImagePath=" + bookImagePath +
        ", bookType=" + bookType +
        ", planName=" + planName +
        ", remark=" + remark +
        ", updateTime=" + updateTime +
        ", state=" + state +
        "}";
    }
}
