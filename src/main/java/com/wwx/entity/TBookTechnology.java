package com.wwx.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
public class TBookTechnology implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId("bookId")
    private Integer bookId;

    @TableField("technologyId")
    private Integer technologyId;


    public Integer getBookId() {
        return bookId;
    }

    public TBookTechnology setBookId(Integer bookId) {
        this.bookId = bookId;
        return this;
    }

    public Integer getTechnologyId() {
        return technologyId;
    }

    public TBookTechnology setTechnologyId(Integer technologyId) {
        this.technologyId = technologyId;
        return this;
    }

    @Override
    public String toString() {
        return "TBookTechnology{" +
        "bookId=" + bookId +
        ", technologyId=" + technologyId +
        "}";
    }
}
