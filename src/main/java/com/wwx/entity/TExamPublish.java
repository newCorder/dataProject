package com.wwx.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
public class TExamPublish implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "examId", type = IdType.AUTO)
    private Integer examId;

    @TableField("examName")
    private String examName;

    private String uid;

    @TableField("collegeId")
    private Integer collegeId;

    @TableField("majorId")
    private Integer majorId;

    @TableField("classId")
    private Integer classId;

    @TableField("gradeId")
    private Integer gradeId;

    @TableField("startTime")
    private LocalDateTime startTime;

    /**
     * 扩展为状态： 0 不可用， 1已发布， 2已删除
     */
    @TableField("endTime")
    private LocalDateTime endTime;

    /**
     * 试卷配置唯一ID
     */
    @TableField("paperId")
    private Integer paperId;

    @TableField("bookId")
    private Integer bookId;

    @TableField("bookName")
    private String bookName;

    @TableField("examLevel")
    private Integer examLevel;

    /**
     * 试卷属性： 1 表示独占，2 共享（默认值）
     */
    @TableField("examState")
    private Integer examState;

    @TableField("examDsc")
    private String examDsc;

    @TableField("imageUrl")
    private String imageUrl;


    public Integer getExamId() {
        return examId;
    }

    public TExamPublish setExamId(Integer examId) {
        this.examId = examId;
        return this;
    }

    public String getExamName() {
        return examName;
    }

    public TExamPublish setExamName(String examName) {
        this.examName = examName;
        return this;
    }

    public String getUid() {
        return uid;
    }

    public TExamPublish setUid(String uid) {
        this.uid = uid;
        return this;
    }

    public Integer getCollegeId() {
        return collegeId;
    }

    public TExamPublish setCollegeId(Integer collegeId) {
        this.collegeId = collegeId;
        return this;
    }

    public Integer getMajorId() {
        return majorId;
    }

    public TExamPublish setMajorId(Integer majorId) {
        this.majorId = majorId;
        return this;
    }

    public Integer getClassId() {
        return classId;
    }

    public TExamPublish setClassId(Integer classId) {
        this.classId = classId;
        return this;
    }

    public Integer getGradeId() {
        return gradeId;
    }

    public TExamPublish setGradeId(Integer gradeId) {
        this.gradeId = gradeId;
        return this;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public TExamPublish setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
        return this;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public TExamPublish setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
        return this;
    }

    public Integer getPaperId() {
        return paperId;
    }

    public TExamPublish setPaperId(Integer paperId) {
        this.paperId = paperId;
        return this;
    }

    public Integer getBookId() {
        return bookId;
    }

    public TExamPublish setBookId(Integer bookId) {
        this.bookId = bookId;
        return this;
    }

    public String getBookName() {
        return bookName;
    }

    public TExamPublish setBookName(String bookName) {
        this.bookName = bookName;
        return this;
    }

    public Integer getExamLevel() {
        return examLevel;
    }

    public TExamPublish setExamLevel(Integer examLevel) {
        this.examLevel = examLevel;
        return this;
    }

    public Integer getExamState() {
        return examState;
    }

    public TExamPublish setExamState(Integer examState) {
        this.examState = examState;
        return this;
    }

    public String getExamDsc() {
        return examDsc;
    }

    public TExamPublish setExamDsc(String examDsc) {
        this.examDsc = examDsc;
        return this;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public TExamPublish setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
        return this;
    }

    @Override
    public String toString() {
        return "TExamPublish{" +
        "examId=" + examId +
        ", examName=" + examName +
        ", uid=" + uid +
        ", collegeId=" + collegeId +
        ", majorId=" + majorId +
        ", classId=" + classId +
        ", gradeId=" + gradeId +
        ", startTime=" + startTime +
        ", endTime=" + endTime +
        ", paperId=" + paperId +
        ", bookId=" + bookId +
        ", bookName=" + bookName +
        ", examLevel=" + examLevel +
        ", examState=" + examState +
        ", examDsc=" + examDsc +
        ", imageUrl=" + imageUrl +
        "}";
    }
}
