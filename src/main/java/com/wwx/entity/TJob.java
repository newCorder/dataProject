package com.wwx.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
public class TJob implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "jobId", type = IdType.AUTO)
    private Integer jobId;

    private String name;


    public Integer getJobId() {
        return jobId;
    }

    public TJob setJobId(Integer jobId) {
        this.jobId = jobId;
        return this;
    }

    public String getName() {
        return name;
    }

    public TJob setName(String name) {
        this.name = name;
        return this;
    }

    @Override
    public String toString() {
        return "TJob{" +
        "jobId=" + jobId +
        ", name=" + name +
        "}";
    }
}
