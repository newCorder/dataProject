package com.wwx.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
public class TClass implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "classId", type = IdType.AUTO)
    private Integer classId;

    @TableField("majorId")
    private Integer majorId;

    @TableField("gradeId")
    private Integer gradeId;

    private String name;


    public Integer getClassId() {
        return classId;
    }

    public TClass setClassId(Integer classId) {
        this.classId = classId;
        return this;
    }

    public Integer getMajorId() {
        return majorId;
    }

    public TClass setMajorId(Integer majorId) {
        this.majorId = majorId;
        return this;
    }

    public Integer getGradeId() {
        return gradeId;
    }

    public TClass setGradeId(Integer gradeId) {
        this.gradeId = gradeId;
        return this;
    }

    public String getName() {
        return name;
    }

    public TClass setName(String name) {
        this.name = name;
        return this;
    }

    @Override
    public String toString() {
        return "TClass{" +
        "classId=" + classId +
        ", majorId=" + majorId +
        ", gradeId=" + gradeId +
        ", name=" + name +
        "}";
    }
}
