package com.wwx.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
public class TPractiseItemStu implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId("planId")
    private Integer planId;

    private String uid;

    @TableField("sectionId")
    private Integer sectionId;

    @TableField("configId")
    private Integer configId;

    @TableField("autoScore")
    private Float autoScore;

    @TableField("manualScore")
    private Float manualScore;

    @TableField("scoreForEssay")
    private String scoreForEssay;

    @TableField("hasScore")
    private Boolean hasScore;

    @TableField("isRight")
    private Boolean isRight;

    /**
     * 我的答案json串-需要根据题目类型解析不同的答案
     */
    @TableField("myAnswer")
    private String myAnswer;


    public Integer getPlanId() {
        return planId;
    }

    public TPractiseItemStu setPlanId(Integer planId) {
        this.planId = planId;
        return this;
    }

    public String getUid() {
        return uid;
    }

    public TPractiseItemStu setUid(String uid) {
        this.uid = uid;
        return this;
    }

    public Integer getSectionId() {
        return sectionId;
    }

    public TPractiseItemStu setSectionId(Integer sectionId) {
        this.sectionId = sectionId;
        return this;
    }

    public Integer getConfigId() {
        return configId;
    }

    public TPractiseItemStu setConfigId(Integer configId) {
        this.configId = configId;
        return this;
    }

    public Float getAutoScore() {
        return autoScore;
    }

    public TPractiseItemStu setAutoScore(Float autoScore) {
        this.autoScore = autoScore;
        return this;
    }

    public Float getManualScore() {
        return manualScore;
    }

    public TPractiseItemStu setManualScore(Float manualScore) {
        this.manualScore = manualScore;
        return this;
    }

    public String getScoreForEssay() {
        return scoreForEssay;
    }

    public TPractiseItemStu setScoreForEssay(String scoreForEssay) {
        this.scoreForEssay = scoreForEssay;
        return this;
    }

    public Boolean getHasScore() {
        return hasScore;
    }

    public TPractiseItemStu setHasScore(Boolean hasScore) {
        this.hasScore = hasScore;
        return this;
    }

    public Boolean getRight() {
        return isRight;
    }

    public TPractiseItemStu setRight(Boolean isRight) {
        this.isRight = isRight;
        return this;
    }

    public String getMyAnswer() {
        return myAnswer;
    }

    public TPractiseItemStu setMyAnswer(String myAnswer) {
        this.myAnswer = myAnswer;
        return this;
    }

    @Override
    public String toString() {
        return "TPractiseItemStu{" +
        "planId=" + planId +
        ", uid=" + uid +
        ", sectionId=" + sectionId +
        ", configId=" + configId +
        ", autoScore=" + autoScore +
        ", manualScore=" + manualScore +
        ", scoreForEssay=" + scoreForEssay +
        ", hasScore=" + hasScore +
        ", isRight=" + isRight +
        ", myAnswer=" + myAnswer +
        "}";
    }
}
