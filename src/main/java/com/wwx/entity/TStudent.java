package com.wwx.entity;

import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;

/**
 * 学生表
 * @author wuweixu
 * @since 2019-08-12
 */
public class TStudent implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 用户ID（主键)
     */
    @TableId(value = "studentId")
    private String studentId;
    /**
     * 班级ID
     */
    @TableField("classId")
    private Integer classId;
    /**
     * 年级ID
     */
    @TableField("gradeId")
    private Integer gradeId;
    /**
     * 专业ID
     */
    @TableField("majorId")
    private Integer majorId;
    /**
     * 学院ID
     */
    @TableField("collegeId")
    private Integer collegeId;
    /**
     * 账户名
     */
    private String name;
    /**
     * 密码
     */
    private String pwd;
    /**
     * 上次信息更新时间
     */
    @TableField("updateTime")
    private LocalDateTime updateTime;
    /**
     * 上次登录时间
     */
    private LocalDateTime logintime;
    /**
     * 0 男 1 女
     */
    private Integer sex;
    /**
     * 电话
     */
    private String tel;
    /**
     * 邮箱
     */
    private String mail;

    /**
     * 0 离线， 1 在线
     */
    @TableField("onlineState")
    private Integer onlineState;

    /**
     * 0 禁用 1 启用
     */
    private Integer state;

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public Integer getGradeId() {
        return gradeId;
    }

    public void setGradeId(Integer gradeId) {
        this.gradeId = gradeId;
    }

    public Integer getMajorId() {
        return majorId;
    }

    public void setMajorId(Integer majorId) {
        this.majorId = majorId;
    }

    public Integer getCollegeId() {
        return collegeId;
    }

    public void setCollegeId(Integer collegeId) {
        this.collegeId = collegeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    public LocalDateTime getLogintime() {
        return logintime;
    }

    public void setLogintime(LocalDateTime logintime) {
        this.logintime = logintime;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public Integer getOnlineState() {
        return onlineState;
    }

    public void setOnlineState(Integer onlineState) {
        this.onlineState = onlineState;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    @Override
    public String toString() {
        return "TStudent{" +
                "studentId='" + studentId + '\'' +
                ", classId=" + classId +
                ", gradeId=" + gradeId +
                ", majorId=" + majorId +
                ", collegeId=" + collegeId +
                ", name='" + name + '\'' +
                ", pwd='" + pwd + '\'' +
                ", updateTime=" + updateTime +
                ", logintime=" + logintime +
                ", sex=" + sex +
                ", tel='" + tel + '\'' +
                ", mail='" + mail + '\'' +
                ", onlineState=" + onlineState +
                ", state=" + state +
                '}';
    }
}
