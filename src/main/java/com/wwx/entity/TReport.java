package com.wwx.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
public class TReport implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId("planId")
    private Integer planId;

    private String uid;

    @TableField("sectionId")
    private Integer sectionId;

    private String title;

    private LocalDateTime examtime;

    @TableField("topologyUrl")
    private String topologyUrl;

    @TableField("toolMsgs")
    private String toolMsgs;

    private String principle;

    private String teacher;

    private String experience;

    @TableField("reportScore")
    private Float reportScore;


    public Integer getPlanId() {
        return planId;
    }

    public TReport setPlanId(Integer planId) {
        this.planId = planId;
        return this;
    }

    public String getUid() {
        return uid;
    }

    public TReport setUid(String uid) {
        this.uid = uid;
        return this;
    }

    public Integer getSectionId() {
        return sectionId;
    }

    public TReport setSectionId(Integer sectionId) {
        this.sectionId = sectionId;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public TReport setTitle(String title) {
        this.title = title;
        return this;
    }

    public LocalDateTime getExamtime() {
        return examtime;
    }

    public TReport setExamtime(LocalDateTime examtime) {
        this.examtime = examtime;
        return this;
    }

    public String getTopologyUrl() {
        return topologyUrl;
    }

    public TReport setTopologyUrl(String topologyUrl) {
        this.topologyUrl = topologyUrl;
        return this;
    }

    public String getToolMsgs() {
        return toolMsgs;
    }

    public TReport setToolMsgs(String toolMsgs) {
        this.toolMsgs = toolMsgs;
        return this;
    }

    public String getPrinciple() {
        return principle;
    }

    public TReport setPrinciple(String principle) {
        this.principle = principle;
        return this;
    }

    public String getTeacher() {
        return teacher;
    }

    public TReport setTeacher(String teacher) {
        this.teacher = teacher;
        return this;
    }

    public String getExperience() {
        return experience;
    }

    public TReport setExperience(String experience) {
        this.experience = experience;
        return this;
    }

    public Float getReportScore() {
        return reportScore;
    }

    public TReport setReportScore(Float reportScore) {
        this.reportScore = reportScore;
        return this;
    }

    @Override
    public String toString() {
        return "TReport{" +
        "planId=" + planId +
        ", uid=" + uid +
        ", sectionId=" + sectionId +
        ", title=" + title +
        ", examtime=" + examtime +
        ", topologyUrl=" + topologyUrl +
        ", toolMsgs=" + toolMsgs +
        ", principle=" + principle +
        ", teacher=" + teacher +
        ", experience=" + experience +
        ", reportScore=" + reportScore +
        "}";
    }
}
