package com.wwx.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

/**
 * <p>
 * 上传的文件
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
public class TUpload implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private String name;

    /**
     * 相对路径
     */
    private String path;


    public Integer getId() {
        return id;
    }

    public TUpload setId(Integer id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public TUpload setName(String name) {
        this.name = name;
        return this;
    }

    public String getPath() {
        return path;
    }

    public TUpload setPath(String path) {
        this.path = path;
        return this;
    }

    @Override
    public String toString() {
        return "TUpload{" +
        "id=" + id +
        ", name=" + name +
        ", path=" + path +
        "}";
    }
}
