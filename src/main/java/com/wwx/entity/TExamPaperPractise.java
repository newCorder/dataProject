package com.wwx.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
public class TExamPaperPractise implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "configId", type = IdType.AUTO)
    private Integer configId;

    @TableField("paperId")
    private Integer paperId;

    @TableField("questionId")
    private Integer questionId;

    /**
     * 1单选，2多选， 3填空， 4问答， 5程序， 6实操
     */
    @TableField("questionTypeId")
    private Integer questionTypeId;

    private Float score;


    public Integer getConfigId() {
        return configId;
    }

    public TExamPaperPractise setConfigId(Integer configId) {
        this.configId = configId;
        return this;
    }

    public Integer getPaperId() {
        return paperId;
    }

    public TExamPaperPractise setPaperId(Integer paperId) {
        this.paperId = paperId;
        return this;
    }

    public Integer getQuestionId() {
        return questionId;
    }

    public TExamPaperPractise setQuestionId(Integer questionId) {
        this.questionId = questionId;
        return this;
    }

    public Integer getQuestionTypeId() {
        return questionTypeId;
    }

    public TExamPaperPractise setQuestionTypeId(Integer questionTypeId) {
        this.questionTypeId = questionTypeId;
        return this;
    }

    public Float getScore() {
        return score;
    }

    public TExamPaperPractise setScore(Float score) {
        this.score = score;
        return this;
    }

    @Override
    public String toString() {
        return "TExamPaperPractise{" +
        "configId=" + configId +
        ", paperId=" + paperId +
        ", questionId=" + questionId +
        ", questionTypeId=" + questionTypeId +
        ", score=" + score +
        "}";
    }
}
