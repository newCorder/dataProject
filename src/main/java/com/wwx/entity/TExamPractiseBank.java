package com.wwx.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
public class TExamPractiseBank implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId("bookId")
    private Integer bookId;

    @TableField("questionTypeId")
    private Integer questionTypeId;

    @TableField("questionId")
    private Integer questionId;

    /**
     * 1表示 不共享， 2表示共享
     */
    private Integer state;

    /**
     * 创建试题的用户id
     */
    @TableField("userId")
    private String userId;


    public Integer getBookId() {
        return bookId;
    }

    public TExamPractiseBank setBookId(Integer bookId) {
        this.bookId = bookId;
        return this;
    }

    public Integer getQuestionTypeId() {
        return questionTypeId;
    }

    public TExamPractiseBank setQuestionTypeId(Integer questionTypeId) {
        this.questionTypeId = questionTypeId;
        return this;
    }

    public Integer getQuestionId() {
        return questionId;
    }

    public TExamPractiseBank setQuestionId(Integer questionId) {
        this.questionId = questionId;
        return this;
    }

    public Integer getState() {
        return state;
    }

    public TExamPractiseBank setState(Integer state) {
        this.state = state;
        return this;
    }

    public String getUserId() {
        return userId;
    }

    public TExamPractiseBank setUserId(String userId) {
        this.userId = userId;
        return this;
    }

    @Override
    public String toString() {
        return "TExamPractiseBank{" +
        "bookId=" + bookId +
        ", questionTypeId=" + questionTypeId +
        ", questionId=" + questionId +
        ", state=" + state +
        ", userId=" + userId +
        "}";
    }
}
