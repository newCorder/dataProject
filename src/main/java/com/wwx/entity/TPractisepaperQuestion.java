package com.wwx.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-21
 */
public class TPractisepaperQuestion implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 测评试卷ID
     */
    @TableId("practisePaperId")
    private Integer practisePaperId;

    /**
     * 题目ID
     */
    @TableField("questionId")
    private Integer questionId;


    public Integer getPractisePaperId() {
        return practisePaperId;
    }

    public TPractisepaperQuestion setPractisePaperId(Integer practisePaperId) {
        this.practisePaperId = practisePaperId;
        return this;
    }

    public Integer getQuestionId() {
        return questionId;
    }

    public TPractisepaperQuestion setQuestionId(Integer questionId) {
        this.questionId = questionId;
        return this;
    }

    @Override
    public String toString() {
        return "TPractisepaperQuestion{" +
        "practisePaperId=" + practisePaperId +
        ", questionId=" + questionId +
        "}";
    }
}
