package com.wwx.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
public class TExamInstancePractise implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "configId", type = IdType.AUTO)
    private Integer configId;

    @TableField("examId")
    private Integer examId;

    @TableField("questionTypeId")
    private Integer questionTypeId;

    @TableField("questionId")
    private Integer questionId;

    @TableField("questionContent")
    private String questionContent;

    @TableField("answerOptions")
    private String answerOptions;

    @TableField("questionNumber")
    private Integer questionNumber;

    private String answer;

    private String analyse;

    private Float score;


    public Integer getConfigId() {
        return configId;
    }

    public TExamInstancePractise setConfigId(Integer configId) {
        this.configId = configId;
        return this;
    }

    public Integer getExamId() {
        return examId;
    }

    public TExamInstancePractise setExamId(Integer examId) {
        this.examId = examId;
        return this;
    }

    public Integer getQuestionTypeId() {
        return questionTypeId;
    }

    public TExamInstancePractise setQuestionTypeId(Integer questionTypeId) {
        this.questionTypeId = questionTypeId;
        return this;
    }

    public Integer getQuestionId() {
        return questionId;
    }

    public TExamInstancePractise setQuestionId(Integer questionId) {
        this.questionId = questionId;
        return this;
    }

    public String getQuestionContent() {
        return questionContent;
    }

    public TExamInstancePractise setQuestionContent(String questionContent) {
        this.questionContent = questionContent;
        return this;
    }

    public String getAnswerOptions() {
        return answerOptions;
    }

    public TExamInstancePractise setAnswerOptions(String answerOptions) {
        this.answerOptions = answerOptions;
        return this;
    }

    public Integer getQuestionNumber() {
        return questionNumber;
    }

    public TExamInstancePractise setQuestionNumber(Integer questionNumber) {
        this.questionNumber = questionNumber;
        return this;
    }

    public String getAnswer() {
        return answer;
    }

    public TExamInstancePractise setAnswer(String answer) {
        this.answer = answer;
        return this;
    }

    public String getAnalyse() {
        return analyse;
    }

    public TExamInstancePractise setAnalyse(String analyse) {
        this.analyse = analyse;
        return this;
    }

    public Float getScore() {
        return score;
    }

    public TExamInstancePractise setScore(Float score) {
        this.score = score;
        return this;
    }

    @Override
    public String toString() {
        return "TExamInstancePractise{" +
        "configId=" + configId +
        ", examId=" + examId +
        ", questionTypeId=" + questionTypeId +
        ", questionId=" + questionId +
        ", questionContent=" + questionContent +
        ", answerOptions=" + answerOptions +
        ", questionNumber=" + questionNumber +
        ", answer=" + answer +
        ", analyse=" + analyse +
        ", score=" + score +
        "}";
    }
}
