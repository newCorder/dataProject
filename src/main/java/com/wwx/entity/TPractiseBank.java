package com.wwx.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
public class TPractiseBank implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId("sectionId")
    private Integer sectionId;

    @TableField("questionTypeId")
    private Integer questionTypeId;

    @TableField("questionId")
    private Integer questionId;


    public Integer getSectionId() {
        return sectionId;
    }

    public TPractiseBank setSectionId(Integer sectionId) {
        this.sectionId = sectionId;
        return this;
    }

    public Integer getQuestionTypeId() {
        return questionTypeId;
    }

    public TPractiseBank setQuestionTypeId(Integer questionTypeId) {
        this.questionTypeId = questionTypeId;
        return this;
    }

    public Integer getQuestionId() {
        return questionId;
    }

    public TPractiseBank setQuestionId(Integer questionId) {
        this.questionId = questionId;
        return this;
    }

    @Override
    public String toString() {
        return "TPractiseBank{" +
        "sectionId=" + sectionId +
        ", questionTypeId=" + questionTypeId +
        ", questionId=" + questionId +
        "}";
    }
}
