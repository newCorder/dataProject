package com.wwx.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-21
 */
public class TPractiseQuestionScore implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 测评信息ID
     */
    @TableId("practiseId")
    private Integer practiseId;

    /**
     * 题目ID
     */
    @TableField("questionId")
    private Integer questionId;

    /**
     * 学生ID
     */
    @TableField("studentId")
    private String studentId;

    /**
     * 学生该题的得分
     */
    @TableField("studentScore")
    private Float studentScore;

    /**
     * 该题目的满分
     */
    private Float score;

    /**
     * 测评的开始时间
     */
    @TableField("startTime")
    private LocalDateTime startTime;

    /**
     * 学生答案
     */
    @TableField("studentAnswer")
    private String studentAnswer;


    public Integer getPractiseId() {
        return practiseId;
    }

    public TPractiseQuestionScore setPractiseId(Integer practiseId) {
        this.practiseId = practiseId;
        return this;
    }

    public Integer getQuestionId() {
        return questionId;
    }

    public TPractiseQuestionScore setQuestionId(Integer questionId) {
        this.questionId = questionId;
        return this;
    }

    public String getStudentId() {
        return studentId;
    }

    public TPractiseQuestionScore setStudentId(String studentId) {
        this.studentId = studentId;
        return this;
    }

    public Float getStudentScore() {
        return studentScore;
    }

    public TPractiseQuestionScore setStudentScore(Float studentScore) {
        this.studentScore = studentScore;
        return this;
    }

    public Float getScore() {
        return score;
    }

    public TPractiseQuestionScore setScore(Float score) {
        this.score = score;
        return this;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public TPractiseQuestionScore setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
        return this;
    }

    public String getStudentAnswer() {
        return studentAnswer;
    }

    public TPractiseQuestionScore setStudentAnswer(String studentAnswer) {
        this.studentAnswer = studentAnswer;
        return this;
    }

    @Override
    public String toString() {
        return "TPractiseQuestionScore{" +
        "practiseId=" + practiseId +
        ", questionId=" + questionId +
        ", studentId=" + studentId +
        ", studentScore=" + studentScore +
        ", score=" + score +
        ", startTime=" + startTime +
        ", studentAnswer=" + studentAnswer +
        "}";
    }
}
