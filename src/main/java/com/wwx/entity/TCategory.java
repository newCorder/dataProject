package com.wwx.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
public class TCategory implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "categoryId", type = IdType.AUTO)
    private Integer categoryId;

    @TableField("technologyId")
    private Integer technologyId;

    private String name;

    /**
     * 指示统一技术方向下，课程列表排序
            
     */
    private Integer level;


    public Integer getCategoryId() {
        return categoryId;
    }

    public TCategory setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
        return this;
    }

    public Integer getTechnologyId() {
        return technologyId;
    }

    public TCategory setTechnologyId(Integer technologyId) {
        this.technologyId = technologyId;
        return this;
    }

    public String getName() {
        return name;
    }

    public TCategory setName(String name) {
        this.name = name;
        return this;
    }

    public Integer getLevel() {
        return level;
    }

    public TCategory setLevel(Integer level) {
        this.level = level;
        return this;
    }

    @Override
    public String toString() {
        return "TCategory{" +
        "categoryId=" + categoryId +
        ", technologyId=" + technologyId +
        ", name=" + name +
        ", level=" + level +
        "}";
    }
}
