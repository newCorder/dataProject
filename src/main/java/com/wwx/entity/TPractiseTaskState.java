package com.wwx.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
public class TPractiseTaskState implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId("configId")
    private Integer configId;

    @TableField("taskId")
    private Integer taskId;

    @TableField("planId")
    private Integer planId;

    @TableField("sectionId")
    private Integer sectionId;

    private String uid;

    @TableField("vmType")
    private Integer vmType;

    @TableField("vmUUID")
    private String vmUUID;

    @TableField("tagName")
    private String tagName;

    /**
     * 0 未开展 1 已完成
     */
    private Integer state;

    private LocalDateTime time;


    public Integer getConfigId() {
        return configId;
    }

    public TPractiseTaskState setConfigId(Integer configId) {
        this.configId = configId;
        return this;
    }

    public Integer getTaskId() {
        return taskId;
    }

    public TPractiseTaskState setTaskId(Integer taskId) {
        this.taskId = taskId;
        return this;
    }

    public Integer getPlanId() {
        return planId;
    }

    public TPractiseTaskState setPlanId(Integer planId) {
        this.planId = planId;
        return this;
    }

    public Integer getSectionId() {
        return sectionId;
    }

    public TPractiseTaskState setSectionId(Integer sectionId) {
        this.sectionId = sectionId;
        return this;
    }

    public String getUid() {
        return uid;
    }

    public TPractiseTaskState setUid(String uid) {
        this.uid = uid;
        return this;
    }

    public Integer getVmType() {
        return vmType;
    }

    public TPractiseTaskState setVmType(Integer vmType) {
        this.vmType = vmType;
        return this;
    }

    public String getVmUUID() {
        return vmUUID;
    }

    public TPractiseTaskState setVmUUID(String vmUUID) {
        this.vmUUID = vmUUID;
        return this;
    }

    public String getTagName() {
        return tagName;
    }

    public TPractiseTaskState setTagName(String tagName) {
        this.tagName = tagName;
        return this;
    }

    public Integer getState() {
        return state;
    }

    public TPractiseTaskState setState(Integer state) {
        this.state = state;
        return this;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public TPractiseTaskState setTime(LocalDateTime time) {
        this.time = time;
        return this;
    }

    @Override
    public String toString() {
        return "TPractiseTaskState{" +
        "configId=" + configId +
        ", taskId=" + taskId +
        ", planId=" + planId +
        ", sectionId=" + sectionId +
        ", uid=" + uid +
        ", vmType=" + vmType +
        ", vmUUID=" + vmUUID +
        ", tagName=" + tagName +
        ", state=" + state +
        ", time=" + time +
        "}";
    }
}
