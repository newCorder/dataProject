package com.wwx.entity;

import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
public class TLatestSectionStu implements Serializable {

    private static final long serialVersionUID = 1L;

    private String uid;

    @TableField("planId")
    private Integer planId;

    @TableField("sectionId")
    private Integer sectionId;

    @TableField("bookId")
    private Integer bookId;

    private LocalDateTime time;


    public String getUid() {
        return uid;
    }

    public TLatestSectionStu setUid(String uid) {
        this.uid = uid;
        return this;
    }

    public Integer getPlanId() {
        return planId;
    }

    public TLatestSectionStu setPlanId(Integer planId) {
        this.planId = planId;
        return this;
    }

    public Integer getSectionId() {
        return sectionId;
    }

    public TLatestSectionStu setSectionId(Integer sectionId) {
        this.sectionId = sectionId;
        return this;
    }

    public Integer getBookId() {
        return bookId;
    }

    public TLatestSectionStu setBookId(Integer bookId) {
        this.bookId = bookId;
        return this;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public TLatestSectionStu setTime(LocalDateTime time) {
        this.time = time;
        return this;
    }

    @Override
    public String toString() {
        return "TLatestSectionStu{" +
        "uid=" + uid +
        ", planId=" + planId +
        ", sectionId=" + sectionId +
        ", bookId=" + bookId +
        ", time=" + time +
        "}";
    }
}
