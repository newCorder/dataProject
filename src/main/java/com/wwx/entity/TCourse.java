package com.wwx.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-21
 */
public class TCourse implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 课程ID
     */
    private Integer courseid;

    /**
     * 课程名字 
     */
    @TableField("courseName")
    private String courseName;

    /**
     * 课程类型
     */
    @TableField("courseType")
    private Integer courseType;

    /**
     * 课程图片路径
     */
    @TableField("courseImagePath")
    private String courseImagePath;


    public Integer getCourseid() {
        return courseid;
    }

    public TCourse setCourseid(Integer courseid) {
        this.courseid = courseid;
        return this;
    }

    public String getCourseName() {
        return courseName;
    }

    public TCourse setCourseName(String courseName) {
        this.courseName = courseName;
        return this;
    }

    public Integer getCourseType() {
        return courseType;
    }

    public TCourse setCourseType(Integer courseType) {
        this.courseType = courseType;
        return this;
    }

    public String getCourseImagePath() {
        return courseImagePath;
    }

    public TCourse setCourseImagePath(String courseImagePath) {
        this.courseImagePath = courseImagePath;
        return this;
    }

    @Override
    public String toString() {
        return "TCourse{" +
        "courseid=" + courseid +
        ", courseName=" + courseName +
        ", courseType=" + courseType +
        ", courseImagePath=" + courseImagePath +
        "}";
    }
}
