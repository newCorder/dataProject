package com.wwx.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-21
 */
public class TChapter implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 章节ID
     */
    @TableId("chapterId")
    private Integer chapterId;

    /**
     * 书本ID
     */
    @TableField("courseId")
    private Integer courseId;

    /**
     * 章节名称
     */
    @TableField("chapterName")
    private String chapterName;

    /**
     * Html存储路径
     */
    private String html;

    /**
     * Pdf存储路径
     */
    private String pdf;

    /**
     * 每月个人薪资福利总额
     */
    @TableField("resName")
    private String resName;

    /**
     * 上传资源的原始路径，zip，ppt，doc等（转换成html，pdf）
     */
    @TableField("orgResPath")
    private String orgResPath;

    /**
     * 目的
     */
    private String purpose;

    /**
     * 建议课时
     */
    @TableField("suggestTime")
    private Integer suggestTime;

    /**
     * 视频名称
     */
    @TableField("videoName")
    private String videoName;

    /**
     * 视频存储路径
     */
    private String video;

    /**
     * 章节简介
     */
    private String content;


    public Integer getChapterId() {
        return chapterId;
    }

    public TChapter setChapterId(Integer chapterId) {
        this.chapterId = chapterId;
        return this;
    }

    public Integer getCourseId() {
        return courseId;
    }

    public TChapter setCourseId(Integer courseId) {
        this.courseId = courseId;
        return this;
    }

    public String getChapterName() {
        return chapterName;
    }

    public TChapter setChapterName(String chapterName) {
        this.chapterName = chapterName;
        return this;
    }

    public String getHtml() {
        return html;
    }

    public TChapter setHtml(String html) {
        this.html = html;
        return this;
    }

    public String getPdf() {
        return pdf;
    }

    public TChapter setPdf(String pdf) {
        this.pdf = pdf;
        return this;
    }

    public String getResName() {
        return resName;
    }

    public TChapter setResName(String resName) {
        this.resName = resName;
        return this;
    }

    public String getOrgResPath() {
        return orgResPath;
    }

    public TChapter setOrgResPath(String orgResPath) {
        this.orgResPath = orgResPath;
        return this;
    }

    public String getPurpose() {
        return purpose;
    }

    public TChapter setPurpose(String purpose) {
        this.purpose = purpose;
        return this;
    }

    public Integer getSuggestTime() {
        return suggestTime;
    }

    public TChapter setSuggestTime(Integer suggestTime) {
        this.suggestTime = suggestTime;
        return this;
    }

    public String getVideoName() {
        return videoName;
    }

    public TChapter setVideoName(String videoName) {
        this.videoName = videoName;
        return this;
    }

    public String getVideo() {
        return video;
    }

    public TChapter setVideo(String video) {
        this.video = video;
        return this;
    }

    public String getContent() {
        return content;
    }

    public TChapter setContent(String content) {
        this.content = content;
        return this;
    }

    @Override
    public String toString() {
        return "TChapter{" +
        "chapterId=" + chapterId +
        ", courseId=" + courseId +
        ", chapterName=" + chapterName +
        ", html=" + html +
        ", pdf=" + pdf +
        ", resName=" + resName +
        ", orgResPath=" + orgResPath +
        ", purpose=" + purpose +
        ", suggestTime=" + suggestTime +
        ", videoName=" + videoName +
        ", video=" + video +
        ", content=" + content +
        "}";
    }
}
