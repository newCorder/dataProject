package com.wwx.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
public class TPlanBook implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId("planId")
    private Integer planId;

    @TableField("bookId")
    private Integer bookId;

    @TableField("bookName")
    private String bookName;

    @TableField("bookImagePath")
    private String bookImagePath;

    @TableField("bookType")
    private Integer bookType;


    public Integer getPlanId() {
        return planId;
    }

    public TPlanBook setPlanId(Integer planId) {
        this.planId = planId;
        return this;
    }

    public Integer getBookId() {
        return bookId;
    }

    public TPlanBook setBookId(Integer bookId) {
        this.bookId = bookId;
        return this;
    }

    public String getBookName() {
        return bookName;
    }

    public TPlanBook setBookName(String bookName) {
        this.bookName = bookName;
        return this;
    }

    public String getBookImagePath() {
        return bookImagePath;
    }

    public TPlanBook setBookImagePath(String bookImagePath) {
        this.bookImagePath = bookImagePath;
        return this;
    }

    public Integer getBookType() {
        return bookType;
    }

    public TPlanBook setBookType(Integer bookType) {
        this.bookType = bookType;
        return this;
    }

    @Override
    public String toString() {
        return "TPlanBook{" +
        "planId=" + planId +
        ", bookId=" + bookId +
        ", bookName=" + bookName +
        ", bookImagePath=" + bookImagePath +
        ", bookType=" + bookType +
        "}";
    }
}
