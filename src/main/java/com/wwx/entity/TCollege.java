package com.wwx.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

/**
 * 院系表
 * @author wuweixu
 * @since 2019-08-12
 */
public class TCollege implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 学院ID
     */
    @TableId(value = "collegeId", type = IdType.AUTO)
    private Integer collegeId;
    /**
     * 学院名
     */
    private String name;

    /**
     * 0 禁用 1 启用
     */
    private Integer state;

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Integer getCollegeId() {
        return collegeId;
    }

    public TCollege setCollegeId(Integer collegeId) {
        this.collegeId = collegeId;
        return this;
    }

    public String getName() {
        return name;
    }

    public TCollege setName(String name) {
        this.name = name;
        return this;
    }

    @Override
    public String toString() {
        return "TCollege{" +
                "collegeId=" + collegeId +
                ", name='" + name + '\'' +
                ", state=" + state +
                '}';
    }
}
