package com.wwx.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
public class TExamPaper implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "paperId", type = IdType.AUTO)
    private Integer paperId;

    /**
     * 试卷关联的书籍
     */
    @TableField("bookId")
    private Integer bookId;

    private String name;

    /**
     * 试卷属性： 1 表示独占，2 共享（默认值）
     */
    private Integer state;

    /**
     * 创建者
     */
    @TableField("userId")
    private String userId;

    private String author;

    /**
     * 难易程度： 待扩展成配置
     */
    private Integer level;

    /**
     * 试卷简介
     */
    private String dsc;

    /**
     * 试卷封面
     */
    @TableField("imageUrl")
    private String imageUrl;

    @TableField("createTime")
    private LocalDateTime createTime;

    @TableField("updateTime")
    private LocalDateTime updateTime;


    public Integer getPaperId() {
        return paperId;
    }

    public TExamPaper setPaperId(Integer paperId) {
        this.paperId = paperId;
        return this;
    }

    public Integer getBookId() {
        return bookId;
    }

    public TExamPaper setBookId(Integer bookId) {
        this.bookId = bookId;
        return this;
    }

    public String getName() {
        return name;
    }

    public TExamPaper setName(String name) {
        this.name = name;
        return this;
    }

    public Integer getState() {
        return state;
    }

    public TExamPaper setState(Integer state) {
        this.state = state;
        return this;
    }

    public String getUserId() {
        return userId;
    }

    public TExamPaper setUserId(String userId) {
        this.userId = userId;
        return this;
    }

    public String getAuthor() {
        return author;
    }

    public TExamPaper setAuthor(String author) {
        this.author = author;
        return this;
    }

    public Integer getLevel() {
        return level;
    }

    public TExamPaper setLevel(Integer level) {
        this.level = level;
        return this;
    }

    public String getDsc() {
        return dsc;
    }

    public TExamPaper setDsc(String dsc) {
        this.dsc = dsc;
        return this;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public TExamPaper setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
        return this;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public TExamPaper setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
        return this;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public TExamPaper setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
        return this;
    }

    @Override
    public String toString() {
        return "TExamPaper{" +
        "paperId=" + paperId +
        ", bookId=" + bookId +
        ", name=" + name +
        ", state=" + state +
        ", userId=" + userId +
        ", author=" + author +
        ", level=" + level +
        ", dsc=" + dsc +
        ", imageUrl=" + imageUrl +
        ", createTime=" + createTime +
        ", updateTime=" + updateTime +
        "}";
    }
}
