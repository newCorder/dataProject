package com.wwx.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
public class TScorePractiseReport implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @TableField("planId")
    private Integer planId;

    @TableField("sectionId")
    private Integer sectionId;

    private String uid;

    @TableField("classId")
    private Integer classId;

    /**
     *  0作答中， 1 已提交 ，2 已评分
     */
    @TableField("practiseState")
    private Integer practiseState;

    @TableField("practiseAutoScore")
    private Float practiseAutoScore;

    @TableField("practiseManualScore")
    private Float practiseManualScore;

    /**
     *  0作答中， 1 已提交 ，2 已评分
     */
    @TableField("reportState")
    private Integer reportState;

    @TableField("reportScore")
    private Float reportScore;

    /**
     * 测评提交时间
     */
    @TableField("practiseSubmitTime")
    private LocalDateTime practiseSubmitTime;

    /**
     * 测评打分时间
     */
    @TableField("practiseScoreTime")
    private LocalDateTime practiseScoreTime;

    /**
     * 实验报告提交时间
     */
    @TableField("reportSubmitTime")
    private LocalDateTime reportSubmitTime;

    /**
     * 实验报告打分时间
     */
    @TableField("reportScoreTime")
    private LocalDateTime reportScoreTime;


    public Integer getId() {
        return id;
    }

    public TScorePractiseReport setId(Integer id) {
        this.id = id;
        return this;
    }

    public Integer getPlanId() {
        return planId;
    }

    public TScorePractiseReport setPlanId(Integer planId) {
        this.planId = planId;
        return this;
    }

    public Integer getSectionId() {
        return sectionId;
    }

    public TScorePractiseReport setSectionId(Integer sectionId) {
        this.sectionId = sectionId;
        return this;
    }

    public String getUid() {
        return uid;
    }

    public TScorePractiseReport setUid(String uid) {
        this.uid = uid;
        return this;
    }

    public Integer getClassId() {
        return classId;
    }

    public TScorePractiseReport setClassId(Integer classId) {
        this.classId = classId;
        return this;
    }

    public Integer getPractiseState() {
        return practiseState;
    }

    public TScorePractiseReport setPractiseState(Integer practiseState) {
        this.practiseState = practiseState;
        return this;
    }

    public Float getPractiseAutoScore() {
        return practiseAutoScore;
    }

    public TScorePractiseReport setPractiseAutoScore(Float practiseAutoScore) {
        this.practiseAutoScore = practiseAutoScore;
        return this;
    }

    public Float getPractiseManualScore() {
        return practiseManualScore;
    }

    public TScorePractiseReport setPractiseManualScore(Float practiseManualScore) {
        this.practiseManualScore = practiseManualScore;
        return this;
    }

    public Integer getReportState() {
        return reportState;
    }

    public TScorePractiseReport setReportState(Integer reportState) {
        this.reportState = reportState;
        return this;
    }

    public Float getReportScore() {
        return reportScore;
    }

    public TScorePractiseReport setReportScore(Float reportScore) {
        this.reportScore = reportScore;
        return this;
    }

    public LocalDateTime getPractiseSubmitTime() {
        return practiseSubmitTime;
    }

    public TScorePractiseReport setPractiseSubmitTime(LocalDateTime practiseSubmitTime) {
        this.practiseSubmitTime = practiseSubmitTime;
        return this;
    }

    public LocalDateTime getPractiseScoreTime() {
        return practiseScoreTime;
    }

    public TScorePractiseReport setPractiseScoreTime(LocalDateTime practiseScoreTime) {
        this.practiseScoreTime = practiseScoreTime;
        return this;
    }

    public LocalDateTime getReportSubmitTime() {
        return reportSubmitTime;
    }

    public TScorePractiseReport setReportSubmitTime(LocalDateTime reportSubmitTime) {
        this.reportSubmitTime = reportSubmitTime;
        return this;
    }

    public LocalDateTime getReportScoreTime() {
        return reportScoreTime;
    }

    public TScorePractiseReport setReportScoreTime(LocalDateTime reportScoreTime) {
        this.reportScoreTime = reportScoreTime;
        return this;
    }

    @Override
    public String toString() {
        return "TScorePractiseReport{" +
        "id=" + id +
        ", planId=" + planId +
        ", sectionId=" + sectionId +
        ", uid=" + uid +
        ", classId=" + classId +
        ", practiseState=" + practiseState +
        ", practiseAutoScore=" + practiseAutoScore +
        ", practiseManualScore=" + practiseManualScore +
        ", reportState=" + reportState +
        ", reportScore=" + reportScore +
        ", practiseSubmitTime=" + practiseSubmitTime +
        ", practiseScoreTime=" + practiseScoreTime +
        ", reportSubmitTime=" + reportSubmitTime +
        ", reportScoreTime=" + reportScoreTime +
        "}";
    }
}
