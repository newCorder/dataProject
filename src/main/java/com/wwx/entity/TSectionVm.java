package com.wwx.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
public class TSectionVm implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId("sectionId")
    private Integer sectionId;

    @TableField("vmType")
    private Integer vmType;

    @TableField("vmUUID")
    private String vmUUID;

    @TableField("tagName")
    private String tagName;

    @TableField("vmBtnName")
    private String vmBtnName;

    @TableField("vmSnapshotName")
    private String vmSnapshotName;

    /**
     * 逗号分隔：vb 是快照名,快照名； docker是 image-tag,image-tag
     */
    @TableField("restoreSnapshotName")
    private String restoreSnapshotName;


    public Integer getSectionId() {
        return sectionId;
    }

    public TSectionVm setSectionId(Integer sectionId) {
        this.sectionId = sectionId;
        return this;
    }

    public Integer getVmType() {
        return vmType;
    }

    public TSectionVm setVmType(Integer vmType) {
        this.vmType = vmType;
        return this;
    }

    public String getVmUUID() {
        return vmUUID;
    }

    public TSectionVm setVmUUID(String vmUUID) {
        this.vmUUID = vmUUID;
        return this;
    }

    public String getTagName() {
        return tagName;
    }

    public TSectionVm setTagName(String tagName) {
        this.tagName = tagName;
        return this;
    }

    public String getVmBtnName() {
        return vmBtnName;
    }

    public TSectionVm setVmBtnName(String vmBtnName) {
        this.vmBtnName = vmBtnName;
        return this;
    }

    public String getVmSnapshotName() {
        return vmSnapshotName;
    }

    public TSectionVm setVmSnapshotName(String vmSnapshotName) {
        this.vmSnapshotName = vmSnapshotName;
        return this;
    }

    public String getRestoreSnapshotName() {
        return restoreSnapshotName;
    }

    public TSectionVm setRestoreSnapshotName(String restoreSnapshotName) {
        this.restoreSnapshotName = restoreSnapshotName;
        return this;
    }

    @Override
    public String toString() {
        return "TSectionVm{" +
        "sectionId=" + sectionId +
        ", vmType=" + vmType +
        ", vmUUID=" + vmUUID +
        ", tagName=" + tagName +
        ", vmBtnName=" + vmBtnName +
        ", vmSnapshotName=" + vmSnapshotName +
        ", restoreSnapshotName=" + restoreSnapshotName +
        "}";
    }
}
