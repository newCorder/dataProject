package com.wwx.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
public class TPlanPractiseVm implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId("planId")
    private Integer planId;

    @TableField("sectionId")
    private Integer sectionId;

    @TableField("questionTypeId")
    private Integer questionTypeId;

    @TableField("questionId")
    private Integer questionId;

    @TableField("vmType")
    private Integer vmType;

    @TableField("vmUUID")
    private String vmUUID;

    @TableField("vmName")
    private String vmName;

    @TableField("tagName")
    private String tagName;

    @TableField("vmBtnName")
    private String vmBtnName;

    @TableField("vmSnapshotName")
    private String vmSnapshotName;

    @TableField("restoreSnapshotName")
    private String restoreSnapshotName;


    public Integer getPlanId() {
        return planId;
    }

    public TPlanPractiseVm setPlanId(Integer planId) {
        this.planId = planId;
        return this;
    }

    public Integer getSectionId() {
        return sectionId;
    }

    public TPlanPractiseVm setSectionId(Integer sectionId) {
        this.sectionId = sectionId;
        return this;
    }

    public Integer getQuestionTypeId() {
        return questionTypeId;
    }

    public TPlanPractiseVm setQuestionTypeId(Integer questionTypeId) {
        this.questionTypeId = questionTypeId;
        return this;
    }

    public Integer getQuestionId() {
        return questionId;
    }

    public TPlanPractiseVm setQuestionId(Integer questionId) {
        this.questionId = questionId;
        return this;
    }

    public Integer getVmType() {
        return vmType;
    }

    public TPlanPractiseVm setVmType(Integer vmType) {
        this.vmType = vmType;
        return this;
    }

    public String getVmUUID() {
        return vmUUID;
    }

    public TPlanPractiseVm setVmUUID(String vmUUID) {
        this.vmUUID = vmUUID;
        return this;
    }

    public String getVmName() {
        return vmName;
    }

    public TPlanPractiseVm setVmName(String vmName) {
        this.vmName = vmName;
        return this;
    }

    public String getTagName() {
        return tagName;
    }

    public TPlanPractiseVm setTagName(String tagName) {
        this.tagName = tagName;
        return this;
    }

    public String getVmBtnName() {
        return vmBtnName;
    }

    public TPlanPractiseVm setVmBtnName(String vmBtnName) {
        this.vmBtnName = vmBtnName;
        return this;
    }

    public String getVmSnapshotName() {
        return vmSnapshotName;
    }

    public TPlanPractiseVm setVmSnapshotName(String vmSnapshotName) {
        this.vmSnapshotName = vmSnapshotName;
        return this;
    }

    public String getRestoreSnapshotName() {
        return restoreSnapshotName;
    }

    public TPlanPractiseVm setRestoreSnapshotName(String restoreSnapshotName) {
        this.restoreSnapshotName = restoreSnapshotName;
        return this;
    }

    @Override
    public String toString() {
        return "TPlanPractiseVm{" +
        "planId=" + planId +
        ", sectionId=" + sectionId +
        ", questionTypeId=" + questionTypeId +
        ", questionId=" + questionId +
        ", vmType=" + vmType +
        ", vmUUID=" + vmUUID +
        ", vmName=" + vmName +
        ", tagName=" + tagName +
        ", vmBtnName=" + vmBtnName +
        ", vmSnapshotName=" + vmSnapshotName +
        ", restoreSnapshotName=" + restoreSnapshotName +
        "}";
    }
}
