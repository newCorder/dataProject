package com.wwx.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
public class TReportStep implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "stepId", type = IdType.AUTO)
    private Integer stepId;

    @TableField("planId")
    private Integer planId;

    private String uid;

    @TableField("sectionId")
    private Integer sectionId;

    private String name;

    private String dsc;

    /**
     * 1 过程，2 结果
     */
    private Integer state;


    public Integer getStepId() {
        return stepId;
    }

    public TReportStep setStepId(Integer stepId) {
        this.stepId = stepId;
        return this;
    }

    public Integer getPlanId() {
        return planId;
    }

    public TReportStep setPlanId(Integer planId) {
        this.planId = planId;
        return this;
    }

    public String getUid() {
        return uid;
    }

    public TReportStep setUid(String uid) {
        this.uid = uid;
        return this;
    }

    public Integer getSectionId() {
        return sectionId;
    }

    public TReportStep setSectionId(Integer sectionId) {
        this.sectionId = sectionId;
        return this;
    }

    public String getName() {
        return name;
    }

    public TReportStep setName(String name) {
        this.name = name;
        return this;
    }

    public String getDsc() {
        return dsc;
    }

    public TReportStep setDsc(String dsc) {
        this.dsc = dsc;
        return this;
    }

    public Integer getState() {
        return state;
    }

    public TReportStep setState(Integer state) {
        this.state = state;
        return this;
    }

    @Override
    public String toString() {
        return "TReportStep{" +
        "stepId=" + stepId +
        ", planId=" + planId +
        ", uid=" + uid +
        ", sectionId=" + sectionId +
        ", name=" + name +
        ", dsc=" + dsc +
        ", state=" + state +
        "}";
    }
}
