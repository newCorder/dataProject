package com.wwx.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-21
 */
public class TCoursetag implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 书本标签ID
     */
    private Integer tagid;

    /**
     * 书本ID
     */
    @TableField("courseId")
    private Integer courseId;

    /**
     * 书本标签名
     */
    @TableField("tagName")
    private String tagName;


    public Integer getTagid() {
        return tagid;
    }

    public TCoursetag setTagid(Integer tagid) {
        this.tagid = tagid;
        return this;
    }

    public Integer getCourseId() {
        return courseId;
    }

    public TCoursetag setCourseId(Integer courseId) {
        this.courseId = courseId;
        return this;
    }

    public String getTagName() {
        return tagName;
    }

    public TCoursetag setTagName(String tagName) {
        this.tagName = tagName;
        return this;
    }

    @Override
    public String toString() {
        return "TCoursetag{" +
        "tagid=" + tagid +
        ", courseId=" + courseId +
        ", tagName=" + tagName +
        "}";
    }
}
