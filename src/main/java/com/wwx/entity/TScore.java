package com.wwx.entity;

import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-21
 */
public class TScore implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 成绩ID（根据scoreType存储对应的id，分别存储T_exam中的examId,或T_ practise中的practiseId或T_report中的reportId）
     */
    private Integer scored;

    /**
     * 成绩类型（1：考试成绩，2：测评成绩，3:实验成绩）
     */
    @TableField("scoreType")
    private Integer scoreType;

    /**
     * 学生ID
     */
    @TableField("studentId")
    private String studentId;

    /**
     * 成绩分数
     */
    private Float score;

    /**
     * 开始时间
     */
    @TableField("startTime")
    private LocalDateTime startTime;

    /**
     * 结束时间
     */
    @TableField("endTime")
    private LocalDateTime endTime;


    public Integer getScored() {
        return scored;
    }

    public TScore setScored(Integer scored) {
        this.scored = scored;
        return this;
    }

    public Integer getScoreType() {
        return scoreType;
    }

    public TScore setScoreType(Integer scoreType) {
        this.scoreType = scoreType;
        return this;
    }

    public String getStudentId() {
        return studentId;
    }

    public TScore setStudentId(String studentId) {
        this.studentId = studentId;
        return this;
    }

    public Float getScore() {
        return score;
    }

    public TScore setScore(Float score) {
        this.score = score;
        return this;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public TScore setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
        return this;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public TScore setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
        return this;
    }

    @Override
    public String toString() {
        return "TScore{" +
        "scored=" + scored +
        ", scoreType=" + scoreType +
        ", studentId=" + studentId +
        ", score=" + score +
        ", startTime=" + startTime +
        ", endTime=" + endTime +
        "}";
    }
}
