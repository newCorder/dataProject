package com.wwx.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-21
 */
public class TChapterRecord implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 用户ID（根据userType存储对应的id）
     */
    @TableId("userId")
    private String userId;

    /**
     * 访问时间
     */
    private LocalDateTime time;

    /**
     * 章节ID
     */
    @TableField("chapterId")
    private Integer chapterId;

    /**
     * 成绩类型（1：学生，2：老师，3:管理员）
     */
    @TableField("userType")
    private Integer userType;


    public String getUserId() {
        return userId;
    }

    public TChapterRecord setUserId(String userId) {
        this.userId = userId;
        return this;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public TChapterRecord setTime(LocalDateTime time) {
        this.time = time;
        return this;
    }

    public Integer getChapterId() {
        return chapterId;
    }

    public TChapterRecord setChapterId(Integer chapterId) {
        this.chapterId = chapterId;
        return this;
    }

    public Integer getUserType() {
        return userType;
    }

    public TChapterRecord setUserType(Integer userType) {
        this.userType = userType;
        return this;
    }

    @Override
    public String toString() {
        return "TChapterRecord{" +
        "userId=" + userId +
        ", time=" + time +
        ", chapterId=" + chapterId +
        ", userType=" + userType +
        "}";
    }
}
