package com.wwx.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
public class TPractiseVm implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId("questionTypeId")
    private Integer questionTypeId;

    @TableField("questionId")
    private Integer questionId;

    @TableField("vmType")
    private Integer vmType;

    @TableField("vmUUID")
    private String vmUUID;

    @TableField("tagName")
    private String tagName;

    @TableField("vmBtnName")
    private String vmBtnName;

    @TableField("vmSnapshotName")
    private String vmSnapshotName;

    @TableField("restoreSnapshotName")
    private String restoreSnapshotName;


    public Integer getQuestionTypeId() {
        return questionTypeId;
    }

    public TPractiseVm setQuestionTypeId(Integer questionTypeId) {
        this.questionTypeId = questionTypeId;
        return this;
    }

    public Integer getQuestionId() {
        return questionId;
    }

    public TPractiseVm setQuestionId(Integer questionId) {
        this.questionId = questionId;
        return this;
    }

    public Integer getVmType() {
        return vmType;
    }

    public TPractiseVm setVmType(Integer vmType) {
        this.vmType = vmType;
        return this;
    }

    public String getVmUUID() {
        return vmUUID;
    }

    public TPractiseVm setVmUUID(String vmUUID) {
        this.vmUUID = vmUUID;
        return this;
    }

    public String getTagName() {
        return tagName;
    }

    public TPractiseVm setTagName(String tagName) {
        this.tagName = tagName;
        return this;
    }

    public String getVmBtnName() {
        return vmBtnName;
    }

    public TPractiseVm setVmBtnName(String vmBtnName) {
        this.vmBtnName = vmBtnName;
        return this;
    }

    public String getVmSnapshotName() {
        return vmSnapshotName;
    }

    public TPractiseVm setVmSnapshotName(String vmSnapshotName) {
        this.vmSnapshotName = vmSnapshotName;
        return this;
    }

    public String getRestoreSnapshotName() {
        return restoreSnapshotName;
    }

    public TPractiseVm setRestoreSnapshotName(String restoreSnapshotName) {
        this.restoreSnapshotName = restoreSnapshotName;
        return this;
    }

    @Override
    public String toString() {
        return "TPractiseVm{" +
        "questionTypeId=" + questionTypeId +
        ", questionId=" + questionId +
        ", vmType=" + vmType +
        ", vmUUID=" + vmUUID +
        ", tagName=" + tagName +
        ", vmBtnName=" + vmBtnName +
        ", vmSnapshotName=" + vmSnapshotName +
        ", restoreSnapshotName=" + restoreSnapshotName +
        "}";
    }
}
