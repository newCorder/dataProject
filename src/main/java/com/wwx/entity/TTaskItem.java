package com.wwx.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
public class TTaskItem implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "taskId", type = IdType.AUTO)
    private Integer taskId;

    /**
     * 任务名字
     */
    private String name;

    /**
     * 任务描述，显示在左下角status
     */
    private String dsc;

    /**
     * 任务详细提示
     */
    private String tip;

    private String rule;


    public Integer getTaskId() {
        return taskId;
    }

    public TTaskItem setTaskId(Integer taskId) {
        this.taskId = taskId;
        return this;
    }

    public String getName() {
        return name;
    }

    public TTaskItem setName(String name) {
        this.name = name;
        return this;
    }

    public String getDsc() {
        return dsc;
    }

    public TTaskItem setDsc(String dsc) {
        this.dsc = dsc;
        return this;
    }

    public String getTip() {
        return tip;
    }

    public TTaskItem setTip(String tip) {
        this.tip = tip;
        return this;
    }

    public String getRule() {
        return rule;
    }

    public TTaskItem setRule(String rule) {
        this.rule = rule;
        return this;
    }

    @Override
    public String toString() {
        return "TTaskItem{" +
        "taskId=" + taskId +
        ", name=" + name +
        ", dsc=" + dsc +
        ", tip=" + tip +
        ", rule=" + rule +
        "}";
    }
}
