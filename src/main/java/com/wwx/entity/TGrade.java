package com.wwx.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
public class TGrade implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "gradeId", type = IdType.AUTO)
    private Integer gradeId;

    private String name;


    public Integer getGradeId() {
        return gradeId;
    }

    public TGrade setGradeId(Integer gradeId) {
        this.gradeId = gradeId;
        return this;
    }

    public String getName() {
        return name;
    }

    public TGrade setName(String name) {
        this.name = name;
        return this;
    }

    @Override
    public String toString() {
        return "TGrade{" +
        "gradeId=" + gradeId +
        ", name=" + name +
        "}";
    }
}
