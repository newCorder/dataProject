package com.wwx.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-21
 */
public class TExampaperQuestion implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 考试试卷ID
     */
    @TableId("examPaperId")
    private Integer examPaperId;

    /**
     * 题目ID
     */
    @TableField("questionId")
    private Integer questionId;


    public Integer getExamPaperId() {
        return examPaperId;
    }

    public TExampaperQuestion setExamPaperId(Integer examPaperId) {
        this.examPaperId = examPaperId;
        return this;
    }

    public Integer getQuestionId() {
        return questionId;
    }

    public TExampaperQuestion setQuestionId(Integer questionId) {
        this.questionId = questionId;
        return this;
    }

    @Override
    public String toString() {
        return "TExampaperQuestion{" +
        "examPaperId=" + examPaperId +
        ", questionId=" + questionId +
        "}";
    }
}
