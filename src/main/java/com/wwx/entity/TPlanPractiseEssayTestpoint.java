package com.wwx.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
public class TPlanPractiseEssayTestpoint implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "testpointId", type = IdType.AUTO)
    private Integer testpointId;

    @TableField("planId")
    private Integer planId;

    @TableField("sectionId")
    private Integer sectionId;

    @TableField("questionTypeId")
    private Integer questionTypeId;

    @TableField("questionId")
    private Integer questionId;

    @TableField("testpointName")
    private String testpointName;

    private String answer;

    private String analyse;


    public Integer getTestpointId() {
        return testpointId;
    }

    public TPlanPractiseEssayTestpoint setTestpointId(Integer testpointId) {
        this.testpointId = testpointId;
        return this;
    }

    public Integer getPlanId() {
        return planId;
    }

    public TPlanPractiseEssayTestpoint setPlanId(Integer planId) {
        this.planId = planId;
        return this;
    }

    public Integer getSectionId() {
        return sectionId;
    }

    public TPlanPractiseEssayTestpoint setSectionId(Integer sectionId) {
        this.sectionId = sectionId;
        return this;
    }

    public Integer getQuestionTypeId() {
        return questionTypeId;
    }

    public TPlanPractiseEssayTestpoint setQuestionTypeId(Integer questionTypeId) {
        this.questionTypeId = questionTypeId;
        return this;
    }

    public Integer getQuestionId() {
        return questionId;
    }

    public TPlanPractiseEssayTestpoint setQuestionId(Integer questionId) {
        this.questionId = questionId;
        return this;
    }

    public String getTestpointName() {
        return testpointName;
    }

    public TPlanPractiseEssayTestpoint setTestpointName(String testpointName) {
        this.testpointName = testpointName;
        return this;
    }

    public String getAnswer() {
        return answer;
    }

    public TPlanPractiseEssayTestpoint setAnswer(String answer) {
        this.answer = answer;
        return this;
    }

    public String getAnalyse() {
        return analyse;
    }

    public TPlanPractiseEssayTestpoint setAnalyse(String analyse) {
        this.analyse = analyse;
        return this;
    }

    @Override
    public String toString() {
        return "TPlanPractiseEssayTestpoint{" +
        "testpointId=" + testpointId +
        ", planId=" + planId +
        ", sectionId=" + sectionId +
        ", questionTypeId=" + questionTypeId +
        ", questionId=" + questionId +
        ", testpointName=" + testpointName +
        ", answer=" + answer +
        ", analyse=" + analyse +
        "}";
    }
}
