package com.wwx.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
public class TPractiseEssayTestpoint implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "testpointId", type = IdType.AUTO)
    private Integer testpointId;

    @TableField("questionTypeId")
    private Integer questionTypeId;

    @TableField("questionId")
    private Integer questionId;

    @TableField("testpointName")
    private String testpointName;

    private String answer;

    private String analyse;


    public Integer getTestpointId() {
        return testpointId;
    }

    public TPractiseEssayTestpoint setTestpointId(Integer testpointId) {
        this.testpointId = testpointId;
        return this;
    }

    public Integer getQuestionTypeId() {
        return questionTypeId;
    }

    public TPractiseEssayTestpoint setQuestionTypeId(Integer questionTypeId) {
        this.questionTypeId = questionTypeId;
        return this;
    }

    public Integer getQuestionId() {
        return questionId;
    }

    public TPractiseEssayTestpoint setQuestionId(Integer questionId) {
        this.questionId = questionId;
        return this;
    }

    public String getTestpointName() {
        return testpointName;
    }

    public TPractiseEssayTestpoint setTestpointName(String testpointName) {
        this.testpointName = testpointName;
        return this;
    }

    public String getAnswer() {
        return answer;
    }

    public TPractiseEssayTestpoint setAnswer(String answer) {
        this.answer = answer;
        return this;
    }

    public String getAnalyse() {
        return analyse;
    }

    public TPractiseEssayTestpoint setAnalyse(String analyse) {
        this.analyse = analyse;
        return this;
    }

    @Override
    public String toString() {
        return "TPractiseEssayTestpoint{" +
        "testpointId=" + testpointId +
        ", questionTypeId=" + questionTypeId +
        ", questionId=" + questionId +
        ", testpointName=" + testpointName +
        ", answer=" + answer +
        ", analyse=" + analyse +
        "}";
    }
}
