package com.wwx.entity;

import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;

import java.io.Serializable;

/**
 *  教师表
 * @author wuweixu
 * @since 2019-08-12
 */
public class TTeacher implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 用户ID(主键)
     */
    @TableId(value = "teacherId")
    private String teacherId;

    /**
     * 学院ID
     */
    @ApiModelProperty(value="学院id",name="collegeId",example="1111")
    @TableField("collegeId")
    private Integer collegeId;

    /**
     * 账户
     */
    private String name;

    /**
     * 密码
     */
    private String pwd;

    /**
     * 最新信息更新时间
     */
    @TableField("updateTime")
    private LocalDateTime updateTime;
    /**
     * 上次登录时间
     */
    private LocalDateTime logintime;

    /**
     * 0 禁用 1 启用
     */
    private Integer state;

    /**
     * 0 男 1 女
     */
    private Integer sex;
    /**
     * 电话
     */
    private String tel;
    /**
     * 邮箱
     */
    private String mail;

    /**
     * 0离线，1在线
     */
    @TableField("onlineState")
    private Integer onlineState;

    /**
     * 0正常数据 1软删除的垃圾数据
     */
    @TableField("yn")
    private Integer yn;

    public String getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(String teacherId) {
        this.teacherId = teacherId;
    }

    public Integer getCollegeId() {
        return collegeId;
    }

    public void setCollegeId(Integer collegeId) {
        this.collegeId = collegeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    public LocalDateTime getLogintime() {
        return logintime;
    }

    public void setLogintime(LocalDateTime logintime) {
        this.logintime = logintime;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public Integer getOnlineState() {
        return onlineState;
    }

    public void setOnlineState(Integer onlineState) {
        this.onlineState = onlineState;
    }

    public Integer getYn() {
        return yn;
    }

    public void setYn(Integer yn) {
        this.yn = yn;
    }

    @Override
    public String toString() {
        return "TTeacher{" +
                "teacherId='" + teacherId + '\'' +
                ", collegeId=" + collegeId +
                ", name='" + name + '\'' +
                ", pwd='" + pwd + '\'' +
                ", updateTime=" + updateTime +
                ", logintime=" + logintime +
                ", state=" + state +
                ", sex=" + sex +
                ", tel='" + tel + '\'' +
                ", mail='" + mail + '\'' +
                ", onlineState=" + onlineState +
                ", yn=" + yn +
                '}';
    }
}
