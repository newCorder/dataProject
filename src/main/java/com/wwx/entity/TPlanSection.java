package com.wwx.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
public class TPlanSection implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId("planId")
    private Integer planId;

    @TableField("sectionId")
    private Integer sectionId;

    @TableField("bookId")
    private Integer bookId;

    @TableField("sectionName")
    private String sectionName;

    private String pdf;

    private String html;

    @TableField("resName")
    private String resName;

    @TableField("orgResPath")
    private String orgResPath;

    private String video;

    @TableField("videoName")
    private String videoName;

    @TableField("suggestTime")
    private Integer suggestTime;

    private String purpose;

    private String content;


    public Integer getPlanId() {
        return planId;
    }

    public TPlanSection setPlanId(Integer planId) {
        this.planId = planId;
        return this;
    }

    public Integer getSectionId() {
        return sectionId;
    }

    public TPlanSection setSectionId(Integer sectionId) {
        this.sectionId = sectionId;
        return this;
    }

    public Integer getBookId() {
        return bookId;
    }

    public TPlanSection setBookId(Integer bookId) {
        this.bookId = bookId;
        return this;
    }

    public String getSectionName() {
        return sectionName;
    }

    public TPlanSection setSectionName(String sectionName) {
        this.sectionName = sectionName;
        return this;
    }

    public String getPdf() {
        return pdf;
    }

    public TPlanSection setPdf(String pdf) {
        this.pdf = pdf;
        return this;
    }

    public String getHtml() {
        return html;
    }

    public TPlanSection setHtml(String html) {
        this.html = html;
        return this;
    }

    public String getResName() {
        return resName;
    }

    public TPlanSection setResName(String resName) {
        this.resName = resName;
        return this;
    }

    public String getOrgResPath() {
        return orgResPath;
    }

    public TPlanSection setOrgResPath(String orgResPath) {
        this.orgResPath = orgResPath;
        return this;
    }

    public String getVideo() {
        return video;
    }

    public TPlanSection setVideo(String video) {
        this.video = video;
        return this;
    }

    public String getVideoName() {
        return videoName;
    }

    public TPlanSection setVideoName(String videoName) {
        this.videoName = videoName;
        return this;
    }

    public Integer getSuggestTime() {
        return suggestTime;
    }

    public TPlanSection setSuggestTime(Integer suggestTime) {
        this.suggestTime = suggestTime;
        return this;
    }

    public String getPurpose() {
        return purpose;
    }

    public TPlanSection setPurpose(String purpose) {
        this.purpose = purpose;
        return this;
    }

    public String getContent() {
        return content;
    }

    public TPlanSection setContent(String content) {
        this.content = content;
        return this;
    }

    @Override
    public String toString() {
        return "TPlanSection{" +
        "planId=" + planId +
        ", sectionId=" + sectionId +
        ", bookId=" + bookId +
        ", sectionName=" + sectionName +
        ", pdf=" + pdf +
        ", html=" + html +
        ", resName=" + resName +
        ", orgResPath=" + orgResPath +
        ", video=" + video +
        ", videoName=" + videoName +
        ", suggestTime=" + suggestTime +
        ", purpose=" + purpose +
        ", content=" + content +
        "}";
    }
}
