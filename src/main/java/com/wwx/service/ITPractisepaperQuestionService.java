package com.wwx.service;

import com.wwx.entity.TPractisepaperQuestion;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-21
 */
public interface ITPractisepaperQuestionService extends IService<TPractisepaperQuestion> {

}
