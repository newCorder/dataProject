package com.wwx.service;

import com.wwx.entity.TUpload;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 上传的文件 服务类
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
public interface ITUploadService extends IService<TUpload> {

}
