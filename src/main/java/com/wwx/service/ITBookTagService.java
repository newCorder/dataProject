package com.wwx.service;

import com.wwx.entity.TBookTag;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
public interface ITBookTagService extends IService<TBookTag> {

}
