package com.wwx.service;

import com.wwx.entity.TPractise;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 测评信息表 服务类
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
public interface ITPractiseService extends IService<TPractise> {

}
