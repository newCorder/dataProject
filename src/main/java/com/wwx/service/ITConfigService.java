package com.wwx.service;

import com.wwx.entity.TConfig;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * system config 服务类
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-12
 */
public interface ITConfigService extends IService<TConfig> {

}
