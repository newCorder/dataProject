package com.wwx.service;

import com.wwx.entity.TChapterRecord;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-21
 */
public interface ITChapterRecordService extends IService<TChapterRecord> {

}
