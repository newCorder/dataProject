package com.wwx.service;

import com.wwx.entity.TopicAt;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 评论@信息表 服务类
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-05
 */
public interface ITopicAtService extends IService<TopicAt> {

}
