package com.wwx.service;

import com.wwx.entity.TRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 角色表 服务类
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-19
 */
public interface ITRoleService extends IService<TRole> {

}
