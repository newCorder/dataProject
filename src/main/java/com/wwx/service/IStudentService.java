package com.wwx.service;

import com.wwx.entity.Student;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-06
 */
public interface IStudentService extends IService<Student> {

}
