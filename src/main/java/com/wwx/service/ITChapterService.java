package com.wwx.service;

import com.wwx.entity.TChapter;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wuweixu
 * @since 2019-08-21
 */
public interface ITChapterService extends IService<TChapter> {

}
